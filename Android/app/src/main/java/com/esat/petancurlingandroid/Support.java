package com.esat.petancurlingandroid;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static java.sql.Types.NULL;

/**
 * Created by mmarti on 26/3/17.
 */

public class Support {

    private Context context;

    static {
        System.loadLibrary("native-code");
    }

    private static native void initIDs(Support support);

    public Support(Context ctx) {
        context = ctx;

        initIDs(this);
    }

    // Method get internal storage folder
    public String pathToSaveGameData(String file) {
        File f = context.getFilesDir();
        String str = f.getAbsolutePath();
        String path = str + file;
        return path;
    }

    // Methods manage sounds

    private SoundPool sp;
    private MediaPlayer mp;
    AssetFileDescriptor afd = null;
    AssetManager manager;
    int loadedSounds = 0;
    int loadedMedia = 0;
    int[] mysounds = new int[10];
    int actualsounds = 0;
    int maxsounds=10;
    public boolean initSounds(int maxNumSounds) {
        String log = "Support:initSounds(" + maxNumSounds + ")";
        Log.i("PetanCurling", log);

        for(int i=0; i<maxNumSounds; i++){
            mysounds[i]=0;
        }
        maxsounds = maxNumSounds;
        if(maxNumSounds<1)
            return false;
        else if(loadedSounds==1)
            return false;
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes mAudioAttribues;
            mAudioAttribues = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            sp = new SoundPool.Builder().
                    setMaxStreams(maxNumSounds).
                    setAudioAttributes(mAudioAttribues).
                    build();
            loadedSounds=1;
            return true;
        } else {
            sp = new SoundPool(maxNumSounds, AudioManager.STREAM_MUSIC, 0);
            loadedSounds=1;
            return true;
        }
    }

    public int loadSound(String file) {
        String log = "Support:loadSound(" + file + ")";
        Log.i("PetanCurling", log);
        int id=-1;
        manager = context.getAssets();
        if(actualsounds>=maxsounds)
            return -1;
        else if(file == null)
            return -1;
        else if (loadedSounds!=1)
            return -1;
        else {
            try {
                afd = manager.openFd(file);
                id = sp.load(afd, 1);
                mysounds[id] = 1;
                actualsounds++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return id;
    }

    public boolean playSound(int index) {
        String log = "Support:playSound(" + index + ")";
        Log.i("PetanCurling", log);
        if(index>=0 && index < maxsounds){
            if(mysounds[index]==1) {
                sp.play(index, 1.0f, 1.0f, 0, 0, 1.0f);
                return true;
            }
        }
        return false;
    }

    public void releaseSounds() {
        String log = "Support:releaseSounds()";
        Log.i("PetanCurling", log);
        sp.release();
        sp = null;
    }

    int loadedMusic = 0;

    // Methods manage music
    public int loadMusic(String file) {
        String log = "Support:loadMusic(" + file + ")";
        Log.i("PetanCurling", log);
        loadedMedia = -1;

        if(file==null)return loadedMedia;

        mp = new MediaPlayer();
        manager = context.getAssets();
        try {
            afd = manager.openFd(file);
            mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mp.prepare();
            loadedMedia=1;
            loadedMusic = 1;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return loadedMedia;
    }

    int playingmusic=0;
    public boolean playMusic(int index) {
        String log = "Support:playMusic(" + index + ")";
        Log.i("PetanCurling", log);
        if(loadedMusic==1){
            mp.start();
            mp.setLooping(true);
            playingmusic=1;
            return true;
        }
        else return false;
    }

    public boolean pauseMusic(int index) {
        String log = "Support:pauseMusic(" + index + ")";
        Log.i("PetanCurling", log);
        if(loadedMusic==1) {
            if (playingmusic == 1) {
                mp.pause();
                return true;
            }
            else return false;
        }
        else return false;
    }

    public boolean isPlayingMusic(int index) {
        String log = "Support:isPlayingMusic(" + index + ")";
        Log.i("PetanCurling", log);
        if(loadedMedia!=1)
            return false;
        else if(mp.isPlaying())
            return true;
        else
            return false;
    }

    public void releaseMusic(int index) {
        String log = "Support:releaseMusic(" + index + ")";
        Log.i("PetanCurling", log);
        if(loadedMedia==1)
            mp.release();
    }

    // Method load images
    public Bitmap getBitmap(String fileName) {
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = null;
        try {
            inputStream = assetManager.open(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        int bytes = bitmap.getByteCount();

        return bitmap;
    }

    // Method release images
    public void releaseBitmap(Bitmap bitmap) {
        bitmap.recycle();
        bitmap = null;
    }


    // Methods load shaders
    public String getVertexShader(String fileName) {
        return ReadTextFileFromRawResource(context, fileName);
    }

    public String getFragmentShader(String fileName) {
        return ReadTextFileFromRawResource(context, fileName);
    }

    // Utility method
    public static String ReadTextFileFromRawResource(final Context context, String fileName)
    {
        AssetManager assetManager = context.getAssets();

        InputStream inputStream = null;
        try {
            inputStream = assetManager.open(fileName);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        final InputStreamReader inputStreamReader = new InputStreamReader(
                inputStream);
        final BufferedReader bufferedReader = new BufferedReader(
                inputStreamReader);

        String nextLine;
        final StringBuilder body = new StringBuilder();

        try
        {
            while ((nextLine = bufferedReader.readLine()) != null)
            {
                body.append(nextLine);
                body.append('\n');
            }
        }
        catch (IOException e)
        {
            return null;
        }

        return body.toString();
    }

    // Utility method
    public static String ReadTextFileFromRawResource_old(final Context context, final int resourceId)
    {
        final InputStream inputStream = context.getResources().openRawResource(
                resourceId);
        final InputStreamReader inputStreamReader = new InputStreamReader(
                inputStream);
        final BufferedReader bufferedReader = new BufferedReader(
                inputStreamReader);

        String nextLine;
        final StringBuilder body = new StringBuilder();

        try
        {
            while ((nextLine = bufferedReader.readLine()) != null)
            {
                body.append(nextLine);
                body.append('\n');
            }
        }
        catch (IOException e)
        {
            return null;
        }

        return body.toString();
    }
}
