attribute vec3 a_position;
attribute vec2 a_texture;
uniform mat4 u_mvp;
varying vec2 text_coords;

void main()
{
    text_coords = a_texture;
    gl_Position = u_mvp * vec4(a_position,1.0);
}
