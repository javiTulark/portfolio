#include "support.h"

#include <jni.h>
#include <android/bitmap.h>
#include <string.h>

#include "utils.h"

static JavaVM *jVM;
static jobject supportJava;

// Callbacks to Android
jmethodID mID_getVertexShader;
jmethodID mID_getFragmentShader;

jmethodID mID_getBitmap;
jmethodID mID_releaseBitmap;

jmethodID mID_pathToSaveGameData;

jmethodID mID_initSounds;
jmethodID mID_loadSound;
jmethodID mID_playSound;
jmethodID mID_releaseSounds;

jmethodID mID_loadMusic;
jmethodID mID_playMusic;
jmethodID mID_pauseMusic;
jmethodID mID_isPlayingMusic;
jmethodID mID_releaseMusic;

extern "C" {

void Java_com_esat_petancurlingandroid_Support_initIDs(JNIEnv *env, jclass cls,
                                               jobject obj);

}

void Java_com_esat_petancurlingandroid_Support_initIDs(JNIEnv *env, jclass cls,
                                               jobject obj) {
    jint ok = env->GetJavaVM(&jVM);
    if (ok != JNI_OK) {
        utils_LogWarn(APP_NAME, "Could not obtain VM");
    }

    supportJava = (jobject)env->NewGlobalRef(obj);
    if (supportJava == 0) {
        utils_LogWarn(APP_NAME, "Support null");
    }

    // methods load shaders
    mID_getVertexShader = env->GetMethodID(cls, "getVertexShader",
                                           "(Ljava/lang/String;)Ljava/lang/String;");
    if (mID_getVertexShader == 0) {
        utils_LogWarn(APP_NAME, "getVertexShader null");
    }

    mID_getFragmentShader = env->GetMethodID(cls, "getFragmentShader",
                                             "(Ljava/lang/String;)Ljava/lang/String;");
    if (mID_getFragmentShader == 0) {
        utils_LogWarn(APP_NAME, "getFragmentShader null");
    }

    // method load images
    mID_getBitmap = env->GetMethodID(cls, "getBitmap", "(Ljava/lang/String;)Landroid/graphics/Bitmap;");
    if (mID_getBitmap == 0) {
        utils_LogWarn(APP_NAME, "getBitmap null");
    }

    // method release images
    mID_releaseBitmap = env->GetMethodID(cls, "releaseBitmap", "(Landroid/graphics/Bitmap;)V");
    if (mID_releaseBitmap == 0) {
        utils_LogWarn(APP_NAME, "releaseBitmap null");
    }

    // method help save game data
    mID_pathToSaveGameData = env->GetMethodID(cls, "pathToSaveGameData", "(Ljava/lang/String;)Ljava/lang/String;");
    if (mID_pathToSaveGameData == 0) {
        utils_LogWarn(APP_NAME, "pathToSaveGameData null");
    }

    // methods manage sounds
    mID_initSounds = env->GetMethodID(cls, "initSounds", "(I)Z");
    if (mID_initSounds == 0) {
        utils_LogWarn(APP_NAME, "initSounds null");
    }

    mID_loadSound = env->GetMethodID(cls, "loadSound", "(Ljava/lang/String;)I");
    if (mID_loadSound == 0) {
        utils_LogWarn(APP_NAME, "loadSound null");
    }

    mID_playSound = env->GetMethodID(cls, "playSound", "(I)Z");
    if (mID_playSound == 0) {
        utils_LogWarn(APP_NAME, "playSound null");
    }

    mID_releaseSounds = env->GetMethodID(cls, "releaseSounds", "()V");
    if (mID_releaseSounds == 0) {
        utils_LogWarn(APP_NAME, "releaseSounds null");
    }

    // methods manage music
    mID_loadMusic = env->GetMethodID(cls, "loadMusic", "(Ljava/lang/String;)I");
    if (mID_loadMusic == 0) {
        utils_LogWarn(APP_NAME, "loadMusic null");
    }

    mID_playMusic = env->GetMethodID(cls, "playMusic", "(I)Z");
    if (mID_playMusic == 0) {
        utils_LogWarn(APP_NAME, "playMusic null");
    }

    mID_pauseMusic = env->GetMethodID(cls, "pauseMusic", "(I)Z");
    if (mID_pauseMusic == 0) {
        utils_LogWarn(APP_NAME, "pauseMusic null");
    }

    mID_isPlayingMusic = env->GetMethodID(cls, "isPlayingMusic", "(I)Z");
    if (mID_isPlayingMusic == 0) {
        utils_LogWarn(APP_NAME, "isPlayingMusic null");
    }

    mID_releaseMusic = env->GetMethodID(cls, "releaseMusic", "(I)V");
    if (mID_releaseMusic == 0) {
        utils_LogWarn(APP_NAME, "releaseMusic null");
    }

    utils_LogDebug(APP_NAME, "initIDs_");
}



void support_ReleaseJni() {
    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    env->DeleteGlobalRef(supportJava);
}

const char* support_LoadVertexShader(const char* shader_file) {
    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    jstring filename = env->NewStringUTF(shader_file);

    jstring result = (jstring)env->CallObjectMethod(supportJava,
                                                    mID_getVertexShader, filename);

    if (result == nullptr)
        return nullptr;

    jsize len = env->GetStringUTFLength(result);
    char *shader = new char[len];
    env->GetStringUTFRegion(result, 0, len, shader);

    return shader;
}

const char* support_LoadFragmentShader(const char* shader_file) {
    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    jstring filename = env->NewStringUTF(shader_file);

    jstring result = (jstring)env->CallObjectMethod(supportJava,
                                                    mID_getFragmentShader, filename);

    if (result == nullptr)
        return nullptr;

    jsize len = env->GetStringUTFLength(result);
    char *shader = new char[len];
    env->GetStringUTFRegion(result, 0, len, shader);

    return shader;
}


unsigned char* support_LoadImageFile(const char* file, int* image_width,
                                    int* image_height, bool* has_alpha) {
    if (file == 0)
        return 0;

    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    jstring filename = env->NewStringUTF(file);

    jobject bitmap = env->CallObjectMethod(supportJava, mID_getBitmap, filename);

    AndroidBitmapInfo info_bitmap;
    int ret;
    if ((ret = AndroidBitmap_getInfo(env, bitmap, &info_bitmap)) < 0) {
        utils_LogWarn(APP_NAME, "AndroidBitmap_getInfo() failed ! error=%d", ret);
        return 0;
    }

    if (info_bitmap.format != 1)
        return 0;

    *image_width = static_cast<int>(info_bitmap.width);
    *image_height = static_cast<int>(info_bitmap.height);
    *has_alpha = true;

    void* bitmap_pixels;
    if ((ret = AndroidBitmap_lockPixels(env, bitmap, &bitmap_pixels)) < 0) {
        utils_LogWarn(APP_NAME, "AndroidBitmap_lockPixels() failed ! error=%d", ret);
        return 0;
    }

    int len = info_bitmap.width * info_bitmap.height * 4;
    unsigned char* data = new unsigned char[len];

    memcpy(data, bitmap_pixels, len);

    AndroidBitmap_unlockPixels(env, bitmap);

    env->CallVoidMethod(supportJava, mID_releaseBitmap, bitmap);

    return data;
}


// Function help save game data
const char* support_PathToSaveGameData(const char* file) {
    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    jstring filename = env->NewStringUTF(file);
    jstring result = (jstring)env->CallObjectMethod(supportJava, mID_pathToSaveGameData, filename);
    jsize len = env->GetStringUTFLength(result);
    char *path = new char[len];
    env->GetStringUTFRegion(result, 0, len, path);
    return path;
}


// Functions manage music
int support_LoadMusic(const char* file) {
    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    jstring filename = env->NewStringUTF(file);
    jint result = env->CallIntMethod(supportJava, mID_loadMusic, filename);

    return result;
}

bool support_PlayMusic(int index) {
    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    jboolean result = env->CallBooleanMethod(supportJava, mID_playMusic, index);

    return result;
}

bool support_PauseMusic(int index) {
    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    jboolean result = env->CallBooleanMethod(supportJava, mID_pauseMusic, index);

    return result;
}

bool support_IsPlayingMusic(int index) {
    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    jboolean result = env->CallBooleanMethod(supportJava, mID_isPlayingMusic, index);

    return result;
}

void support_ReleaseMusic(int index) {
    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    env->CallVoidMethod(supportJava, mID_releaseMusic, index);
}


// Functions manage sounds
bool support_InitSounds(int max_num_sounds) {
    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    jboolean result = env->CallBooleanMethod(supportJava, mID_initSounds, max_num_sounds);

    return result;
}

int support_LoadSound(const char* file) {
    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    jstring filename = env->NewStringUTF(file);
    jint result = env->CallIntMethod(supportJava, mID_loadSound, filename);

    return result;
}

bool support_PlaySound(int index) {
    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    jboolean result = env->CallBooleanMethod(supportJava, mID_playSound, index);

    return result;
}

void support_ReleaseSounds() {
    JNIEnv* env;
    jVM->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6);
    if (env == 0) {
        utils_LogWarn(APP_NAME, "JNIEnv null");
    }

    env->CallVoidMethod(supportJava, mID_releaseSounds);
}

