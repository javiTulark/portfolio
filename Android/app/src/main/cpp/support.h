#ifndef __SUPPORT_H_
#define __SUPPORT_H_ 1

#ifdef __cplusplus
extern "C" {
#endif

void support_ReleaseJni();

// Functions load shaders
const char* support_LoadVertexShader(const char* shader_file);
const char* support_LoadFragmentShader(const char* shader_file);

// Function for load images
unsigned char* support_LoadImageFile(const char* file, int* image_width,
                                     int* image_height, bool* has_alpha);


// Function help save game data
const char* support_PathToSaveGameData(const char* file);


// Functions manage music
int support_LoadMusic(const char* file);
bool support_PlayMusic(int index);
bool support_PauseMusic(int index);
bool support_IsPlayingMusic(int index);
void support_ReleaseMusic(int index);


// Functions manage sounds
bool support_InitSounds(int max_num_sounds);
int support_LoadSound(const char* file);
bool support_PlaySound(int index);
void support_ReleaseSounds();




#ifdef __cplusplus
}
#endif

#endif  // __SUPPORT_H_
