/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

// BEGIN_INCLUDE(all)
#include <jni.h>
#include <errno.h>

#include <EGL/egl.h>
#include <GLES2/gl2.h>

#include <android_native_app_glue.h>

#include <unistd.h>

#include "utils.h"
#include "game.h"

/**
 * Our saved state data.
 */
struct SavedState {    // NOT USED
    float angle = 0.0f;
    int32_t x = 0;
    int32_t y = 0;
};

/**
 * Shared state for our app.
 */
struct AppState {
    struct android_app* app = nullptr;

    petancurling::Game* game = nullptr;


    EGLDisplay display = EGL_NO_DISPLAY;
    EGLSurface surface = EGL_NO_SURFACE;
    EGLContext context = EGL_NO_CONTEXT;
    bool firstRender = true;
    EGLConfig config = nullptr;
    EGLint format = -1;

    struct SavedState saved_state;
};

//////

void InitializeDisplay(struct AppState* app_state) {

    const EGLint attribs[] = {
            EGL_SURFACE_TYPE, EGL_OPENGL_ES2_BIT,
            EGL_BLUE_SIZE, 8,
            EGL_GREEN_SIZE, 8,
            EGL_RED_SIZE, 8,
            EGL_DEPTH_SIZE, 8,
            EGL_NONE
    };

    EGLint format;
    EGLint num_configs;
    EGLConfig config;

    if (app_state->display != EGL_NO_DISPLAY) {
        return;
    }

    EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if (display == EGL_NO_DISPLAY) {
        utils_LogError(APP_NAME, "eglGetDisplay() returned error %d",  eglGetError());
    }

    EGLint major, minor;
    if (!eglInitialize(display, &major, &minor)) {
        utils_LogError(APP_NAME, "eglInitialize() returned error %d",  eglGetError());
    }
    utils_LogDebug(APP_NAME, "EGL version %d.%d", major, minor);

    /* We pick the first EGLConfig that matches our criteria */
    if (!eglChooseConfig(display, attribs, &config, 1, &num_configs)) {
        utils_LogError(APP_NAME, "eglChooseConfig() returned error %d",  eglGetError());
    }

    if (num_configs < 1) {
        utils_LogError(APP_NAME, "eglChooseConfig() returned no configs");
    }

    if (!eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format)) {
        utils_LogError(APP_NAME, "eglGetConfigAttrib() returned error %d",  eglGetError());
    }
    utils_LogDebug(APP_NAME, "Chosen window format is %d", format);

    app_state->display = display;
    app_state->config = config;
    app_state->format = format;
}

void TerminateDisplay(struct AppState* app_state) {
    if (app_state->display != EGL_NO_DISPLAY) {

        // Destroy the surface first, if it still exists. This call needs a valid surface.
        if (app_state->surface != EGL_NO_SURFACE) {
            if (!eglDestroySurface(app_state->display, app_state->surface)) {
                utils_LogError(APP_NAME, "eglDestroySurface() returned error %d",  eglGetError());
            }
            app_state->surface = EGL_NO_SURFACE;
        }

        if (!eglMakeCurrent(app_state->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT)) {
            utils_LogError(APP_NAME, "eglMakeCurrent(EGL_NO_CONTEXT) returned error %d",  eglGetError());
        }

        if (!eglTerminate(app_state->display)) {
            utils_LogError(APP_NAME, "eglTerminate() returned error %d",  eglGetError());
        }
    }

    app_state->display = EGL_NO_DISPLAY;
    app_state->config = nullptr;
    app_state->format = -1;
}

void InitializeContext(struct AppState* app_state) {

    EGLint context_attrib[] = {
            EGL_CONTEXT_CLIENT_VERSION, 2,
            EGL_NONE
    };
    EGLContext context;

    if (app_state->context != EGL_NO_CONTEXT) {
        return;
    }

    context = eglCreateContext(app_state->display, app_state->config, EGL_NO_CONTEXT, context_attrib);
    if (context == EGL_NO_CONTEXT) {
        utils_LogError(APP_NAME, "eglCreateContext() returned error %d",  eglGetError());
    }

    app_state->context = context;
}

void TerminateContext(struct AppState* app_state) {
    if (app_state->display != EGL_NO_DISPLAY) {

        app_state->game->teardownGL();

        if (!eglMakeCurrent(app_state->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT)) {
            utils_LogError(APP_NAME, "eglMakeCurrent(EGL_NO_CONTEXT) returned error %d",
                           eglGetError());
        }

        if (app_state->context != EGL_NO_CONTEXT) {
            if (!eglDestroyContext(app_state->display, app_state->context)) {
                utils_LogError(APP_NAME, "eglDestroyContext() returned error %d",  eglGetError());
            }
        }
    }

    app_state->context = EGL_NO_CONTEXT;
}

void CreateSurface(struct AppState* app_state) {
    EGLint w, h;
    EGLSurface surface;

    ANativeWindow_setBuffersGeometry(app_state->app->window, 0, 0, app_state->format);

    const EGLint surfaceAttribs[] = {EGL_NONE};
    surface = eglCreateWindowSurface(app_state->display, app_state->config, app_state->app->window, surfaceAttribs);
    if (surface == EGL_NO_SURFACE) {
        utils_LogError(APP_NAME, "eglCreateWindowSurface() returned error %d",  eglGetError());
    }

    app_state->surface = surface;

    if (!eglMakeCurrent(app_state->display, surface, surface, app_state->context)) {
        utils_LogError(APP_NAME, "eglMakeCurrent() returned error %d", eglGetError());
    }

    if (app_state->firstRender) {
        app_state->firstRender = false;

        eglQuerySurface(app_state->display, surface, EGL_WIDTH, &w);
        eglQuerySurface(app_state->display, surface, EGL_HEIGHT, &h);

        // Initialize OpenGL game state.
        app_state->game->setupGL();
        app_state->game->initialize(w, h);
    }
}

void DestroySurface(struct AppState* app_state) {
    if (app_state->surface != EGL_NO_SURFACE) {
        if (!eglDestroySurface(app_state->display, app_state->surface)) {
            utils_LogError(APP_NAME, "eglDestroySurface() returned error %d",  eglGetError());
        }
    }

    app_state->surface = EGL_NO_SURFACE;
}

/**
 * Initialize an EGL context for the current display.
 */
static void InitializeEGL(struct AppState* app_state) {
    InitializeDisplay(app_state);
    InitializeContext(app_state);
    CreateSurface(app_state);

    utils_LogDebug(APP_NAME, "EGL initialized");
}

/**
 * Tear down the EGL context currently associated with the display.
 */
static void TerminateEGL(struct AppState* app_state) {
    DestroySurface(app_state);
    TerminateContext(app_state);
    TerminateDisplay(app_state);

    utils_LogDebug(APP_NAME, "EGL destroyed");
}


/**
 * Just the current frame in the display.
 */
static void DrawFrame(struct AppState* app_state) {

    if (app_state->display == EGL_NO_DISPLAY ||
            app_state->surface == EGL_NO_SURFACE ||
            app_state->context == EGL_NO_CONTEXT) {
        // EGL not configured.
        return;
    }

    // Render
    app_state->game->render();


    // Swap Buffers
    if (!eglSwapBuffers(app_state->display, app_state->surface)) {
        utils_LogError(APP_NAME, "eglSwapBuffers() returned error %d",  eglGetError());
    }
}


/**
 * Process the next input event.
 */
static int32_t HandleInput(struct android_app* app, AInputEvent* event) {
    struct AppState* app_state = (struct AppState*)app->userData;

    float x = 0;
    float y = 0;

    int32_t type = AInputEvent_getType(event);

    if(type == 1){
        int32_t tmp = AMotionEvent_getAction(event);
        switch (tmp) {
            case AMOTION_EVENT_ACTION_DOWN:
               // utils_LogInfo(APP_NAME, "DOWN ");
                //int32_t AKeyEvent_getKeyCode(const AInputEvent* key_event);
                app_state->game->KeyDown();
                break;
            case AMOTION_EVENT_ACTION_UP:
                app_state->game->KeyUp();
                break;
            case AMOTION_EVENT_ACTION_MOVE:
                app_state->game->KeyUp();
                break;
        }
        return 1;

    }

    if (type == 2) {
        int32_t tmp = AMotionEvent_getAction(event);
        switch (tmp) {
            case AMOTION_EVENT_ACTION_DOWN:
                //utils_LogInfo(APP_NAME, "DOWN ");
                x = AMotionEvent_getX(event, 0);
                y = AMotionEvent_getY(event, 0);
                app_state->game->touchDownAt(x, y);

                break;
            case AMOTION_EVENT_ACTION_UP:
                //utils_LogInfo(APP_NAME, "UP ");
                app_state->game->touchUpAt(x, y);
                break;
            case AMOTION_EVENT_ACTION_MOVE:
                //utils_LogInfo(APP_NAME, "MOVE ");
                x = AMotionEvent_getX(event, 0);
                y = AMotionEvent_getY(event, 0);
                //app_state->game->touchDownAt(x, y);
                break;
        }
        return 1;
    }
    else {
        return 0;
    }
}


/**
 * Process the next main command.
 */
static void HandleCommands(struct android_app* app, int32_t cmd) {
    struct AppState* app_state = (struct AppState*)app->userData;
    switch (cmd) {
        case APP_CMD_SAVE_STATE:
            // The system has asked us to save our current state.  Do so.
            app_state->app->savedState = malloc(sizeof(struct SavedState));
            *((struct SavedState*)app_state->app->savedState) = app_state->saved_state;
            app_state->app->savedStateSize = sizeof(struct SavedState);
            break;

        case APP_CMD_INIT_WINDOW:
            // The window is being shown, get it ready.
            utils_LogInfo(APP_NAME, "APP_CMD_INIT_WINDOW");
            if (app_state->app->window != NULL) {
                InitializeEGL(app_state);
                DrawFrame(app_state);
            }
            break;

        case APP_CMD_TERM_WINDOW:
            // The window is being hidden or closed, clean it up.
            utils_LogInfo(APP_NAME, "APP_CMD_TERM_WINDOW");
            DestroySurface(app_state);
            break;

        case APP_CMD_GAINED_FOCUS:
            // We can use APP_CMD_RESUME
            utils_LogInfo(APP_NAME, "APP_CMD_GAINED_FOCUS");
            app_state->game->resume();
            break;

        case APP_CMD_LOST_FOCUS:
            // We can use APP_CMD_PAUSE
            utils_LogInfo(APP_NAME, "APP_CMD_LOST_FOCUS");
            app_state->game->pause();
            break;

        case APP_CMD_RESUME:
            utils_LogInfo(APP_NAME, "APP_CMD_RESUME");
            break;

        case APP_CMD_PAUSE:
            utils_LogInfo(APP_NAME, "APP_CMD_PAUSE");
            break;
    }
}

void SetupJni(struct android_app* app) {
    utils_LogInfo(APP_NAME, "SetupJni");
    JNIEnv* jni;
    jint res = app->activity->vm->AttachCurrentThread(&jni, 0);
    if (res != JNI_OK) {
        utils_LogWarn(APP_NAME, "Can not attach current thread.");
    }
}


void TeardownJni(struct android_app* app) {
    utils_LogInfo(APP_NAME, "TeardownJni");
    jint res = app->activity->vm->DetachCurrentThread();
    if (res != JNI_OK) {
        utils_LogWarn(APP_NAME, "Can not detattach current thread.");
    }
}


/**
 * This is the main entry point of a native application that is using
 * android_native_app_glue.  It runs in its own thread, with its own
 * event loop for receiving input events and doing other things.
 */
void android_main(struct android_app* app) {
    struct AppState app_state;

    // Make sure glue isn't stripped.
    app_dummy();

    SetupJni(app);

    app_state.app = app;

    app_state.game = new petancurling::Game();

    app->userData = &app_state;
    app->onAppCmd = HandleCommands;
    app->onInputEvent = HandleInput;

    if (app->savedState != NULL) {
        // We are starting with a previous saved state; restore from it.
        app_state.saved_state = *(struct SavedState*)app->savedState;
    }

    // loop waiting for stuff to do.
    while (1) {
        // Read all pending events.
        int ident;
        int events;
        struct android_poll_source* source;

        // We loop until all events are read, then continue
        // to draw the next frame of animation.
        while ((ident=ALooper_pollAll( 0, NULL, &events,
                                       reinterpret_cast<void**>(&source))) >= 0) {
            // Process this event.
            if (source != NULL) {
                source->process(app, source);
            }

            // If a sensor has data, process it now.
            if (ident == LOOPER_ID_USER) {
            }

            // Check if we are exiting.
            if (app->destroyRequested != 0) {
                utils_LogInfo(APP_NAME, "Destroy requested.");
                TerminateEGL(&app_state);
                delete app_state.game;

                app_state.game = NULL;
                TeardownJni(app);
                return;
            }
        }


        app_state.game->update();
        DrawFrame(&app_state);
    }
}
// END_INCLUDE(all)
