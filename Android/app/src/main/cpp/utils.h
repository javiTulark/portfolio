#ifndef __UTILS_H_
#define __UTILS_H_ 1

#ifdef __ANDROID__
#include <stdint.h>
#include <android/log.h>
#endif


#define APP_NAME "Petancurling"


#ifdef __cplusplus
extern "C" {
#endif

double utils_GetTimeSeconds();

void utils_LogInfo(const char *tag, const char *format, ...);
void utils_LogDebug(const char *tag, const char *format, ...);
void utils_LogWarn(const char *tag, const char *format, ...);
void utils_LogError(const char *tag, const char *format, ...);

#ifdef __cplusplus
}
#endif


#endif  // __UTILS_H_
