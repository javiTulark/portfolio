#include "utils.h"

#include <stdarg.h>
#include <time.h>
#include <math.h>



const double utils_kNanosecondsInASecond = 1000000000.0;

double utils_GetTimeSeconds() {
    struct timespec now;
    int ret = clock_gettime(CLOCK_MONOTONIC, &now);
    return static_cast<double>(now.tv_sec) +
           static_cast<double>(now.tv_nsec) / utils_kNanosecondsInASecond;
}



void utils_LogInfo(const char *tag, const char *format, ...) {
    va_list argp;
    va_start(argp, format);
    __android_log_vprint(ANDROID_LOG_INFO, tag, format, argp);
    va_end(argp);
}

void utils_LogDebug(const char *tag, const char *format, ...) {
    va_list argp;
    va_start(argp, format);
    __android_log_vprint(ANDROID_LOG_DEBUG, tag, format, argp);
    va_end(argp);
}

void utils_LogWarn(const char *tag, const char *format, ...) {
    va_list argp;
    va_start(argp, format);
    __android_log_vprint(ANDROID_LOG_WARN, tag, format, argp);
    va_end(argp);
}

void utils_LogError(const char *tag, const char *format, ...) {
    va_list argp;
    va_start(argp, format);
    __android_log_vprint(ANDROID_LOG_ERROR, tag, format, argp);
    va_end(argp);
}




