#include "path.h"
#include "ESAT\draw.h"
#include <iostream>

void IA::Path::Add(Vec2 position) {
	Node n;
	n.pos = position;
	path_.push_back(n);
}

int IA::Path::Length() {
	return path_.size();
}

IA::Vec2 IA::Path::NextPoint() {
	Node n = path_.front();
	path_.pop_front();
	return n.pos;
}

IA::Vec2 IA::Path::getLastPoint() {
	Node n = path_.back();
	return n.pos;
}

IA::Vec2 IA::Path::getFirstPoint() {
	Node n = path_.front();
	return n.pos;
}

bool IA::Path::isEmpty() {
	if (path_.empty())return true;
	else return false;
}

void IA::Path::Clear() {
	path_.clear();
}

void IA::Path::Print() {
	for (Node n : path_) {
		std::cout << "x: " << n.pos.x/8 << " y: " << n.pos.y/8 << std::endl;
	}
	std::cout << "----------------" << std::endl;

}

void IA::Path::Draw(int w, int h, int scale) {
	int ii, jj;
	int aux = 0;
	int aux2 = 0;
	for (Node n : path_) {
		aux++;
	}
	
	for (Node n : path_) {
		ii = n.pos.x;
		jj = n.pos.y;

		float p[10] = { ii, jj,
		ii + scale, jj,
		ii + scale, jj + scale,
		ii, jj + scale,
		ii, jj
		};
		if(aux2 == 0) ESAT::DrawSetFillColor(0, 255, 0, 150);
		else if(aux2 == (aux-1))ESAT::DrawSetFillColor(255, 0, 0, 150);
		else ESAT::DrawSetFillColor(0, 0, 255, 100);
		ESAT::DrawSolidPath(&p[0], 4, false);
		aux2++;
	}
}