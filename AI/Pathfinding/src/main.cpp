#include <ESAT/window.h>
#include <ESAT/draw.h>
#include <ESAT/input.h>
#include <ESAT/time.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <list>
#include "path.h"
#include "cost_map.h"
#include "astar.h"

const float k_fps = 60.0f; //60 frames por segundo

struct Gamestate {
	IA::Path p;
	IA::CostMap m[2];
	IA::AStar a;
	IA::Vec2 origin;
	IA::Vec2 finish;
	ESAT::SpriteHandle h[2];
	bool quit_game_;
	int scale;
	int state;
	int mapstate;
	double time_init;
	double time_finish;
};
Gamestate gs;

void Input() {
	int mouse_x = ESAT::MousePositionX();
	int mouse_y = ESAT::MousePositionY();
	IA::Vec2 pos = { mouse_x / gs.scale, mouse_y / gs.scale };


	if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape)) {
		gs.quit_game_ = true;
	}

	if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Enter)) {
		if (gs.mapstate == 0) gs.mapstate = 1;
		else gs.mapstate = 0;
	}

	switch(gs.state) {
		case 0:
			if (ESAT::MouseButtonDown(0)) {
				if (gs.m[gs.mapstate].isWalkable(pos)) {
					gs.state = 1;
					gs.origin.x = mouse_x / gs.scale;
					gs.origin.y = mouse_y / gs.scale;
				}
				else {
					std::cout << "Invalid location" << std::endl;
				}
			}
			break;
		case 1:
			if (ESAT::MouseButtonDown(0)) {
				if (gs.m[gs.mapstate].isWalkable(pos)) {
					gs.state = 0;
					gs.finish.x = mouse_x / gs.scale;
					gs.finish.y = mouse_y / gs.scale;
					gs.time_init = ESAT::Time();
					gs.a.GeneratePath(gs.origin, gs.finish, &gs.m[gs.mapstate], &gs.p);
				  gs.time_finish = ESAT::Time() - gs.time_init;
				}
				else {
					std::cout << "that location is not valid" << std::endl;
				}
			}
			break;
	}
}

void Update(float timestep) {
}

void Draw() {
	ESAT::DrawBegin();
	ESAT::DrawClear(40, 40, 40, 255);

	ESAT::DrawSprite(gs.h[gs.mapstate], 0, 0);

	gs.p.Draw(128, 128, gs.scale);

	if(gs.mapstate==0)ESAT::DrawSetFillColor(0, 0, 0, 255);
	else ESAT::DrawSetFillColor(255, 255, 255, 255);
	ESAT::DrawText(850, 20, "3IA_ASTAR");
	ESAT::DrawText(20, 1000, "press enter to change map");

	ESAT::DrawEnd();
	ESAT::WindowFrame();
}

void Init() {
	ESAT::WindowInit(1024, 1024);
	srand(time(NULL));
	ESAT::DrawSetTextFont("../../data/xirod.ttf");
	ESAT::DrawSetStrokeColor(0, 0, 0, 255);
	ESAT::DrawSetTextSize(20);
	ESAT::WindowSetMouseVisibility(true);

	gs.mapstate = 0;
	gs.state = 0;
	gs.scale = 8;
	gs.quit_game_ = false;
	gs.h[0] = ESAT::SpriteFromFile("../../data/map_02_terrain.bmp");
	gs.h[1] = ESAT::SpriteFromFile("../../data/map_12_terrain.bmp");
	gs.m[0].LoadImage("../../data/map_02_cost.bmp");
	gs.m[1].Loadtxt("../../data/map_12_cost.txt");
	//gs.m.Print();
}

void Start() {
	float current_time = 0.0f;
	float accum_time = 0.0f;
	float dt = 1000.0f / k_fps;

	while (!gs.quit_game_ && ESAT::WindowIsOpened()) {
		Input();
		accum_time = ESAT::Time() - current_time;
		while (accum_time >= dt) {
			Update(dt);
			current_time += dt;
			accum_time = ESAT::Time() - current_time;
		}
		Draw();
	}
}

void ShutDown() {
	ESAT::WindowDestroy();
}

int ESAT::main(int argc, char** argv) {
	Init();
	Start();
	ShutDown();

	return 0;
}