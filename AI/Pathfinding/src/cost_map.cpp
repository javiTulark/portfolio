// project A Star

#include "cost_map.h"
#include "ESAT\draw.h"
#include <iostream>

IA::CostMap::CostMap() {
	handle_ = nullptr;
}

IA::CostMap::~CostMap() {

}


bool IA::CostMap::Loadtxt(const char* name) {
	isanimageloaded_ = false;
	width_ = 0;
	height_ = 0;
	bool count = true;
	int countX = 0;
	int countY = 0;

	int c;
	FILE *file;
	file = fopen(name, "r");
	if (file) {
		while ((c = getc(file)) != EOF) {
			//	printf("%c ", c);
			if (c == 10) { //enter
				height_++;
				count = false;
			}
			else {
				if (count)width_++;
			}
		}
		fclose(file);
	}

	file = fopen(name, "r");
	if (file) {
		while ((c = getc(file)) != EOF) {
			Cell cell;
			if (c == 32) { //spacebar
				cell.is_walkable_ = true;
				cell.position_.x = countX;
				cell.position_.y = countY;
				cost_map_.push_back(cell);
				countX++;
			}
			else if (c == 10) { //enter
			}
			else {
				cell.is_walkable_ = false;
				cell.position_.x = countX;
				cell.position_.y = countY;
				cost_map_.push_back(cell);
			}
		}
		fclose(file);
	}
	return true;
}

bool IA::CostMap::LoadImage(const char* name) {
	isanimageloaded_ = true;
	unsigned char rgba[4];

	handle_ = ESAT::SpriteFromFile(name);
	width_ = ESAT::SpriteWidth(handle_);
	height_ = ESAT::SpriteHeight(handle_);
//	std::cout << width_ << height_ << std::endl;

	cost_map_.clear();

	int aux;
	for (int i = 0; i <height_ ; i++) {
		for (int j = 0; j < width_; j++) {
			ESAT::SpriteGetPixel(handle_, j, i, rgba);
			aux = j + (width_*i);
			Cell c;
			c.position_.x = j;
			c.position_.y = i;
			c.cost_ = 0;

			if (rgba[0] == 0 || j == 0 || i == 0 || j == (width_ - 1) || i == (height_ - 1))
				c.is_walkable_ = false;
			else
				c.is_walkable_ = true;

			cost_map_.push_back(c);
		}
	}
	//std::cout << cost_map_.size() << std::endl;
	return true;
}

void IA::CostMap::Print() {
	int aux = 0;
	for (int i = 0; i <height_ ; i++) {
		for (int j = 0; j < width_; j++) {
			aux = j + (width_ * i);
			if (cost_map_[aux].is_walkable_ == false)
				printf("%c", 178);
			else
				std::cout << " ";
		}
		std::cout << std::endl;
	}
}

void IA::CostMap::Draw() {
//	if (handle_ != nullptr)	ESAT::DrawSprite(handle_, 0, 0);
	
	int scale = 8;
	int ii, jj;
	int aux = 0;
	for (Cell n : cost_map_) {
	ii = n.position_.x * scale;
	jj = n.position_.y * scale;

	float p[10] = { ii, jj,
	ii + scale, jj,
	ii + scale, jj + scale,
	ii, jj + scale,
	ii, jj
	};

	if(n.is_walkable_== true)
	ESAT::DrawSetFillColor(255, 255, 255, 0);
	else
	ESAT::DrawSetFillColor(0, 0, 0, 255);

	aux++;
	ESAT::DrawSetStrokeColor(100, 100, 100, 200);
	ESAT::DrawSolidPath(&p[0], 4, true);
	}
	
}

int IA::CostMap::getHeight() {
	return height_;
}

int IA::CostMap::getWidth() {
	return width_;
}

bool IA::CostMap::isWalkable(Vec2 pos) {
	int id;
	id = (int)pos.x + (width_ * (int)pos.y);

	if (pos.x > width_)return false;
	if (pos.y > height_)return false;
	if (!cost_map_[id].is_walkable_) return false;
	return true;
}

bool IA::CostMap::canImoveRect(Vec2 pos) {
	int id;
	id = (int)pos.x + (width_ * (int)pos.y);
	if (!cost_map_[id].is_walkable_) return false;
	else return true;
}

bool IA::CostMap::canImoveDiag(Vec2 pos, int dir) {
	int id;
	id = (int)pos.x + (width_ * (int)pos.y);
	if (!cost_map_[id].is_walkable_) return false;

	int a1 = id + 1;
	int a2 = id + width_;
	int a3 = id - 1;
	int a4 = id - width_;


	switch (dir) {
		case 0:
			if (!cost_map_[a1].is_walkable_) return false;
			if (!cost_map_[a2].is_walkable_) return false;
			break;
		case 1:
			if (!cost_map_[a3].is_walkable_) return false;
			if (!cost_map_[a2].is_walkable_) return false;
			break;
		case 2:
			if (!cost_map_[a3].is_walkable_) return false;
			if (!cost_map_[a4].is_walkable_) return false;
			break;
		case 3:
			if (!cost_map_[a1].is_walkable_) return false;
			if (!cost_map_[a4].is_walkable_) return false;
			break;
	}

	return true;
}

