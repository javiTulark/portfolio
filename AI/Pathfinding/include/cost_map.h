// project A Star
// cost_map.h
// author: Toni Barella & Javier Escano
// Artificial Intelligence - 3VGP
// ESAT 2016/2017

#ifndef __COST_MAP__
#define __COST_MAP__ 1

#include "ESAT\sprite.h"
#include "types.h"
#include <vector>


namespace IA {
	typedef float Cost;

	// Use one of the next declarations
	typedef struct cell_s {
		Vec2 position_;
		bool is_walkable_;
		Cost cost_;
	}Cell;

	class CostMap {

		int width_;
		int height_;
		std::vector<Cell> cost_map_;
		ESAT::SpriteHandle handle_;
		bool isanimageloaded_;

	public:
		// Cost map can be a text file or an image file
		bool LoadImage(const char *name);
		bool Loadtxt(const char *name);

		CostMap();
		~CostMap();

		int getWidth();
		int getHeight();

		bool isWalkable(Vec2 pos);

		bool canImoveRect(Vec2 pos);
		bool canImoveDiag(Vec2 pos, int dir);

		void Print(); // Prints the cells of cost_map
		void Draw(); // Draws the cost map's image if available
	};
}
#endif // __COST_MAP__