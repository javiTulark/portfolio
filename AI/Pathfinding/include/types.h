#ifndef __TYPES__
#define __TYPES__ 1


namespace IA {

	enum movement {
		kMovement_init = 0,
		kMovement_movingBoxes,
		kMovement_goToDoor,
		kMovement_goToRest,
		kMovement_goToWork,
		kMovement_random,
		kMovement_seeking,
		kMovement_patrol,
		kMovement_goToBase
	};

	enum state {
		kState_init = 0,
		kState_working,
		kState_suspect,
		kState_alert,
		kState_alarm,
		kState_opening_door
	};

	enum instructions {
		kInstructions_up = 0,
		kInstructions_right,
		kInstructions_down,
		kInstructions_left
	};

	enum zones {
		kZones_null = 0,
		kZones_left_base,
		kZones_right_base,
		kZones_load_area,
		kZones_unload_area,
		kZones_rest_area,
		kZones_left_door,
		kZones_right_door,
		kZones_under_door,
	};

	enum role {
		kRole_undefined = 0,
		kRole_prisioner,
		kRole_guard,
		kRole_soldier
	};


	struct Vec2 {
		float x, y;
	};

	struct Vec3 {
		float x, y, z;
	};

	struct Tile {
		int color;
		Vec2 pos;
		Vec2 size;
	};

	struct Node {
		Vec2 pos;

		int id;
		int father;

		float F; // G+H
		float G; // Coste acumulado
		float H; // Heuristica
		float cost; // Coste en caso de diferentes terrenos
	};
}
#endif // __TYPES__