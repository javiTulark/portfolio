#include "manager.h"

Manager::Manager() :
	timer_(0),
	turn_(1),
	doorL(false),
	doorR(false),
	ALARM(false),
	alarm_timer_(0),
	blockL(false),
	blockR(false),
	inside_(true){


	map_.LoadImage("../../data/map_03_120x88_bw.bmp");

	doorLpos = IA::Vec2{ 18 * 8, (27 * 8) - 1 };
	doorRpos = IA::Vec2{ 102 * 8, (32 * 8) + 2 };

	for (int i = 0; i < 30; i++) {
		working_[i] = true;
		resting_[i] = false;
		seen_[i] = false;
		alive_[i] = true;
		astar_[i].PreProcess(&map_);
	}
	aux1.PreProcess(&map_);
	aux2.PreProcess(&map_);
	aux3.PreProcess(&map_);
	aux4.PreProcess(&map_);

}

Manager::~Manager() {
}

int Manager::getTurn() {
	return turn_;
}

void Manager::updateTurn() {
	timer_++;
	if (timer_ > 1000) {
		timer_ = 0;
		turn_ *= -1;
	}
	if (doorL && doorR)ALARM = true;
//	if (!doorL && !doorR)ALARM = false;

	if (ALARM) {
		alarm_timer_++;
		if (alarm_timer_ >1500)
			ALARM = false;
	}
	else {
		alarm_timer_ = 0;
	}
}
