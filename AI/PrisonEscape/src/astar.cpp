#include "astar.h"
#include "ESAT\time.h"
#include <iostream>

IA::AStar::AStar() {
}

IA::AStar::~AStar() {
}

bool IA::AStar::GeneratePath(Vec2 origin, Vec2 destination, CostMap* map, Path* path) {
	PreProcess(map);
	GeneratePath(origin, destination, path);
	return true;
}

bool IA::AStar::PreProcess(CostMap * map) {
	if (map != nullptr) {
		m_ = map;
		return true;
	}
	else return false;
}

bool IA::AStar::GeneratePath(Vec2 origin, Vec2 destination, Path * path) {
	node_start.id = 0;
	node_start.F = 0;
	node_start.G = 0;
	node_start.H = 0;
	node_start.pos.x = (int)origin.x;
	node_start.pos.y = (int)origin.y;

	node_goal.id = 1;
	node_goal.F = 0;
	node_goal.G = 0;
	node_goal.H = 0;
	node_goal.pos.x = (int)destination.x;
	node_goal.pos.y = (int)destination.y;
	node_start.F = node_start.H;
	node_start.H = DistToTarget(node_goal.pos, node_start.pos);

	//-------------------------------------------------

	idList.clear();
	OpenList.clear();
	ClosedList.clear();
	path->Clear();

	double testTime = ESAT::Time();
	bool end_while = false;
	bool end_draw = true;
	int id = 2;

	//3 Put node_start on the OPEN list
	OpenList.push_back(node_start);

	//4 while the OPEN list is not empty
	while (!OpenList.empty() && !end_while) {

		//6 Get the node off the OPEN list with the lowest f and call it node_current
		node_current = OpenList.front();

		int min = node_current.F;
		for (Node n : OpenList) {
			if (n.F < min) {
				node_current = n;
				min = n.F;
			}
		}
		//OpenList.erase(OpenList.begin + );
		OpenList.remove_if([&](Node n) {return (n.id == node_current.id); });

		//7 If node_current is the same state as node_goal: break from the while loop

	//	if(DistToTarget(node_current.pos, node_goal.pos)<1.0f){
		if (node_current.pos.x == node_goal.pos.x &&
				node_current.pos.y == node_goal.pos.y) {
			node_goal.father = node_current.id;
			end_while = true;
		}
		//printf("\n%d", OpenList.size());

		//8 Generate each state node_successor that can come after node_current
		//9 For each node_successor of node_current
		for (int i = 0; i < 8; i++) {
			node_successor[i].id = id;
			id++;
			bool found = false;
			bool discard = false;
			switch (i) {
				/*
				* 0 1 2
				* 7   3
				* 6 5 4
				*/
				case 0:
					node_successor[i].pos.x = node_current.pos.x - 1;
					node_successor[i].pos.y = node_current.pos.y - 1;
					node_successor[i].G = node_current.G + 1.4f;
					if (!m_->canImoveDiag(node_successor[i].pos, 0)) discard = true; //UL
					break;
				case 1:
					node_successor[i].pos.x = node_current.pos.x;
					node_successor[i].pos.y = node_current.pos.y - 1;
					node_successor[i].G = node_current.G + 1.0f;
					if (!m_->canImoveRect(node_successor[i].pos)) discard = true;
					break;
				case 2:
					node_successor[i].pos.x = node_current.pos.x + 1;
					node_successor[i].pos.y = node_current.pos.y - 1;
					node_successor[i].G = node_current.G + 1.4f;
					if (!m_->canImoveDiag(node_successor[i].pos, 1)) discard = true; //UR
					break;
				case 3:
					node_successor[i].pos.x = node_current.pos.x + 1;
					node_successor[i].pos.y = node_current.pos.y;
					node_successor[i].G = node_current.G + 1.0f;
					if (!m_->canImoveRect(node_successor[i].pos)) discard = true;
					break;
				case 4:
					node_successor[i].pos.x = node_current.pos.x + 1;
					node_successor[i].pos.y = node_current.pos.y + 1;
					node_successor[i].G = node_current.G + 1.4f;
					if (!m_->canImoveDiag(node_successor[i].pos, 2)) discard = true; //DR
					break;
				case 5:
					node_successor[i].pos.x = node_current.pos.x;
					node_successor[i].pos.y = node_current.pos.y + 1;
					node_successor[i].G = node_current.G + 1.0f;
					if (!m_->canImoveRect(node_successor[i].pos)) discard = true;
					break;
				case 6:
					node_successor[i].pos.x = node_current.pos.x - 1;
					node_successor[i].pos.y = node_current.pos.y + 1;
					node_successor[i].G = node_current.G + 1.4f;
					if (!m_->canImoveDiag(node_successor[i].pos, 3)) discard = true; //DL
					break;
				case 7:
					node_successor[i].pos.x = node_current.pos.x - 1;
					node_successor[i].pos.y = node_current.pos.y;
					node_successor[i].G = node_current.G + 1.0f;
					if (!m_->canImoveRect(node_successor[i].pos)) discard = true;
					break;
			}
			//11 Set the cost of node_successor to be the cost of node_current plus the cost to
			// get to node_successor from node_current (done inside the switch)

			//12 Find node_successor on the OPEN list
			for (Node n : OpenList) {
				if (node_successor[i].pos.x == n.pos.x &&
						node_successor[i].pos.y == n.pos.y &&
						!found) {

					//13 If node_successor is on the OPEN list but the existing one is as good or
					// better then discard this successor and continue with next successor
					found = true;
					if (n.G <= node_successor[i].G) {
						discard = true;
					}
				}
			}

			//14 If node_successor is on the CLOSED list but the existing one is as good or
			// better then discard this successor and continue with next successor
			found = false;
			if (!ClosedList.empty() && !discard) {
				for (Node n : ClosedList) {
					if (node_successor[i].pos.x == n.pos.x &&
							node_successor[i].pos.y == n.pos.y &&
							!found) {
						found = true;
						if (n.G <= node_successor[i].G) {
							discard = true;
						}
					}
				}
			}

			if (!discard) {
				//15 Remove occurences of node_successor from OPEN and CLOSED
				int auxX = node_successor[i].pos.x;
				int auxY = node_successor[i].pos.y;
				OpenList.remove_if([&](Node n) {
					return (n.pos.x == auxX &&
									n.pos.y == auxY);
				});
				ClosedList.remove_if([&](Node n) {
					return (n.pos.x == auxX &&
									n.pos.y == auxY);
				});

				//16 Set the parent of node_successor to node_current
				node_successor[i].father = node_current.id;

				//17 Set h to be the estimated distance to node_goal (using the heuristic function)
				node_successor[i].H = DistToTarget(node_goal.pos, node_successor[i].pos);
				node_successor[i].F = node_successor[i].G + node_successor[i].H;

				//18 Add node_successor to the OPEN list
				OpenList.push_back(node_successor[i]);

			}
		}
		//20 Add node_current to the CLOSED list
		ClosedList.push_back(node_current);
	}
	//if (ClosedList.size() == 0) std::cout << "Could not find a path" << std::endl;
	//else std::cout << "Path found" << std::endl;

	int aux_id = node_goal.father;
	idList.push_back(aux_id);

	testTime = ESAT::Time();
	while (aux_id != 0) {
		if (ESAT::Time() - testTime > 100) {
		//	std::cout << "Failed drawing path" << std::endl;
			end_draw = false;
			goto endLoad;
		}
		for (Node n : ClosedList) {
			if (n.id == aux_id) {
				aux_id = n.father;
				idList.push_back(aux_id);
			}
		}
	}
	// Le damos la vuelta a la lista para que coincida con el sentido de la marcha
	idList.reverse();
	for (Node n : ClosedList) {
		for (int i : idList) {
			if (n.id == i) {
				n.pos.x *= 8;
				n.pos.y *= 8;
				path->path_.push_back(n);
			}
		}
	}
	return true;
endLoad:
	return false;
}

float IA::AStar::DistToTarget(Vec2 target, Vec2 origin) {
	return sqrt((origin.x - target.x) * (origin.x - target.x) +
		(origin.y - target.y) * (origin.y - target.y));
}