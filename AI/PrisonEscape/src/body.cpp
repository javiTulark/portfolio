#include "body.h"
#include "math.h"
#include "stdlib.h"
#include "stdio.h"
#include "manager.h"

IA::Body::Body() :
	box_(false),
	speed_(0.06f),
	path_set_(false),
	move_(1),
	RL(true),
	first_door_(true),
	target_id_(0),
	first_alert_(true),
	home_(false),
	almost_there_(false),
	Ddoor_(3),
	determinist_set_(false){
}

IA::Body::~Body() {
}

void IA::Body::initPaths() {
	///--------------------------------------
	if ((pos_.x) > (68*8)) {
		path_to_rest[0] = 77;
		path_to_rest[1] = 48;
		path_to_rest[2] = 77;
		path_to_rest[3] = 50;
		path_to_rest[4] = 71;
		path_to_rest[5] = 57;
		path_to_rest[6] = 71;
		path_to_rest[7] = 59;
	}
	else {
		path_to_rest[0] = 58;
		path_to_rest[1] = 48;
		path_to_rest[2] = 58;
		path_to_rest[3] = 50;
		path_to_rest[4] = 71;
		path_to_rest[5] = 57;
		path_to_rest[6] = 71;
		path_to_rest[7] = 59;
	}
	path_to_rest[8] = 68 + rand() % 28;
	path_to_rest[9] = 60 + rand() % 7;
	for (int i = 0; i < 10; i++) {
		path_to_rest[i] *= 8;
	}
	///--------------------------------------
	if (id_ % 2 == 0) {
		path_to_work[0] = 71;
		path_to_work[1] = 59;
		path_to_work[2] = 71;
		path_to_work[3] = 57;
		path_to_work[4] = 77;
		path_to_work[5] = 50;
		path_to_work[6] = 77;
		path_to_work[7] = 48;


	}
	else {
		path_to_work[0] = 71;
		path_to_work[1] = 59;
		path_to_work[2] = 71;
		path_to_work[3] = 57;
		path_to_work[4] = 58;
		path_to_work[5] = 50;
		path_to_work[6] = 58;
		path_to_work[7] = 48;
	}
	for (int i = 0; i < 8; i++) {
		path_to_work[i] *= 8;
	}
}

void IA::Body::init(Vec2 pos, int id) {
	pos_ = pos;
	target_ = pos_;
	id_ = id;
	updateCircle(&geomety_[0], pos.x, pos.y);
	updateBox(&box_geometry_[0], pos.x, pos.y);
	role_ = Manager::instance().role_[id_];
	initPaths();
	///--------------------------------------
}

void IA::Body::setPath(float* points, int len) {
	path_.Clear();
	for (int i = 0; i < len; i++) {
		path_.Add(Vec2{points[i*2], points[(i*2)+1]});
	}
	path_set_ = true;
}

void IA::Body::normalize(Vec2 & v) {
	float len = sqrt((v.x * v.x) + (v.y * v.y));
	if (len == 0.0f)
		len = 0.0001f;
	v.x /= len;
	v.y /= len;
}

void IA::Body::setTarget(Vec2 target) {
	target_ = target;
}

void IA::Body::getDirection() {
	dir_.x = (target_.x - pos_.x);
	dir_.y = (target_.y - pos_.y);
	normalize(dir_);
}

float IA::Body::distToTarget(Vec2 target, Vec2 origin) {
	return sqrt((origin.x - target.x) * (origin.x - target.x) +
		(origin.y - target.y) * (origin.y - target.y));
}

bool IA::Body::locationReached(Vec2 location) {
	if (distToTarget(location, pos_) < 2.0f) {
		return true;
	}
	else
		return false;
}

void IA::Body::Move(float dt) {
	getDirection();
	pos_.x += dir_.x * dt * speed_ * move_;
	pos_.y += dir_.y * dt * speed_ * move_;
	updateCircle(&geomety_[0], pos_.x, pos_.y);
	if (box_) {
		updateBox(&box_geometry_[0], pos_.x, pos_.y);
		speed_ = 0.03f;
	}
	else {
		speed_ = 0.06f;
	}
}

void IA::Body::setDeterministLocation() {
	switch (role_) {
		case kRole_prisioner:
			DeterministLocations_[0].x = 640 + rand() % 40;
			DeterministLocations_[0].y = 240 + rand() % 80;
			DeterministLocations_[1].x = 400 + rand() % 40;
			DeterministLocations_[1].y = 240 + rand() % 80;
			target_ = DeterministLocations_[0];
			break;
	}
}

void IA::Body::determinist(float dt) {
	switch (role_) {
		case kRole_prisioner:
			if (!determinist_set_) {
				setDeterministLocation();
				determinist_set_ = true;
			}
			if (locationReached(target_)) {
				if (RL) {
					target_ = DeterministLocations_[1];
					RL = false;
					box_ = false;
				}
				else {
					target_ = DeterministLocations_[0];
					RL = true;
					box_ = true;
				}
			}
			break;
		case kRole_guard:

			break;
	}
}

void IA::Body::random(float dt) {
	if (distToTarget(target_, pos_) < 4) {
		target_.x = rand() % 60 + 30;
		target_.y = rand() % 50 + 10;
	}
}

void IA::Body::patrol(float dt) {
	patrol_route_[0] = origin_;
	int valuex = 0;
	int valuey = 0;
	for (int i = 0; i < 4; i++) {
		//	printf("\n%d", directions[i]);

		switch (directions[i]) {
			case kInstructions_up:
				valuey = 1;
				valuex = 0;
				break;
			case kInstructions_right:
				valuex = 1;
				valuey = 0;
				break;
			case kInstructions_down:
				valuey = -1;
				valuex = 0;
				break;
			case kInstructions_left:
				valuex = -1;
				valuey = 0;
				break;
		}
		if (valuex == 1)
			origin_.x += coords[i];
		if (valuex == -1)
			origin_.x -= coords[i];
		if (valuey == 1)
			origin_.y -= coords[i];
		if (valuey == -1)
			origin_.y += coords[i];


		patrol_route_[i].x = origin_.x;
		patrol_route_[i].y = origin_.y;
	}
}

void IA::Body::seeking(float dt) {
	Vec2 aux = Manager::instance().agent[target_id_].b_.pos_;
	aux.x /= 8;
	aux.y /= 8;
	if (distToTarget(Vec2{ pos_.x / 8,pos_.y / 8 }, aux) > 4) {
		Manager::instance().seen_[id_] = false;
		path_set_ = false;
	}

	setTarget(Manager::instance().agent[target_id_].b_.pos_);
	getDirection();
	pos_.x += dir_.x * dt * speed_ * move_;
	pos_.y += dir_.y * dt * speed_ * move_;
	updateCircle(&geomety_[0], pos_.x, pos_.y);
	speed_ = 0.05f;
}

void IA::Body::Astar(float dt) {
	if (!path_set_) {
		if (id_ == 10) {
			if (RL) {
				target_location_.x = 20;
				target_location_.y = 28;
				RL = false;
			}
			else {
				target_location_.x = 101;
				target_location_.y = 34;
				RL = true;
			}
		}
		else {
			do {
				target_location_.x = 40 + rand() % 55;
				target_location_.y = 11 + rand() % 52;
				if (target_location_.y > 57 && target_location_.x < 67)target_location_.x += 26;
				if (target_location_.x < 44 && target_location_.y < 14) {
					target_location_.x = 35;
					target_location_.y = 27;
				}
			} while (!Manager::instance().map_.isWalkable(target_location_));
		}
		path_set_ = true;
		///Danger
		Vec2 smolpos = { pos_.x / 8, pos_.y / 8 };
		if (!Manager::instance().astar_[id_].GeneratePath(smolpos, target_location_, &path_))
			path_set_ = false;
		///EndDanger
		lastPoint_ = path_.getLastPoint();
	}
	if (locationReached(lastPoint_)) {
		path_set_ = false;
	}
}

void IA::Body::goBase(float dt) {
	box_ = false;
	speed_ = 0.06f;
	move_ = 1;

	if (!path_set_ && !almost_there_) {

		target_location_.x = 96 + rand()%4;
		target_location_.y = 33 + rand()%2;
		printf("%d  ", id_);

		path_set_ = true;
		///Danger
		Vec2 smolpos = { pos_.x / 8, pos_.y / 8 };
		if (!Manager::instance().astar_[id_].GeneratePath(smolpos, target_location_, &path_))
			path_set_ = false;
		///EndDanger
		lastPoint_ = path_.getLastPoint();
	}

	
	if (locationReached(lastPoint_) && !almost_there_) {
		almost_there_ = true;
		if (id_ == 0)printf("\n");
		printf("%d  ", id_);
		path_.Clear();
		path_ = Manager::instance().doorAtoBaseA_;
		lastPoint_ = path_.getLastPoint();
	}
	
	if (locationReached(lastPoint_) && almost_there_) {
		move_ = 0;
	}
}

void IA::Body::goRest(float dt) {
	if (!Manager::instance().resting_[id_]) {
		if (!path_set_) {
			setPath(&path_to_rest[0], 5);
			box_ = false;
			Manager::instance().working_[id_] = false;
		}
		if (locationReached(Vec2{ path_to_rest[8] ,path_to_rest[9] })) {
			move_ = 0;
			path_set_ = false;
			Manager::instance().resting_[id_] = true;
		}
	}
}

void IA::Body::goWork(float dt) {
	if (!path_set_) {
		setPath(&path_to_work[0], 4);
	}
	if (locationReached(Vec2{ path_to_work[6] ,path_to_work[7] })) {
		Manager::instance().movement_[id_] = kMovement_determinist;
		path_set_ = false;
		box_ = false;
		Manager::instance().working_[id_] = true;
		Manager::instance().resting_[id_] = false;
	}
	move_ = 1;
}

void IA::Body::update(float dt) {
	if (Manager::instance().alive_[id_]) {
		int movement = Manager::instance().movement_[id_];
		Vec2 door;
		sensorization();

		switch (movement) {
			case kMovement_init:
				if (Manager::instance().ALARM)path_set_ = false;
				path_.Clear();
				break;

				//Basic Movements
				///-----------------------------------------------
			case kMovement_determinist:
				determinist(dt);
				break;
				///-----------------------------------------------
			case kMovement_random:
				random(dt);
				break;
				///-----------------------------------------------
			case kMovement_patrol:
				patrol(dt);
				break;
				///-----------------------------------------------
			case kMovement_seeking:
				seeking(dt);
				break;
				///-----------------------------------------------
			case kMovement_Astar:
				Astar(dt);
				break;
				///-----------------------------------------------
			case kMovement_PreLoadedAstar:
				preloadedAstar(dt);
				break;

				//Special Movements
				///-----------------------------------------------
			case kMovement_goToRest:
				goRest(dt);
				break;
				///-----------------------------------------------
			case kMovement_goToWork:
				goWork(dt);
				break;
				///-----------------------------------------------
			case kMovement_goToBase:
				goBase(dt);
				break;
		}
		
		
///-----------------------------------------------

		if (Manager::instance().movement_[id_] != kMovement_seeking) {
			if (locationReached(target_) && path_.isEmpty() == false)
				setTarget(path_.NextPoint());
			Move(dt);
		}
	}
}

void IA::Body::updateCircle(float * points, float x, float y) {
	float xx = 0.0f;
	float yy = 0.0f;
	float angle = 0.34906584f;

	for (int i = 0; i < 18; ++i) {
		x = cos(i * angle) * 2 + x;
		y = sin(i * angle) * 2 + y;

		points[i * 2] = x;
		points[i * 2 + 1] = y;
	}
}

void IA::Body::updateBox(float * points, float x, float y) {
	points[0] = x - 3;
	points[1] = y - 3;
	points[2] = x + 3;
	points[3] = y - 3;
	points[4] = x + 3;
	points[5] = y + 3;
	points[6] = x - 3;
	points[7] = y + 3;
	points[8] = x - 3;
	points[9] = y - 3;
}

void IA::Body::preloadedAstar(float dt) {
	if (locationReached(lastPoint_)) {
		path_set_ = false;
	}

	if (!path_set_) {

		/// From base Left 
		if (distToTarget(Vec2{ pos_.x / 8,pos_.y / 8 }, Vec2{ 5,83 }) < 5) {
			if (!Manager::instance().doorL) {
				/// to Door Right
				path_ = Manager::instance().baseBtoDoorA_;
			}
			else {
				/// to door Left
				path_ = Manager::instance().BaseBtoDoorB_;
			}
		}

		/// From Door Right
		int aux = rand() % 2;
		if (distToTarget(Vec2{ pos_.x / 8,pos_.y / 8 }, Vec2{ 102,33 }) < (Ddoor_+2)) {
			if (!Manager::instance().doorL && aux==0) {
				/// to Door Left
				path_ = Manager::instance().doorAtoDoorB_;
			}
			else if(aux==1){
				/// From door Right to Base Right
				path_ = Manager::instance().doorAtoBaseA_;
			}
			else {
				///  to base Left
				path_ = Manager::instance().DoorAtoBaseB_;
			}
		}

		/// From door Left to Door Right
		if (distToTarget(Vec2{ pos_.x / 8,pos_.y / 8 }, Vec2{ 19,27 }) <(Ddoor_+2)) {
			if (!Manager::instance().doorL) {
				path_ = Manager::instance().DoorBtoDoorA_;
			}
			else {
				/// From door left to base left
				path_ = Manager::instance().DoorBtoBaseB_;
			}
		}

		/// From base Right to door Right
		if (distToTarget(Vec2{ pos_.x / 8,pos_.y / 8 }, Vec2{ 112,83 }) < (Ddoor_+2)) {
			path_ = Manager::instance().BaseAtoDoorA_;
		}

		path_set_ = true;
		first_door_ = false;
		lastPoint_ = path_.getLastPoint();

	}
}

bool IA::Body::hasBox() {
	if (box_)return true;
	else return false;
}

void IA::Body::sensorization() {
	if (!Manager::instance().ALARM)first_alert_ = true;
	Vec2 door;
	bool found = false;
	switch (role_) {
		case kRole_guard:
			if (Manager::instance().doorL) {
				door = Manager::instance().doorLpos;
				if (role_ == kRole_guard) {
					if (distToTarget(Vec2{ pos_.x / 8, pos_.y / 8 }, Vec2{ door.x / 8,door.y / 8 }) < Ddoor_ &&
							Manager::instance().movement_[id_] != kMovement_seeking) {
						if (Manager::instance().doorL) Manager::instance().ALARM = true;
						Manager::instance().doorL = false;
					}
				}
			}
			if (Manager::instance().doorR) {
				door = Manager::instance().doorRpos;
				if (role_ == kRole_guard) {
					if (distToTarget(Vec2{ pos_.x / 8, pos_.y / 8 }, Vec2{ door.x / 8,door.y / 8 }) < Ddoor_ &&
							Manager::instance().movement_[id_] != kMovement_seeking) {
						if (Manager::instance().doorR) Manager::instance().ALARM = true;
						Manager::instance().doorR = false;
					}
				}
			}
			if (!Manager::instance().ALARM) {
				for (int i = 0; i < 30 && !found; i++) {
					if (Manager::instance().agent[i].m_.role_ == kRole_soldier) {
						Vec2 p = Manager::instance().agent[i].b_.pos_;
						p.x /= 8;
						p.y /= 8;
						if (distToTarget(Vec2{ pos_.x / 8, pos_.y / 8 }, p) < 4) {
							target_id_ = i;
							Manager::instance().seen_[id_] = true;
							found = true;
						}
					}
				}
			}
			else {
				for (int i = 0; i < 30 && !found; i++) {
					if (Manager::instance().agent[i].m_.role_ != kRole_guard) {
						Vec2 p = Manager::instance().agent[i].b_.pos_;
						p.x /= 8;
						p.y /= 8;
						if (distToTarget(Vec2{ pos_.x / 8, pos_.y / 8 }, p) < 4) {
							target_id_ = i;
							Manager::instance().seen_[id_] = true;
							found = true;
						}
					}
				}
			}
			break;

			//------------------------------
		case kRole_soldier:
			if (!Manager::instance().doorL) {
				door = Manager::instance().doorLpos;
				if (distToTarget(Vec2{ pos_.x / 8, pos_.y / 8 }, Vec2{ door.x / 8,door.y / 8 }) < Ddoor_ &&
						pos_.y / 8 > 27) {
					Manager::instance().doorL = true;
				}
			}

			if (!Manager::instance().doorR) {
				door = Manager::instance().doorRpos;
				if (distToTarget(Vec2{ pos_.x / 8, pos_.y / 8 }, Vec2{ door.x / 8,door.y / 8 }) < Ddoor_) {
					Manager::instance().doorR = true;
				}
			}
			break;
	}

}