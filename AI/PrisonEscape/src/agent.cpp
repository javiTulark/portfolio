#include "agent.h"
#include "ESAT/draw.h"
#include "stdio.h"
#include "manager.h"

IA::Agent::Agent() {
}

IA::Agent::~Agent() {
}

void IA::Agent::init(Vec2 pos, int state, int role, int id) {
	id_ = id;
	m_.init(pos, state, role, id);
	b_.init(pos, id);
}

void IA::Agent::update(float dt) {
	m_.update(dt);
	b_.update(dt);
}

void IA::Agent::draw() {
	switch (Manager::instance().role_[id_]) {
		case kRole_prisioner:
			ESAT::DrawSetFillColor(255, 0, 0, 255);
			break;
		case kRole_guard:
			ESAT::DrawSetFillColor(0, 0, 255, 255);
			break;
		case kRole_soldier:
			ESAT::DrawSetFillColor(0, 255, 0, 255);
			break;
	}
	if(!Manager::instance().alive_[id_]) ESAT::DrawSetFillColor(0, 0, 0, 100);

	ESAT::DrawSetStrokeColor(255, 255, 255, 255);
	ESAT::DrawSolidPath(&b_.geomety_[0], 18, true);
	
	if(id_>=10 && id_ < 20)b_.path_.Draw(120, 88, 5);


	if (b_.hasBox()) {
		ESAT::DrawSetFillColor(120, 60, 30, 255);
		ESAT::DrawSolidPath(&b_.box_geometry_[0], 5, true);
	}
}
