#include <ESAT/window.h>
#include <ESAT/draw.h>
#include <ESAT/input.h>
#include <ESAT/time.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <list>
#include "agent.h"
#include "path.h"
#include "cost_map.h"
#include "astar.h"
#include "manager.h"

const float k_fps = 30.0f; //60 frames por segundo
const int k_total_agents = 30;

struct Gamestate {
	ESAT::SpriteHandle h;
	bool quit_game_;
	int scale;
	double time_init;
	double time_finish;
	ESAT::SpriteHandle dLo, dRo;

};
Gamestate gs;

void Input() {
	int mouse_x = ESAT::MousePositionX();
	int mouse_y = ESAT::MousePositionY();
	IA::Vec2 pos = { mouse_x / gs.scale, mouse_y / gs.scale };


	if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Escape)) {
		gs.quit_game_ = true;
	}

	if (ESAT::IsSpecialKeyDown(ESAT::kSpecialKey_Enter)) {
	}


	if (ESAT::MouseButtonDown(0)) {
		std::cout << "x: " << pos.x << " y: " << pos.y << std::endl;
	}

}

void Update(float timestep) {
	Manager::instance().updateTurn();
	for (int i = 0; i < k_total_agents; i++) {
		Manager::instance().agent[i].update(timestep);
	}
}

void Draw() {
	ESAT::DrawBegin();
	ESAT::DrawClear(40, 40, 40, 255);

	ESAT::DrawSprite(gs.h, 0, 0);

	///Work
	ESAT::DrawSetStrokeColor(255, 255, 255, 255);
	ESAT::DrawLine(50 * 8, 30 * 8, 85 * 8, 30 * 8);
	ESAT::DrawLine(50 * 8, 40 * 8, 85 * 8, 40 * 8);
	ESAT::DrawLine(50 * 8, 30 * 8, 50 * 8, 40 * 8);
	ESAT::DrawLine(85 * 8, 30 * 8, 85 * 8, 40 * 8);
	///Load
	ESAT::DrawSetStrokeColor(255, 255, 0, 255);
	ESAT::DrawLine(50 * 8, 30 * 8, 55 * 8, 30 * 8);
	ESAT::DrawLine(50 * 8, 40 * 8, 55 * 8, 40 * 8);
	ESAT::DrawLine(50 * 8, 30 * 8, 50 * 8, 40 * 8);
	ESAT::DrawLine(55 * 8, 30 * 8, 55 * 8, 40 * 8);
	///Unload
	ESAT::DrawSetStrokeColor(255, 0, 255, 255);
	ESAT::DrawLine(80 * 8, 30 * 8, 85 * 8, 30 * 8);
	ESAT::DrawLine(80 * 8, 40 * 8, 85 * 8, 40 * 8);
	ESAT::DrawLine(80 * 8, 30 * 8, 80 * 8, 40 * 8);
	ESAT::DrawLine(85 * 8, 30 * 8, 85 * 8, 40 * 8);

	if (Manager::instance().doorL)ESAT::DrawSprite(gs.dLo, Manager::instance().doorLpos.x, Manager::instance().doorLpos.y);
	if (Manager::instance().doorR)ESAT::DrawSprite(gs.dRo, Manager::instance().doorRpos.x, Manager::instance().doorRpos.y);

	//Manager::instance().map_.Draw();

	ESAT::DrawSetFillColor(255, 255, 255, 255);
	if(Manager::instance().ALARM)ESAT::DrawText(750, 20, "ALARM");
	for (int i = 0; i < k_total_agents; i++) {
		Manager::instance().agent[i].draw();
	}

//	Manager::instance().doorAtoBaseA_.Draw(120,88,8);

	ESAT::DrawEnd();
	ESAT::WindowFrame();
}

void Init() {
	ESAT::WindowInit(960, 704);
	srand(time(NULL));
	ESAT::DrawSetTextFont("../../data/xirod.ttf");
	ESAT::DrawSetStrokeColor(0, 0, 0, 255);
	ESAT::DrawSetTextSize(20);
	ESAT::WindowSetMouseVisibility(true);

	gs.scale = 8;

	for (int id = 0; id < k_total_agents; id++) {
		float x = (50 + rand() % 30) * gs.scale;
		float y = (40 + rand() % 8) * gs.scale;
		if (id < 10) {
			Manager::instance().agent[id].init(IA::Vec2{ x,y },
												IA::kState_working,
												IA::kRole_prisioner,
												id);
		}
		
		else if (id > 9 && id < 20) {
			Manager::instance().agent[id].init(IA::Vec2{ x,y },
												IA::kState_working,
												IA::kRole_guard,
												id);
		}
		
		else {
			x = (3 + rand() % 6) * gs.scale;
			y = (80 + rand() % 7) * gs.scale;
			Manager::instance().agent[id].init(IA::Vec2{ x,y },
												IA::kState_opening_door,
												IA::kRole_soldier,
												id);

		}
	}

	gs.quit_game_ = false;
	gs.h = ESAT::SpriteFromFile("../../data/map_03_960x704_layoutAB.bmp");
	gs.dLo = ESAT::SpriteFromFile("../../data/door_h_open.bmp");
	gs.dRo = ESAT::SpriteFromFile("../../data/door_v_open.bmp");


	Manager::instance().aux1.GeneratePath(IA::Vec2{ 5,83 }, IA::Vec2{ 103,33 }, &Manager::instance().baseBtoDoorA_);
	Manager::instance().aux2.GeneratePath(IA::Vec2{ 103,33 }, IA::Vec2{ 112,83 }, &Manager::instance().doorAtoBaseA_);
	Manager::instance().aux3.GeneratePath(IA::Vec2{ 101,33 }, IA::Vec2{ 19,29 }, &Manager::instance().doorAtoDoorB_);
	Manager::instance().aux1.GeneratePath(IA::Vec2{ 5,83 }, IA::Vec2{ 19,26 }, &Manager::instance().BaseBtoDoorB_);
	Manager::instance().DoorAtoBaseB_ = Manager::instance().baseBtoDoorA_;
	Manager::instance().BaseAtoDoorA_ = Manager::instance().doorAtoBaseA_;
	Manager::instance().DoorBtoDoorA_ = Manager::instance().doorAtoDoorB_;
	Manager::instance().DoorBtoBaseB_ = Manager::instance().BaseBtoDoorB_;
	Manager::instance().DoorAtoBaseB_.path_.reverse();
	Manager::instance().BaseAtoDoorA_.path_.reverse();
	Manager::instance().DoorBtoBaseB_.path_.reverse();

	//gs.m.Print();
}

void Start() {
	float current_time = 0.0f;
	float accum_time = 0.0f;
	float dt = 1000.0f / k_fps;

	while (!gs.quit_game_ && ESAT::WindowIsOpened()) {
		Input();
		accum_time = ESAT::Time() - current_time;
		while (accum_time >= dt) {
			Update(dt);
			current_time += dt;
			accum_time = ESAT::Time() - current_time;
		}
		Draw();
	}
}

void ShutDown() {
	ESAT::WindowDestroy();
}

int ESAT::main(int argc, char** argv) {

	Init();
	Start();
	ShutDown();

	return 0;
}