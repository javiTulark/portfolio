#include "mind.h"
#include <math.h>
#include "manager.h"

IA::Mind::Mind(){

}

IA::Mind::~Mind() {
}

void IA::Mind::init(Vec2 pos, int state, int role, int id) {
	pos_ = pos;
	id_ = id;
	Manager::instance().role_[id_] = role;
	Manager::instance().state_[id_] = state;
}

void IA::Mind::update(float dt) {

	role_ = Manager::instance().role_[id_];
	switch (Manager::instance().state_[id_]) {
		case kState_init:
			break;
			///-----------------------------------------------
		case kState_working:
			switch (role_) {
				case kRole_prisioner:
					if (id_ % 2 == 0) {
						if (Manager::instance().getTurn() > 0)
							Manager::instance().movement_[id_] = kMovement_goToRest;
						else if (Manager::instance().working_[id_] == false)
							Manager::instance().movement_[id_] = kMovement_goToWork;
						else 
							Manager::instance().movement_[id_] = kMovement_determinist;
					}
					else {
						if (Manager::instance().getTurn() < 0)
							Manager::instance().movement_[id_] = kMovement_goToRest;
						else if (Manager::instance().working_[id_] == false)
							Manager::instance().movement_[id_] = kMovement_goToWork;
						else
							Manager::instance().movement_[id_] = kMovement_determinist;
					}
					if (Manager::instance().ALARM && (Manager::instance().alarm_timer_ > (10 * id_)))
						Manager::instance().state_[id_] = kState_alert;

					break;
					///-----------------------------------------------
				case kRole_guard:
					Manager::instance().movement_[id_] = kMovement_Astar;
					if (Manager::instance().seen_[id_]) {
						Manager::instance().movement_[id_] = kMovement_seeking;
						Manager::instance().state_[id_] = kState_suspect;
					}
					break;
					///-----------------------------------------------
				case kRole_soldier:
					break;
			}
			break;
			///-----------------------------------------------
		case kState_suspect:
			switch (role_) {
				case kRole_prisioner:
					break;
				case kRole_guard:
					Manager::instance().movement_[id_] = kMovement_seeking;
					if (!Manager::instance().seen_[id_])
						Manager::instance().state_[id_] = kState_working;
					break;
				case kRole_soldier:
					break;
			}
			break;
			///-----------------------------------------------
		case kState_alert:
			switch (role_) {
				case kRole_prisioner:
					Manager::instance().movement_[id_] = kMovement_goToBase;
					//if(!Manager::instance().ALARM && Manager::instance().inside_)
					//	Manager::instance().movement_[id_] = kState_working;
					break;
				case kRole_guard:
					break;
				case kRole_soldier:
					break;
			}
			break;
			///-----------------------------------------------
		case kState_alarm:
			switch (role_) {
				case kRole_prisioner:
					break;
				case kRole_guard:
					break;
				case kRole_soldier:
					break;
			}
			break;
			///-----------------------------------------------
		case kState_opening_door:
			switch (role_) {
				case kRole_prisioner:
					break;
				case kRole_guard:
					break;
				case kRole_soldier:
					Manager::instance().movement_[id_] = kMovement_PreLoadedAstar;
					break;
			}
			break;

	}
}


float IA::Mind::distToTarget(Vec2 target, Vec2 origin) {
	return sqrt((origin.x - target.x) * (origin.x - target.x) +
		(origin.y - target.y) * (origin.y - target.y));
}