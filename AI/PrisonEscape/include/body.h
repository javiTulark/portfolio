#ifndef __BODY__
#define __BODY__ 1

#include <types.h>
#include "path.h"
#include <thread>

namespace IA {
	class Body {
	public:
		Body();
		~Body();

		/// Methods
		void init(Vec2 pos, int id);
		void update(float dt);

		/// variables
		float geomety_[36];
		float box_geometry_[10];

		bool hasBox();
		Path path_;

	private:
		void Move(float dt);

		//Basic Movements
		void determinist(float dt);
		void random(float dt);
		void patrol(float dt);
		void seeking(float dt);
		void Astar(float dt);
		void preloadedAstar(float dt);

		//Special Movements
		void goBase(float dt);
		void goRest(float dt);
		void goWork(float dt);

		void sensorization();
		void setDeterministLocation();
		void updateCircle(float* points, float x, float y);
		void updateBox(float* points, float x, float y);
		void normalize(Vec2 &v);
		void setTarget(Vec2 target);
		void getDirection();
		float distToTarget(Vec2 target, Vec2 origin);
		bool locationReached(Vec2 location);
		void setPath(float* points, int len);
		void initPaths();

		float path_to_rest[10];
		float path_to_work[8];
		Vec2 patrol_route_[4];
		Vec2 origin_;
		Vec2 pos_;
		Vec2 dir_;
		Vec2 target_;
		Vec2 lastPoint_;
		Vec2 DeterministLocations_[2];
		int directions[4];
		float coords[4];
		bool determinist_set_;
		bool box_;
		Vec2 target_location_;
		int target_id_;
		int id_;
		float speed_;
		bool path_set_;
		int move_;
		bool RL;
		bool first_door_;
		bool first_alert_;
		bool home_;
		bool almost_there_;
		int Ddoor_;
		int role_;
	};
}
#endif // __BODY__