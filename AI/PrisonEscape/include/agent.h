#ifndef __AGENT__
#define __AGENT__ 1

#include <types.h>
#include "body.h"
#include "mind.h"

namespace IA {
	class Agent {
	public:
		Agent();
		~Agent();

		void init(Vec2 pos, int state, int role, int id);
		void update(float dt);
		void draw();

		Body b_;
		Mind m_;
	private:

		int id_;
	};
}
#endif // __AGENT__