#ifndef __MIND__
#define __MIND__ 1

#include <types.h>

namespace IA {
	class Mind {
	public:
		Mind();
		~Mind();

		/// Methods
		void init(Vec2 pos, int state, int role, int id);
		void update(float dt);
		float distToTarget(Vec2 target, Vec2 origin);
		int role_;
		

	private:
		int id_;
		Vec2 pos_;
	};
}
#endif // __MIND__