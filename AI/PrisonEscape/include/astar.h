// project A Star
// astar.h
// author: Toni Barella & Javier Escano
// Artificial Intelligence - 3VGP
// ESAT 2016/2017

#ifndef __ASTAR__
#define __ASTAR__

#include "types.h"
#include "cost_map.h"
#include "path.h"

namespace IA {
	class AStar {
	public:
		AStar();
		~AStar();
		bool GeneratePath(Vec2 origin, Vec2 destination, CostMap *map, Path *path);
		
		bool PreProcess(CostMap *map);
		bool GeneratePath(Vec2 origin, Vec2 destination, Path *path);
		
	private:
		CostMap* m_;

		std::list<Node>OpenList;
		std::list<Node>ClosedList;
		std::list<int>idList;

		Node node_goal;
		Node node_start;
		Node node_current;
		Node node_successor[8];

		float DistToTarget(Vec2 target, Vec2 origin);
	};
}
#endif // __ASTAR__