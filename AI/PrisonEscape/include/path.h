// project A Star
// path.h
// author: Toni Barella & Javier Escano
// Artificial Intelligence - 3VGP
// ESAT 2016/2017

#ifndef __PATH__
#define __PATH__

#include <list>
#include <types.h>

namespace IA {
	class Path {
	public:
		std::list<Node> path_;
		int current_point_;

		void Add(Vec2 position);
		int Length();
		Vec2 NextPoint();
		Vec2 getLastPoint();
		Vec2 getFirstPoint();
		void Clear();
		bool isEmpty();

		void Print(); // Prints the path's positions
		void Draw(int w, int h, int scale); // Prints the path's positions
	};
}
#endif // __PATH__