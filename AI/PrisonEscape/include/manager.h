#ifndef __MANAGER_H__
#define __MANAGER_H__ 1

#include <stdlib.h>
#include "agent.h"
#include "astar.h"
#include "cost_map.h"
#include "path.h"
#include "types.h"

class Manager {
public:

	/// singleton
	static Manager& instance() {
		static Manager* gm_instance = nullptr;
		if (gm_instance == nullptr) {
			gm_instance = new Manager();
		}
		return *gm_instance;
	}

	int getTurn();
	void updateTurn();

	IA::Agent agent[30];
	int state_[30];
	int role_[30];
	int movement_[30];
	bool resting_[30];
	bool working_[30];
	bool seen_[30];
	bool blockL, blockR;
	bool alive_[30];
	int alarm_timer_;
	bool inside_;

	IA::AStar astar_[30];
	IA::CostMap map_;
	IA::Path path_[30];

	IA::AStar aux1, aux2, aux3, aux4;
	IA::Path baseBtoDoorA_;
	IA::Path DoorAtoBaseB_;

	IA::Path doorAtoBaseA_;
	IA::Path BaseAtoDoorA_;

	IA::Path doorAtoDoorB_;
	IA::Path DoorBtoDoorA_;

	IA::Path BaseBtoDoorB_;
	IA::Path DoorBtoBaseB_;

	bool doorL, doorR;
	IA::Vec2 doorLpos, doorRpos;

	bool ALARM;

private:
	Manager();
	~Manager();
	Manager(const Manager& o) = delete;
	void operator=(Manager const&) = delete;

	int timer_;
	int turn_;
};
#endif