#include <ESAT/window.h>
#include <ESAT/draw.h>
#include <ESAT/input.h>
#include <ESAT/time.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

const float PI2 = 6.28318530718f;
const int number_of_agents = 50;
const int kmax_stamina = 2000;

enum state {
	kState_dead = 0,
	kState_sleep,
	kState_awake,
	kState_hunting,
	kState_running,
	kState_patrolling
};

enum control {
	kControl_zero = 0,
	kControl_body = 20,
	kControl_sleep = 150,
	kControl_awake = 250,
	kControl_hunting = 350,
	kControl_running = 400
};

enum player {
	kPlayer_null = 0,
	kPlayer_pasive,
	kPlayer_active
};

struct Vec2 {
	float x;
	float y;
};

float DistToTarget(Vec2 target, Vec2 origin) {
	return sqrt((origin.x - target.x) * (origin.x - target.x) +
	            (origin.y - target.y) * (origin.y - target.y));
}

Vec2 FollowVector(Vec2 target, Vec2 origin) {
	Vec2 v = {(target.x - origin.x), (target.y - origin.y)};
	return v;
}

void Normalize(Vec2& v) {
	float len = sqrt((v.x * v.x) + (v.y * v.y));
	if (len < 0.0001)
		len = 0.0001f;
	v.x /= len;
	v.y /= len;
}

void Circle(float buffer[72], float radius, float posx, float posy) {
	float x = 0.0f;
	float y = 0.0f;
	float angle = PI2 / 36;

	for (int i = 0; i < 36; ++i) {
		x = cos(i * angle) * radius + posx;
		y = sin(i * angle) * radius + posy;

		buffer[i * 2] = x;
		buffer[i * 2 + 1] = y;
	}
}

struct Player {
	Vec2 pos;
	int state;
	float phisical[72];

	void init() {
		pos.x = 0.0f;
		pos.y = 0.0f;
		state = kPlayer_pasive;
		Circle(&phisical[0], 20, pos.x, pos.y);
	}

	void update() {
		pos.x = ESAT::MousePositionX();
		pos.y = ESAT::MousePositionY();
		Circle(&phisical[0], 20, pos.x, pos.y);
		draw();
	}

	void draw() {
		ESAT::DrawSetStrokeColor(255, 255, 255, 255);
		for (int i = 0; i < 35; i ++) {
			ESAT::DrawLine(phisical[i * 2], phisical[i * 2 + 1],
			               phisical[(i + 1) * 2], phisical[(i + 1) * 2 + 1]);
		}
		ESAT::DrawLine(phisical[70], phisical[71], phisical[0], phisical[1]);
	}
};

struct Mind {
	Vec2 target;
	Vec2 patrol_target;
	int state;
	int enemy_state;
	float controlzone;
	//max 4 points around the map to patrol
	float patrol_route[8];
	int patrol_step;
	bool ipatrol;
	bool scared;
};

struct Body {
	Vec2 pos;
	Vec2 dir;
	float phisical[72];
	float control[72];
	float speed;
	int stamina;
	float show_stats;
};

struct Agent {
	unsigned int id;
	Mind mind;
	Body body;

	void init(Player* p) {
		mind.target.x = ESAT::MousePositionX();
		mind.target.y = ESAT::MousePositionY();
		mind.patrol_target.x = 0.0f;
		mind.patrol_target.y = 0.0f;
		mind.state = (rand() % 5) + 1;
		//if (mind.state == kState_hunting)mind.state = kState_awake;
		mind.enemy_state = p->state;
		mind.controlzone = kControl_zero;
		if (rand() % 10 == 0)mind.scared = true;
		else mind.scared = false;
		body.pos.x = (rand() % 1240) + 100;
		body.pos.y = (rand() % 700) + 100;
		body.dir.x = 0.0f;
		body.dir.y = 0.0f;
		body.speed = 2;
		body.stamina = rand() % kmax_stamina;
		body.show_stats = false;

		for (int i = 0; i < 8; i += 2) {
			mind.patrol_route[i] = (rand() % 1240) + 100;
			mind.patrol_route[i + 1] = (rand() % 700) + 100;
		}
		mind.patrol_step = 0;
		if (mind.state == kState_patrolling) mind.ipatrol = true;
		else mind.ipatrol = false;

		Circle(&body.phisical[0], kControl_body, body.pos.x, body.pos.y);
		//Circle(&body.control[0], kControl_sleep, body.pos.x, body.pos.y);
	}

	void update(Player* p) {
		switch (mind.state) {
			case kState_dead:
				mind.controlzone = kControl_zero;
				break;

			case kState_sleep:
				mind.controlzone = kControl_sleep;
				mind.target.x = ESAT::MousePositionX();
				mind.target.y = ESAT::MousePositionY();
				body.stamina += 10;
				if (DistToTarget(mind.target, body.pos) <= mind.controlzone &&
				    body.stamina > 100) {
					if (!mind.scared)
						mind.state = kState_hunting;
					else
						mind.state = kState_running;
				}

				if (body.stamina > kmax_stamina) {
					if (mind.ipatrol) mind.state = kState_patrolling;
					else mind.state = kState_awake;
				}
				break;

			case kState_awake:
				mind.controlzone = kControl_awake;
				mind.target.x = ESAT::MousePositionX();
				mind.target.y = ESAT::MousePositionY();


				if (DistToTarget(mind.target, body.pos) <= mind.controlzone) {
					if (!mind.scared)
						mind.state = kState_hunting;
					else
						mind.state = kState_running;
				}
				if (rand() % 100 == 0) {
					body.dir.x = rand() % 100 - 50;
					body.dir.y = rand() % 100 - 50;
				}

				Normalize(body.dir);
				body.speed = 2;
				body.stamina -= body.speed;
				if (body.stamina < 0)mind.state = kState_sleep;

				if (body.pos.x + body.dir.x * body.speed > 10 &&
				    body.pos.x + body.dir.x * body.speed < 1430) {
					body.pos.x += body.dir.x * body.speed;
				}
				if (body.pos.y + body.dir.y * body.speed > 10 &&
				    body.pos.y + body.dir.y * body.speed < 890) {
					body.pos.y += body.dir.y * body.speed;
				}
				break;

			case kState_hunting:
				mind.controlzone = kControl_hunting;
				mind.enemy_state = p->state;
				mind.target.x = ESAT::MousePositionX();
				mind.target.y = ESAT::MousePositionY();

				if (DistToTarget(mind.target, body.pos) <= mind.controlzone &&
				    mind.enemy_state == kPlayer_pasive) {
					body.dir = FollowVector(mind.target, body.pos);
					Normalize(body.dir);
					body.speed = 5;

					if (DistToTarget(mind.target, body.pos) > 10) {
						//	if ((body.pos.x != p->pos.x) && (body.pos.y != p->pos.y)) {
						body.pos.x += body.dir.x * body.speed;
						body.pos.y += body.dir.y * body.speed;
						body.stamina -= body.speed;
					}
					if (body.stamina < 0)mind.state = kState_sleep;
				}
				if (DistToTarget(mind.target, body.pos) > mind.controlzone) {
					if (mind.ipatrol)
						mind.state = kState_patrolling;
					else
						mind.state = kState_awake;
				}
				break;

			case kState_running:
				mind.controlzone = kControl_running;
				mind.enemy_state = p->state;
				mind.target.x = ESAT::MousePositionX();
				mind.target.y = ESAT::MousePositionY();

				if (DistToTarget(mind.target, body.pos) <= mind.controlzone &&
				    mind.enemy_state == kPlayer_pasive) {
					body.dir = FollowVector(mind.target, body.pos);
					Normalize(body.dir);
					body.speed = 5;
				}

				if (DistToTarget(mind.target, body.pos) > 10) {
					if (body.pos.x + body.dir.x * body.speed > 15 &&
					    body.pos.x + body.dir.x * body.speed < 1425) {
						body.pos.x -= body.dir.x * body.speed;
					}
					if (body.pos.y + body.dir.y * body.speed > 15 &&
					    body.pos.y + body.dir.y * body.speed < 885) {
						body.pos.y -= body.dir.y * body.speed;
					}
					body.stamina -= body.speed;
				}
				if (body.stamina < 0)mind.state = kState_sleep;

				if (DistToTarget(mind.target, body.pos) > mind.controlzone) {
					if (mind.ipatrol)
						mind.state = kState_patrolling;
					else
						mind.state = kState_awake;
				}

				break;

			case kState_patrolling:
				mind.controlzone = kControl_awake;
				mind.target.x = ESAT::MousePositionX();
				mind.target.y = ESAT::MousePositionY();
				mind.patrol_target.x = mind.patrol_route[mind.patrol_step];
				mind.patrol_target.y = mind.patrol_route[mind.patrol_step + 1];
				body.speed = 2;
				body.stamina -= body.speed;
				if (body.stamina < 0)mind.state = kState_sleep;

				body.dir = FollowVector(mind.patrol_target, body.pos);
				Normalize(body.dir);
				body.pos.x += body.dir.x * body.speed;
				body.pos.y += body.dir.y * body.speed;

				if (DistToTarget(mind.patrol_target, body.pos) < 5.0f)
					mind.patrol_step += 2;
				if (mind.patrol_step >= 8)
					mind.patrol_step = 0;

				if (DistToTarget(mind.target, body.pos) <= mind.controlzone)
					mind.state = kState_hunting;
				break;
		}

		Circle(&body.phisical[0], kControl_body, body.pos.x, body.pos.y);
		//Circle(&body.control[0], mind.controlzone, body.pos.x, body.pos.y);
		draw();
	}

	void draw() {

		switch (mind.state) {
			case kState_dead:
				ESAT::DrawSetFillColor(50, 50, 50, 255);
				break;
			case kState_sleep:
				ESAT::DrawSetFillColor(100, 100, 100, 255);
				break;
			case kState_awake:
				ESAT::DrawSetFillColor(0, 0, 255, 255);
				break;
			case kState_hunting:
				ESAT::DrawSetFillColor(255, 0, 0, 255);
				break;
			case kState_running:
				ESAT::DrawSetFillColor(0, 255, 0, 255);
				break;
			case kState_patrolling:
				ESAT::DrawSetStrokeColor(0, 150, 255, 30);
				ESAT::DrawLine(body.pos.x, body.pos.y,
				               mind.patrol_target.x, mind.patrol_target.y);
				ESAT::DrawSetFillColor(0, 150, 255, 255);
				break;
		}
		/*
		char text[80];
		_itoa_s(body.stamina, text, 50, 10);
		ESAT::DrawText(body.pos.x - 17, body.pos.y + 2, text);
		*/

		ESAT::DrawSolidPath(&body.phisical[0], 36, false);
		/*
		for (int i = 0; i < 35; i ++) {
			ESAT::DrawLine(body.phisical[i * 2],
			               body.phisical[i * 2 + 1],
			               body.phisical[(i + 1) * 2],
			               body.phisical[(i + 1) * 2 + 1]);
		}
		ESAT::DrawLine(body.phisical[70], body.phisical[71],
		               body.phisical[0], body.phisical[1]);

		/*
				if (mind.state == kState_hunting)
					ESAT::DrawSetStrokeColor(255, 0, 0, 255);
				for (int i = 0; i < 35; i += 4) {
					ESAT::DrawLine(body.control[i * 2],
					               body.control[i * 2 + 1],
					               body.control[(i + 1) * 2],
					               body.control[(i + 1) * 2 + 1]);
				}
				*/
	}
};

void Update(Player* p, Agent a[number_of_agents]) {
	p->update();
	for (int i = 0; i < number_of_agents; i++) {
		a[i].update(p);
	}
}

void Draw() {
	ESAT::DrawSetTextSize(20);
	ESAT::DrawSetFillColor(50, 50, 50, 255);
	ESAT::DrawText(10, 20, "Dead");
	ESAT::DrawSetFillColor(100, 100, 100, 255);
	ESAT::DrawText(10, 40, "Resting");
	ESAT::DrawSetFillColor(0, 0, 255, 255);
	ESAT::DrawText(10, 60, "Wandering");
	ESAT::DrawSetFillColor(255, 0, 0, 255);
	ESAT::DrawText(10, 80, "Pursuing");
	ESAT::DrawSetFillColor(0, 255, 0, 255);
	ESAT::DrawText(10, 100, "Scared");
	ESAT::DrawSetFillColor(0, 150, 255, 255);
	ESAT::DrawText(10, 120, "patrolling");
	ESAT::DrawSetTextSize(12);
}

int ESAT::main(int 	argc, char** argv) {
	ESAT::WindowInit(1440, 900);
	srand(time(NULL));
	ESAT::DrawSetTextFont("../data/xirod.ttf");
	ESAT::DrawSetStrokeColor(255, 255, 255, 255);

	float timeinit = 0.0f;
	float timefinish = 0.0f;

	Player player;
	player.init();

	Agent a[number_of_agents];

	for (int i = 0; i < number_of_agents; i++) {
		a[i].init(&player);
	}


	while (ESAT::WindowIsOpened()) {
		ESAT::DrawBegin();
		timeinit = ESAT::Time();
		ESAT::DrawClear(20, 20, 20, 255);

		Update(&player, &a[0]);
		Draw();
		if (timefinish < 16) {
			ESAT::Sleep(16 - timefinish);
		}

		ESAT::DrawEnd();
		ESAT::WindowFrame();
	}
	ESAT::WindowDestroy();
	return 0;
}
