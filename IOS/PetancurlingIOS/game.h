//
//  game.h
//  PetancurlingIOS
//
//  Created by Marcos Martí Nacher on 23/10/16.
//  Copyright © 2016 ESAT. All rights reserved.
//

#ifndef __GAME_H__
#define __GAME_H__ 1

#include <stdint.h>

#include <iostream>
#include <fstream>
#include <string>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "glm/gtc/type_ptr.hpp"

namespace petancurling {


    enum State{
        kState_menu = 0,
        kState_reset_position,
        kState_select_rotation,
        kState_set_rotation,
        kState_select_force,
        kState_set_force,
        kState_game,
        kState_pause,
        kState_score
    };
    
    struct vertex{
        float position[3];
        float uv[2];
    };
    
    struct TextureBuffer {
        unsigned int vbo;
        unsigned int ebo;
    };
    
    struct Image{
        glm::vec2 pos;
        glm::vec2 size;
        float rotation;
        bool active;
    };
    
    struct Ball{
        Image img;
        glm::vec2 direction;
        float speed;
        float friction;
    };
    
    struct GameState{
        int state;
        int attempts;
        int score;
    };
    
    struct Scene{
        GameState gs;
        Ball ball[3];
        Image goal;
        Image arrow;
        Image pause;
        Image play;
        Image replay;
        Image grey;
        Image intensity;
        Image gradiant;
        Image logo;
        Image smallgoal[3];
        Image smallgoal2[3];
        Image score;
        Image attempt[3];
        int oldstate_;
    };
    
    struct Touch{
        float x, y;
        bool active;
    };
    
    
class Game {
    public:
    Game();
    ~Game();
        
    void setupGL();
    void initialize(int width, int height);
    void teardownGL();
    
    void touchDownScreen();
    void touchUpScreen();
    void touchPause();

    void pause();
    void resume();

    void update();
    void render();
    
    void SaveData();
    void ReadData();
    
    int getGameState();


    
    private:
    Game(const Game&);
    void operator=(const Game&);
    
    bool is_opengl_init_;
    bool is_paused_;
    double secs_last_update_;
    int width_;
    int height_;
    bool add;
    //FILE* file;
    int var1, var2;
    
    bool touched;
    bool paused;
    
    float DistToTarget(float x1,float y1,float x2, float y2);
    
    Scene s;
    Touch touch;
    bool first_load_;
    
    //Music
    int theme_;
    //Sound
    int bounce_, launch_, success_, fail_;
    
    unsigned int index_[6];
    vertex v_[4];
    
    // GL ES
    int program_handle_;
    int attribute_position_;
    int uv_position_;
    int uniform_mvp_matrix_;
    
    // textures
    void loadTexture(unsigned int* texID, const char* path);
    void compileTexture(TextureBuffer *buffer);
    void drawTexture(float x, float y, float w, float h, float r, unsigned int texID) const;
    void drawArrow(float x, float y, float w, float h, float r, unsigned int texID) const;
    
    // methods load shaders
    bool loadShaders();
    int loadProgram(const char* str_vert_source, const char* str_frag_source);
    int loadShader(const char* str_source, int id_type);
    
    struct TextureBuffer arrowTB, goalTB, pauseTB, greyTB, intensityTB;
    struct TextureBuffer playTB, replayTB, gradiantTB, logoTB, scoreTB;
    unsigned int arrow_id, goal_id, pause_id, grey_id, intensity_id;
    unsigned int play_id, replay_id, gradiant_id, logo_id, score_id;
    
    //3 balls
    struct TextureBuffer RockTB[3];
    unsigned int Rock_id[3];
    //3 attempts
    struct TextureBuffer attemptTB[3];
    unsigned int attempt_id[3];
    //3 goals
    struct TextureBuffer smallGoalTB[3];
    unsigned int smallGoal_id[3];
};
    
}  // namespace petancurling

#endif  // __GAME_H__
