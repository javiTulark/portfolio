//
//  support.m
//  PetancurlingIOS
//
//  Created by Marcos Martí Nacher on 22/10/16.
//  Copyright © 2016 ESAT. All rights reserved.
//

#include "support.h"

#import "PetancurlingIOS-Swift.h"

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <UIKit/UIImage.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVAudioPlayer.h>


#include "utils.h"


typedef enum {
	kGLTexturePixelFormat_Automatic = 0,
	kGLTexturePixelFormat_RGBA8888,
	kGLTexturePixelFormat_RGB565,
	kGLTexturePixelFormat_A8,
} GLTexturePixelFormat;

const char* support_PathForResource(const char* resource) {
	NSString* file =[NSString stringWithUTF8String:resource];
	
	NSString* fileName = [[file lastPathComponent] stringByDeletingPathExtension];
	NSString* fileType = [file pathExtension];
	
	NSString* path = [[NSBundle mainBundle] pathForResource:fileName ofType:fileType];
	if (path == nil) {
		utils_LogDebug("Petancurling", "Could not find file resource '%@.%@'", fileName, fileType);
		return NULL;
	}
	
	return [path UTF8String];
}

const char* support_ContentsOfFile(const char* file) {
	NSString *fileNS =[NSString stringWithUTF8String:file];
	NSString *sourceNS = [NSString stringWithContentsOfFile:fileNS encoding:NSUTF8StringEncoding error:nil];
	char* tmp = (char *)[sourceNS UTF8String];
	int length = [sourceNS lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
	char *source = (char*)calloc(length+1, 1);
	strncpy(source, tmp, length);
	
	return source;
}

const char* support_LoadVertexShader(const char* shader_file) {
	const char *file = support_PathForResource(shader_file);
	return support_ContentsOfFile(file);
}

const char* support_LoadFragmentShader(const char* shader_file) {
	const char *file = support_PathForResource(shader_file);
	return support_ContentsOfFile(file);
}

unsigned char* support_LoadImageFile(const char* file, int* image_width, int* image_height, bool* has_alpha)
{
	NSUInteger				width, height;
	CGContextRef			cgContext = nil;
	void*					data = NULL;
	CGColorSpaceRef			colorSpace;
	BOOL					hasAlpha;
	CGImageAlphaInfo		info;
	GLTexturePixelFormat    pixelFormat;
	CGImageRef				image;
	
	NSString *fileNS =[NSString stringWithUTF8String:file];
	NSString *fileName = [[fileNS lastPathComponent] stringByDeletingPathExtension];
	NSString *fileType = [fileNS pathExtension];
	NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:fileType];
	if (path == nil) {
		utils_LogDebug("Petancurling", "ERROR - Could not find image file '%@.%@'", fileName, fileType);
		return NULL;
	}
	
	UIImage *uiImage = [UIImage imageWithContentsOfFile:path];
	image = [uiImage CGImage];
	if(image == NULL) {
		utils_LogDebug("Petancurling", "Image is Null");
		return NULL;
	}
	
	info = CGImageGetAlphaInfo(image);
	
	hasAlpha = ((info == kCGImageAlphaPremultipliedLast) || (info == kCGImageAlphaPremultipliedFirst) || (info == kCGImageAlphaLast) || (info == kCGImageAlphaFirst) ? YES : NO);
	
	if(CGImageGetColorSpace(image)) {
		if(hasAlpha) {
			pixelFormat = kGLTexturePixelFormat_RGBA8888;
		} else {
			pixelFormat = kGLTexturePixelFormat_RGB565;
		}
	} else {  //NOTE: No colorspace means a mask image
		return NULL;
	}
	
	width = CGImageGetWidth(image);
	height = CGImageGetHeight(image);
	
	switch(pixelFormat) {
		case kGLTexturePixelFormat_RGBA8888:
			colorSpace = CGColorSpaceCreateDeviceRGB();
			data = malloc(height * width * 4);
			cgContext = CGBitmapContextCreate(data, width, height, 8, 4 * width, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
			CGColorSpaceRelease(colorSpace);
			break;
		case kGLTexturePixelFormat_RGB565:
			colorSpace = CGColorSpaceCreateDeviceRGB();
			data = malloc(height * width * 4);
			cgContext = CGBitmapContextCreate(data, width, height, 8, 4 * width, colorSpace, kCGImageAlphaNoneSkipLast | kCGBitmapByteOrder32Big);
			CGColorSpaceRelease(colorSpace);
			break;
		default:
			[NSException raise:NSInternalInconsistencyException format:@"Invalid pixel format"];
	}
	
	CGContextClearRect(cgContext, CGRectMake(0, 0, width, height));
	
	CGContextDrawImage(cgContext, CGRectMake(0, 0, CGImageGetWidth(image), CGImageGetHeight(image)), image);
	
	CGContextRelease(cgContext);
	
	*image_width = (int)width;
	*image_height = (int)height;
	*has_alpha = true;
	
	return (unsigned char*)data;
}

void* support_LoadOpenALAudioData(const char* file, ALsizei *out_data_size, ALenum *out_data_format, ALsizei* out_sample_rate) {
	OSStatus                        err = noErr;
	UInt64                          fileDataSize = 0;
	AudioStreamBasicDescription     theFileFormat;
	UInt32                          thePropertySize = sizeof(theFileFormat);
	AudioFileID                     afid = 0;
	void*                           theData = NULL;
	
	NSString *fileNS =[NSString stringWithUTF8String:file];
	NSString *fileName = [[fileNS lastPathComponent] stringByDeletingPathExtension];
	NSString *fileType = [fileNS pathExtension];
	NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:fileType];
	if (path == nil) {
		utils_LogDebug("Petancurling", "LoadOpenALAudioData: Could not find file '%@.%@'", fileName, fileType);
		return NULL;
	}
	CFURLRef fileURL = (__bridge CFURLRef)[NSURL fileURLWithPath:path];
	
	// Open a file with ExtAudioFileOpen()
	err = AudioFileOpenURL(fileURL, kAudioFileReadPermission, 0, &afid);
	if(err) {
		utils_LogDebug("Petancurling", "LoadOpenALAudioData: AudioFileOpenURL FAILED, Error = %ld\n", err);
		if (afid) AudioFileClose(afid);
		return NULL;
	}
	
	// Get the audio data format
	err = AudioFileGetProperty(afid, kAudioFilePropertyDataFormat, &thePropertySize, &theFileFormat);
	if(err) {
		utils_LogDebug("Petancurling", "LoadOpenALAudioData: AudioFileGetProperty(kAudioFileProperty_DataFormat) FAILED, Error = %ld\n", err);
		if (afid) AudioFileClose(afid);
		return NULL;
	}
	
	if (theFileFormat.mChannelsPerFrame > 2)  {
		utils_LogDebug("Petancurling", "LoadOpenALAudioData - Unsupported Format, channel count is greater than stereo\n");
		if (afid) AudioFileClose(afid);
		return NULL;
	}
	
	if ((theFileFormat.mFormatID != kAudioFormatLinearPCM) || (!TestAudioFormatNativeEndian(theFileFormat))) {
		utils_LogDebug("Petancurling", "LoadOpenALAudioData - Unsupported Format, must be little-endian PCM\n");
		if (afid) AudioFileClose(afid);
		return NULL;
	}
	
	if ((theFileFormat.mBitsPerChannel != 8) && (theFileFormat.mBitsPerChannel != 16)) {
		utils_LogDebug("Petancurling", "LoadOpenALAudioData - Unsupported Format, must be 8 or 16 bit PCM\n");
		if (afid) AudioFileClose(afid);
		return NULL;
	}
	
	
	thePropertySize = sizeof(fileDataSize);
	err = AudioFileGetProperty(afid, kAudioFilePropertyAudioDataByteCount, &thePropertySize, &fileDataSize);
	if(err) {
		utils_LogDebug("Petancurling", "LoadOpenALAudioData: AudioFileGetProperty(kAudioFilePropertyAudioDataByteCount) FAILED, Error = %ld\n", err);
		if (afid) AudioFileClose(afid);
		return NULL;
	}
	
	// Read all the data into memory
	UInt32 dataSize = (UInt32)fileDataSize;
	theData = malloc(dataSize);
	if (theData)
	{
		AudioFileReadBytes(afid, false, 0, &dataSize, theData);
		if(err == noErr)
		{
			// success
			*out_data_size = (ALsizei)dataSize;
			*out_data_format = (theFileFormat.mChannelsPerFrame > 1) ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16;
			*out_sample_rate = (ALsizei)theFileFormat.mSampleRate;
		}
		else
		{
			// failure
			free (theData);
			theData = NULL; // make sure to return NULL
			utils_LogDebug("Petancurling", "LoadOpenALAudioData: ExtAudioFileRead FAILED, Error = %ld\n", err);
			if (afid) AudioFileClose(afid);
			return NULL;
		}
	}
	
	// Dispose the ExtAudioFileRef, it is no longer needed
	if (afid) AudioFileClose(afid);
	return theData;
}


// Function help save game data
const char* support_PathToSaveGameData(const char* file) {
	NSString* fileNS =[NSString stringWithUTF8String:file];
	NSString *path =  [Support PathToSaveGameData:fileNS];
	if (path == nil) {
		utils_LogDebug("Petancurling", "Could not find file in documents '%@'", fileNS);
		return NULL;
	}
    
	return [path UTF8String];
	
}



int support_LoadMusic(const char* file) {
  NSString* fileNS =[NSString stringWithUTF8String:file];
  return [Support LoadMusic:fileNS];
}

bool support_PlayMusic(unsigned int index) {
  return [Support PlayMusic:index];
}

bool support_PauseMusic(unsigned int index) {
  return [Support PauseMusic:index];
}

bool support_IsPlayingMusic(unsigned int index) {
  return [Support IsPlayingMusic:index];
}

void support_ReleaseMusic(unsigned int index) {
  [Support ReleaseMusic:index];
}

// Functions manage sounds
bool support_InitSounds(unsigned int max_num_sounds) {
  return [Support InitSounds:max_num_sounds];
}

int support_LoadSound(const char* file) {
  NSString* fileNS =[NSString stringWithUTF8String:file];
  return [Support LoadSound:fileNS];
}

bool support_PlaySound(unsigned int index) {
  return [Support PlaySound:index];
}

void support_ReleaseSounds() {
  [Support ReleaseSounds];
}
