//
//  GameWrapper.h
//  PetancurlingIOS
//
//  Created by Marcos Martí Nacher on 22/10/16.
//  Copyright © 2016 ESAT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameWrapper : NSObject

- (void)setupGL;
- (void)tearDownGL;

- (void)initializeWithWidth:(float)width andHeight:(float)height;

- (void)touchDownScreen;
- (void)touchUpScreen;
- (void)touchDownArrow;
- (void)touchUpArrow;
- (void)movePlayerLeft_;
- (void)movePlayerRight_;
- (void)rotatePlayerLeft_;
- (void)rotatePlayerRight_;
- (int)getEstadoJuego_;

- (void)pause;
- (void)resume;

- (void)update;
- (void)render;

@end

