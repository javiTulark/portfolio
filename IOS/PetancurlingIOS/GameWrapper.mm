//
//  GameWrapper.mm
//  PetancurlingIOS
//
//  Created by Marcos Martí Nacher on 22/10/16.
//  Copyright © 2016 ESAT. All rights reserved.
//

#import "GameWrapper.h"

#include "game.h"

@interface GameWrapper () {
	void* _game;
}
@end


@implementation GameWrapper

- (id) init
{
	self = [super init];
	if(self)
	{
		_game = new petancurling::Game();
        
	}
	return self;
}

- (void) dealloc
{
	delete (petancurling::Game*)_game;
}

- (void)setupGL
{
	((petancurling::Game*)_game)->setupGL();
}

- (void)tearDownGL
{
	((petancurling::Game*)_game)->teardownGL();
}

- (void)initializeWithWidth:(float)width andHeight:(float)height
{
	((petancurling::Game*)_game)->initialize(width, height);
}

- (void)touchDownScreen
{
	((petancurling::Game*)_game)->touchDownScreen();
}

- (void)touchUpScreen
{
	((petancurling::Game*)_game)->touchUpScreen();
}

- (void)touchPause
{
  ((petancurling::Game*)_game)->touchPause();
}
- (int)getGameState
{
    return ((petancurling::Game*)_game)->getGameState();
}

- (void)pause
{
	((petancurling::Game*)_game)->pause();
}

- (void)resume
{
	((petancurling::Game*)_game)->resume();
}

- (void)update
{
	((petancurling::Game*)_game)->update();
}

- (void)render
{
	((petancurling::Game*)_game)->render();
}

@end
