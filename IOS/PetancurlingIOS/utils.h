//
//  utils.h
//  PetancurlingIOS
//
//  Created by Marcos Martí Nacher on 22/10/16.
//  Copyright © 2016 ESAT. All rights reserved.
//

#ifndef __UTILS_H__
#define __UTILS_H__ 1

#ifdef __ANDROID__
#include <stdint.h>
#include <android/log.h>
#endif


#ifdef __cplusplus
extern "C" {
#endif
	
double utils_GetTimeSeconds();

void utils_LogInfo(const char *tag, const char *format, ...);
void utils_LogDebug(const char *tag, const char *format, ...);
void utils_LogWarn(const char *tag, const char *format, ...);
void utils_LogError(const char *tag, const char *format, ...);
	
#ifdef __cplusplus
}
#endif


#endif  // __UTILS_H__
