//
//  game.cc
//  PetancurlingIOS
//
//  Created by Marcos Martí Nacher on 23/10/16.
//  Copyright © 2016 ESAT. All rights reserved.
//

#include "game.h"

#ifdef __ANDROID__
#include <GLES2/gl2.h>
#else
#include <OpenGLES/ES2/gl.h>

#include <stddef.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#endif

using namespace std;


#include "support.h"
#include "utils.h"
#define DEGREES_TO_RADIANS(x) (3.14159265358979323846 * x / 180.0)

namespace petancurling {
  
  Game::Game()
		: is_opengl_init_(false),
  is_paused_(false),
  secs_last_update_(0.0),
  width_(0),
  height_(0),
  program_handle_(0),
  attribute_position_(0),
  uniform_mvp_matrix_(0) {//utils_LogInfo("Petancurling", "Game::Game()");
    
    
    // variables
    index_[0] = 0;
    index_[1] = 1;
    index_[2] = 2;
    index_[3] = 0;
    index_[4] = 2;
    index_[5] = 3;
    
    v_[0] = {{0.0f, 0.0f, 0.0f},{0.0f, 1.0f}};
    v_[1] = {{0.0f, 1.0f, 0.0f},{0.0f, 0.0f}};
    v_[2] = {{1.0f, 1.0f, 0.0f},{1.0f, 0.0f}};
    v_[3] = {{1.0f, 0.0f, 0.0f},{1.0f, 1.0f}};
  }
  
  Game::~Game() {
    utils_LogInfo("Petancurling", "Game::~Game()");
  }
  
  void Game::setupGL() {
    utils_LogInfo("Petancurling", "Game::setupGL()");
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glClearColor(0.65f, 0.65f, 0.65f, 1.0f);
    
    loadShaders();
    
    is_opengl_init_ = true;
  }
  
  void Game::teardownGL() {
    if (is_opengl_init_ == true) {
      is_opengl_init_ = false;
      
      if (program_handle_) {
        glDeleteProgram(program_handle_);
        program_handle_ = 0;
      }
      support_ReleaseMusic(theme_);
      support_ReleaseSounds();
      
      glDeleteBuffers(1, &goalTB.vbo);
      glDeleteBuffers(1, &goalTB.ebo);
      glDeleteBuffers(1, &arrowTB.vbo);
      glDeleteBuffers(1, &arrowTB.ebo);
      glDeleteBuffers(1, &pauseTB.vbo);
      glDeleteBuffers(1, &pauseTB.ebo);
      glDeleteBuffers(1, &greyTB.vbo);
      glDeleteBuffers(1, &greyTB.ebo);
      glDeleteBuffers(1, &intensityTB.vbo);
      glDeleteBuffers(1, &intensityTB.ebo);
      glDeleteBuffers(1, &playTB.vbo);
      glDeleteBuffers(1, &playTB.ebo);
      glDeleteBuffers(1, &replayTB.vbo);
      glDeleteBuffers(1, &replayTB.ebo);
      glDeleteBuffers(1, &gradiantTB.vbo);
      glDeleteBuffers(1, &gradiantTB.ebo);
      glDeleteBuffers(1, &logoTB.vbo);
      glDeleteBuffers(1, &logoTB.ebo);
      glDeleteBuffers(1, &scoreTB.vbo);
      glDeleteBuffers(1, &scoreTB.ebo);
      
      for(int i=0; i<3; i++){
        glDeleteBuffers(1, &RockTB[i].vbo);
        glDeleteBuffers(1, &RockTB[i].ebo);
        glDeleteBuffers(1, &smallGoalTB[i].vbo);
        glDeleteBuffers(1, &smallGoalTB[i].ebo);
        glDeleteBuffers(1, &attemptTB[i].vbo);
        glDeleteBuffers(1, &attemptTB[i].ebo);
      }
      
      glDeleteTextures(1, &arrow_id);
      glDeleteTextures(1, &goal_id);
      glDeleteTextures(1, &pause_id);
      glDeleteTextures(1, &grey_id);
      glDeleteTextures(1, &intensity_id);
      glDeleteTextures(1, &play_id);
      glDeleteTextures(1, &replay_id);
      glDeleteTextures(1, &gradiant_id);
      glDeleteTextures(1, &logo_id);
      glDeleteTextures(1, &score_id);
      glDeleteTextures(3, &Rock_id[0]);
      glDeleteTextures(3, &attempt_id[0]);
      glDeleteTextures(3, &smallGoal_id[0]);
    }
  }
  
  void Game::initialize(int width, int height) {
    width_ = width;
    height_ = height;
    
    srand(time(NULL));
    
    // reset timer
    secs_last_update_ = 0.0;
    
    //Gamestate
    s.gs.state = kState_menu;
    s.gs.score = 0;
    s.gs.attempts = 0;
    
    
    //Music
    theme_ = support_LoadMusic("theme");
    support_PlayMusic(theme_);
    
    //Sound
    support_InitSounds(4);
    bounce_ = support_LoadSound("bounce");
    launch_ = support_LoadSound("launch");
    success_ = support_LoadSound("success");
    fail_ = support_LoadSound("fail");
    
    touch.active = false;
    touched=false;
    
    for(int i=0; i<3; i++) {
      //Balls
      s.ball[i].img.pos.x = rand() % (width_ - 70) + 10;
      s.ball[i].img.pos.y = 105.0f;
      s.ball[i].img.size.x = 50.0f;
      s.ball[i].img.size.y = 50.0f;
      s.ball[i].direction.x = 1.0f;
      s.ball[i].direction.y = 1.0f;
      s.ball[i].speed = 0.0f;
      s.ball[i].friction = 0.06f;
      s.ball[i].img.rotation = 0.0f;
      loadShaders();
      loadTexture(&Rock_id[i], "rockRed.png");
      compileTexture(&RockTB[i]);
      
      //small goals used for score
      s.smallgoal[i].size.x = 40;
      s.smallgoal[i].size.y = 40;
      s.smallgoal[i].pos.x = 50;
      s.smallgoal[i].pos.y = (height_-20) - 50*i;
      s.smallgoal[i].rotation = 0;
      loadTexture(&smallGoal_id[i], "goal.png");
      compileTexture(&smallGoalTB[i]);
      
      //small balls used for attempts
      s.attempt[i].size.x = 40;
      s.attempt[i].size.y = 40;
      s.attempt[i].pos.x = 5;
      s.attempt[i].pos.y = (height_-120) + 50*i;
      s.attempt[i].rotation = 0;
      loadTexture(&attempt_id[i], "rockGreen.png");
      compileTexture(&attemptTB[i]);
    }
    
    //Score
    s.score.size.x = 200.0f;
    s.score.size.y = 100.0f;
    s.score.pos.x = (width_*0.5f)-100;
    s.score.pos.y = (height_*0.5f)-100;
    s.score.rotation = 0.0f;
    loadTexture(&score_id, "score.png");
    compileTexture(&scoreTB);
    
    
    //Goal
    s.goal.size.x = 300.0f;
    s.goal.size.y = 300.0f;
    s.goal.pos.x = (width_*0.5f)-150;
    s.goal.pos.y = (height_*0.5f)+370;
    s.goal.rotation = 0.0f;
    loadTexture(&goal_id, "goal.png");
    compileTexture(&goalTB);
    
    //Logo
    s.logo.size.x = 426.0f;
    s.logo.size.y = 206.0f;
    s.logo.pos.x = (width_*0.5f)-218;
    s.logo.pos.y = (height_*0.5f)-150.0f;
    s.logo.rotation = 0.0f;
    loadTexture(&logo_id, "logo.png");
    compileTexture(&logoTB);
    
    //Direction Arrow
    s.arrow.size.x = 100.0f;
    s.arrow.size.y = 50.0f;
    s.arrow.pos.x = s.ball[0].img.pos.x+100;
    s.arrow.pos.y = s.ball[0].img.pos.y;
    s.arrow.rotation = 0;
    loadTexture(&arrow_id, "arrow.png");
    compileTexture(&arrowTB);
    
    //Pause Button
    s.pause.size.x = 70.0f;
    s.pause.size.y = 70.0f;
    s.pause.pos.x = width_-115;
    s.pause.pos.y = height_;
    s.pause.rotation = 0.0f;
    loadTexture(&pause_id, "pause.png");
    compileTexture(&pauseTB);
    
    // Play button
    s.play.size.x = 200.0f;
    s.play.size.y = 200.0f;
    s.play.pos.x = (width_*0.5f)-100;
    s.play.pos.y = (height_*0.5f)+100;
    s.play.rotation = 0.0f;
    loadTexture(&play_id, "play.png");
    compileTexture(&playTB);
    
    // Replay button
    s.replay.size.x = 100.0f;
    s.replay.size.y = 100.0f;
    s.replay.pos.x = (width_*0.5f)-50;
    s.replay.pos.y = (height_*0.5f);
    s.replay.rotation = 0.0f;
    loadTexture(&replay_id, "replay.png");
    compileTexture(&replayTB);
    
    // Start location
    s.grey.size.x = width_ - 50.0f;
    s.grey.size.y = 80.0f;
    s.grey.pos.x = 25.0f;
    s.grey.pos.y = 120.0f;
    s.grey.rotation = 0.0f;
    loadTexture(&grey_id, "grey.png");
    compileTexture(&greyTB);
    
    // Intensity selector
    s.intensity.size.x = 40.0f;
    s.intensity.size.y = 30.0f;
    s.intensity.pos.x = width_ -45.0f;
    s.intensity.pos.y = 150.0f;
    s.intensity.rotation = 0.0f;
    loadTexture(&intensity_id, "grey.png");
    compileTexture(&intensityTB);
    
    // Gradiant colors
    s.gradiant.size.x = 30.0f;
    s.gradiant.size.y = height_ - 150.0f;
    s.gradiant.pos.x = width_ -40.0f;
    s.gradiant.pos.y = height_-25.0f;
    s.gradiant.rotation = 0.0f;
    loadTexture(&gradiant_id, "gradiant.png");
    compileTexture(&gradiantTB);
    
    
   // resume();
  }
  
  void Game::touchDownScreen() {
    //if(!touched)
    utils_LogDebug("Petancurling", "---------------------IM touching-------------");
    touched=true;
  }
  
  void Game::touchPause(){
    if(paused)paused=false;
    else paused=true;
  }
  
  void Game::touchUpScreen() {
    touched=false;
  }
  
  int Game::getGameState() {
    return s.gs.state;
  }
  
  
  void Game::pause() {
    utils_LogInfo("Petancurling", "Game::pause()");
    is_paused_ = true;
    support_PauseMusic(theme_);
    SaveData();
  }
  
  void Game::resume() {
    utils_LogInfo("Petancurling", "Game::resume()");
    is_paused_ = false;
    support_PlayMusic(theme_);
    ReadData();
  }
  
  
  void Game::SaveData(){
    utils_LogDebug("Petancurling", "---------------------IM SAVING-------------");
    s.oldstate_ = s.gs.state;
    s.gs.state = kState_pause;
    ofstream file;
    file.open(support_PathToSaveGameData("fileSaveData.txt"), ios::out | ios::trunc);
    file.write((char*)&s,sizeof(Scene));
    file.close();
  }
  
  void Game::ReadData() {
    utils_LogDebug("Petancurling", "---------------------IM LOADING-------------");
    ifstream file;
    file.open (support_PathToSaveGameData("fileSaveData.txt"), ios::in);
    file.read((char*)&s,sizeof(Scene));
    file.close();
  }
  
  float Game::DistToTarget(float x1,float y1,float x2, float y2) {
    return sqrt((x1 - x2) * (x1- x2) + (y1 - y2) * (y1 - y2));
    
  }
  
  void Game::update() {
    if (is_paused_ || !is_opengl_init_) return;
    // Calculate time since last update
    if (secs_last_update_ == 0.0) {
      secs_last_update_ = utils_GetTimeSeconds();
    }
    double secs_current_time = utils_GetTimeSeconds();
    secs_last_update_ = secs_current_time;
    
    float sx, sy;
    switch(s.gs.state){
      case kState_menu:
        sx = s.play.pos.x + (s.play.size.x/2);
        sy = s.play.pos.y - (s.play.size.y/2);
        if(DistToTarget(touch.x, touch.y, sx , sy) < s.play.size.x/2){
          s.gs.state = kState_reset_position;
          touch.active=false;
        }
        if(touched){
          s.gs.state = kState_reset_position;
          touched=false;
          
        }
        break;
        
      case kState_reset_position:
        for(int i=0; i<3; i++) {
          s.ball[i].img.pos.x = rand() % (width_ - 70) + 10;
          s.ball[i].img.pos.y = 105.0f;
        }
        
        s.arrow.rotation = 0.0f;
        s.intensity.pos.x = width_ -45.0f;
        s.intensity.pos.y = 150.0f;
        s.gs.score=0;
        s.gs.attempts=0;
        for(int i=0; i<3;i++) {
          s.smallgoal[i].pos.x = 50;
          s.smallgoal[i].pos.y = (height_ - 20) - 50 * i;
        }
        s.gs.state++;
        break;
        
      case kState_select_rotation:
        s.arrow.pos.x = s.ball[s.gs.attempts].img.pos.x+40;
        s.arrow.pos.y = s.ball[s.gs.attempts].img.pos.y;
        //Arrow update
        if(s.arrow.rotation < 0.4f)add = true;
        if(s.arrow.rotation > 2.64159f)add = false;
        if(add==true)s.arrow.rotation += 0.1f;
        else s.arrow.rotation -= 0.1f;
        if(paused){
          s.oldstate_ = s.gs.state;
          s.gs.state = kState_pause;
        }
        if(touched){
          s.gs.state++;
          touched=false;
        }
        if(touch.active){
          sx = s.pause.pos.x + (s.pause.size.x/2.0f);
          sy = s.pause.pos.y - (s.pause.size.y/2.0f);
          if(DistToTarget(touch.x, touch.y, sx , sy) < s.pause.size.x/2.0f){
            s.oldstate_ = s.gs.state;
            s.gs.state = kState_pause;
            touch.active = false;
          }
          else {
            s.gs.state++;
            touch.active = false;
          }
        }
        break;
        
      case kState_set_rotation:
        s.ball[s.gs.attempts].direction.x = cos(s.arrow.rotation);
        s.ball[s.gs.attempts].direction.y = sin(s.arrow.rotation);
        s.gs.state++;
        break;
        
      case kState_select_force:
        //Intensity update
        if(s.intensity.pos.y >(height_ - 15.0f))add = false;
        if(s.intensity.pos.y < 151.0f)add = true;
        if(add)s.intensity.pos.y+=20;
        else s.intensity.pos.y-=20;
        if(touched){
          s.gs.state++;
          touched=false;
        }
        if(paused){
          s.oldstate_ = s.gs.state;
          s.gs.state = kState_pause;
        }
        if(touch.active){
          sx = s.pause.pos.x + (s.pause.size.x/2);
          sy = s.pause.pos.y - (s.pause.size.y/2);
          if(DistToTarget(touch.x, touch.y, sx , sy) < s.pause.size.x/2){
            s.oldstate_ = s.gs.state;
            s.gs.state = kState_pause;
            touch.active = false;
          }
          else {
            s.gs.state++;
            touch.active = false;
          }
        }
        break;
        
      case kState_set_force:
        support_PlaySound(launch_);
        s.ball[s.gs.attempts].speed = (s.intensity.pos.y*0.02f);
        s.gs.state++;
        break;
        
      case kState_pause:
        sx = s.play.pos.x + (s.play.size.x/2);
        sy = s.play.pos.y - (s.play.size.y/2);
        if(DistToTarget(touch.x, touch.y, sx , sy) < s.play.size.x/2){
          s.gs.state = s.oldstate_;
          touch.active=false;
        }
        if(touched){
          s.gs.state = s.oldstate_;
          paused=false;
        }
        
        break;
        
      case kState_game:
        if(touch.active){
          sx = s.pause.pos.x + (s.pause.size.x/2);
          sy = s.pause.pos.y - (s.pause.size.y/2);
          if(DistToTarget(touch.x, touch.y, sx , sy) < s.pause.size.x/2){
            s.oldstate_ = s.gs.state;
            s.gs.state = kState_pause;
            touch.active = false;
          }
        }
        
        if(paused){
          s.oldstate_ = s.gs.state;
          s.gs.state = kState_pause;
        }
        
        
        //Ball update
        if(s.ball[s.gs.attempts].img.pos.x < 0){
          s.ball[s.gs.attempts].direction.x *= -1.0f;
          support_PlaySound(bounce_);
        }
        if(s.ball[s.gs.attempts].img.pos.y < s.ball[0].img.size.y){
          s.ball[s.gs.attempts].direction.y *= -1.0f;
          support_PlaySound(bounce_);
        }
        if(s.ball[s.gs.attempts].img.pos.x>width_ - s.ball[0].img.size.x){
          s.ball[s.gs.attempts].direction.x *= -1.0f;
          support_PlaySound(bounce_);
        }
        if(s.ball[s.gs.attempts].img.pos.y>height_){
          s.ball[s.gs.attempts].direction.y *= -1.0f;
          support_PlaySound(bounce_);
        }
        s.ball[s.gs.attempts].img.pos.x += (s.ball[s.gs.attempts].direction.x*s.ball[s.gs.attempts].speed);
        s.ball[s.gs.attempts].img.pos.y += (s.ball[s.gs.attempts].direction.y*s.ball[s.gs.attempts].speed);
        s.ball[s.gs.attempts].speed -= s.ball[0].friction;
        if (s.ball[s.gs.attempts].speed < 0){
          s.ball[s.gs.attempts].speed = 0;
          if(DistToTarget(s.ball[s.gs.attempts].img.pos.x+(s.ball[0].img.size.x/2),
                          s.ball[s.gs.attempts].img.pos.y-(s.ball[0].img.size.y/2),
                          s.goal.pos.x + (s.goal.size.x/2),
                          s.goal.pos.y - (s.goal.size.y/2))
             < (s.goal.size.x/2)){
            support_PlaySound(success_);
            s.gs.score++;
          }
          else{
            support_PlaySound(fail_);
          }
          s.gs.attempts++;
          if(s.gs.attempts<3)s.gs.state = kState_select_rotation;
          else s.gs.state = kState_score;
        }
        break;
      case kState_score:
        for(int i=0; i<3;i++) {
          s.smallgoal[i].pos.x = ((width_ / 2) - 70) + (50 * i);
          s.smallgoal[i].pos.y = (height_ / 2) - 180;
        }
        if(touched){
          s.gs.state = kState_reset_position;
          touched=false;
        }
        if(touch.active){
          sx = s.replay.pos.x + (s.replay.size.x/2);
          sy = s.replay.pos.y - (s.replay.size.y/2);
          if(DistToTarget(touch.x, touch.y, sx , sy) < s.replay.size.x/2){
            s.gs.state = kState_reset_position;
            touch.active=false;
          }
        }
        break;
    }
  }
  
  void Game::render() {
    if (!is_opengl_init_) return;
    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(program_handle_);
    
    switch(s.gs.state){
        
        
      case kState_menu:
        drawTexture(s.goal.pos.x, s.goal.pos.y, s.goal.size.x, s.goal.size.y, s.goal.rotation, goal_id);
        drawTexture(s.logo.pos.x, s.logo.pos.y, s.logo.size.x, s.logo.size.y, s.logo.rotation, logo_id);
        drawTexture(s.play.pos.x, s.play.pos.y, s.play.size.x, s.play.size.y, s.play.rotation, play_id);
        break;
      case kState_reset_position:
        drawTexture(s.gradiant.pos.x, s.gradiant.pos.y, s.gradiant.size.x, s.gradiant.size.y, s.gradiant.rotation, gradiant_id);
        drawTexture(s.intensity.pos.x, s.intensity.pos.y, s.intensity.size.x, s.intensity.size.y, s.intensity.rotation, intensity_id);
        drawTexture(s.grey.pos.x, s.grey.pos.y, s.grey.size.x, s.grey.size.y, s.grey.rotation, grey_id);
        drawTexture(s.goal.pos.x, s.goal.pos.y, s.goal.size.x, s.goal.size.y, s.goal.rotation, goal_id);
        break;
        
        
      case kState_select_rotation:
        for(int i=s.gs.attempts; i<3; i++){
        drawTexture(s.attempt[i].pos.x, s.attempt[i].pos.y, s.attempt[i].size.x, s.attempt[i].size.y, s.attempt[i].rotation, attempt_id[i]);
      }
        drawTexture(s.gradiant.pos.x, s.gradiant.pos.y, s.gradiant.size.x, s.gradiant.size.y, s.gradiant.rotation, gradiant_id);
        if(s.gs.score>0) drawTexture(s.smallgoal[0].pos.x, s.smallgoal[0].pos.y, s.smallgoal[0].size.x, s.smallgoal[0].size.y, s.smallgoal[0].rotation, smallGoal_id[0]);
        if(s.gs.score>1) drawTexture(s.smallgoal[1].pos.x, s.smallgoal[1].pos.y, s.smallgoal[1].size.x, s.smallgoal[1].size.y, s.smallgoal[1].rotation, smallGoal_id[1]);
        if(s.gs.score>2) drawTexture(s.smallgoal[2].pos.x, s.smallgoal[2].pos.y, s.smallgoal[2].size.x, s.smallgoal[2].size.y, s.smallgoal[2].rotation, smallGoal_id[2]);
          drawTexture(s.intensity.pos.x, s.intensity.pos.y, s.intensity.size.x, s.intensity.size.y, s.intensity.rotation, intensity_id);
          drawTexture(s.grey.pos.x, s.grey.pos.y, s.grey.size.x, s.grey.size.y, s.grey.rotation, grey_id);
          drawTexture(s.goal.pos.x, s.goal.pos.y, s.goal.size.x, s.goal.size.y, s.goal.rotation, goal_id);
          drawArrow(s.arrow.pos.x, s.arrow.pos.y, s.arrow.size.x, s.arrow.size.y, s.arrow.rotation, arrow_id);
          for(int i=0;i<=s.gs.attempts;i++)
            drawTexture(s.ball[i].img.pos.x, s.ball[i].img.pos.y, s.ball[i].img.size.x, s.ball[i].img.size.y, s.ball[i].img.rotation, Rock_id[i]);
          drawTexture(s.pause.pos.x, s.pause.pos.y, s.pause.size.x, s.pause.size.y, s.pause.rotation, pause_id);
        break;
        
        
      case kState_set_rotation:
        for(int i=s.gs.attempts; i<3; i++){
          drawTexture(s.attempt[i].pos.x, s.attempt[i].pos.y, s.attempt[i].size.x, s.attempt[i].size.y, s.attempt[i].rotation, attempt_id[i]);
        }
        drawTexture(s.gradiant.pos.x, s.gradiant.pos.y, s.gradiant.size.x, s.gradiant.size.y, s.gradiant.rotation, gradiant_id);
        if(s.gs.score>0) drawTexture(s.smallgoal[0].pos.x, s.smallgoal[0].pos.y, s.smallgoal[0].size.x, s.smallgoal[0].size.y, s.smallgoal[0].rotation, smallGoal_id[0]);
        if(s.gs.score>1) drawTexture(s.smallgoal[1].pos.x, s.smallgoal[1].pos.y, s.smallgoal[1].size.x, s.smallgoal[1].size.y, s.smallgoal[1].rotation, smallGoal_id[1]);
        if(s.gs.score>2) drawTexture(s.smallgoal[2].pos.x, s.smallgoal[2].pos.y, s.smallgoal[2].size.x, s.smallgoal[2].size.y, s.smallgoal[2].rotation, smallGoal_id[2]);
        drawTexture(s.intensity.pos.x, s.intensity.pos.y, s.intensity.size.x, s.intensity.size.y, s.intensity.rotation, intensity_id);
        drawTexture(s.grey.pos.x, s.grey.pos.y, s.grey.size.x, s.grey.size.y, s.grey.rotation, grey_id);
        drawTexture(s.goal.pos.x, s.goal.pos.y, s.goal.size.x, s.goal.size.y, s.goal.rotation, goal_id);
        drawArrow(s.arrow.pos.x, s.arrow.pos.y, s.arrow.size.x, s.arrow.size.y, s.arrow.rotation, arrow_id);
        for(int i=0;i<=s.gs.attempts;i++)
          drawTexture(s.ball[i].img.pos.x, s.ball[i].img.pos.y, s.ball[i].img.size.x, s.ball[i].img.size.y, s.ball[i].img.rotation, Rock_id[i]);
        drawTexture(s.pause.pos.x, s.pause.pos.y, s.pause.size.x, s.pause.size.y, s.pause.rotation, pause_id);
        break;
        
      case kState_select_force:
        for(int i=s.gs.attempts; i<3; i++){
          drawTexture(s.attempt[i].pos.x, s.attempt[i].pos.y, s.attempt[i].size.x, s.attempt[i].size.y, s.attempt[i].rotation, attempt_id[i]);
        }
        drawTexture(s.gradiant.pos.x, s.gradiant.pos.y, s.gradiant.size.x, s.gradiant.size.y, s.gradiant.rotation, gradiant_id);
        if(s.gs.score>0) drawTexture(s.smallgoal[0].pos.x, s.smallgoal[0].pos.y, s.smallgoal[0].size.x, s.smallgoal[0].size.y, s.smallgoal[0].rotation, smallGoal_id[0]);
        if(s.gs.score>1) drawTexture(s.smallgoal[1].pos.x, s.smallgoal[1].pos.y, s.smallgoal[1].size.x, s.smallgoal[1].size.y, s.smallgoal[1].rotation, smallGoal_id[1]);
        if(s.gs.score>2) drawTexture(s.smallgoal[2].pos.x, s.smallgoal[2].pos.y, s.smallgoal[2].size.x, s.smallgoal[2].size.y, s.smallgoal[2].rotation, smallGoal_id[2]);
        drawTexture(s.intensity.pos.x, s.intensity.pos.y, s.intensity.size.x, s.intensity.size.y, s.intensity.rotation, intensity_id);
        drawTexture(s.grey.pos.x, s.grey.pos.y, s.grey.size.x, s.grey.size.y, s.grey.rotation, grey_id);
        drawTexture(s.goal.pos.x, s.goal.pos.y, s.goal.size.x, s.goal.size.y, s.goal.rotation, goal_id);
        drawArrow(s.arrow.pos.x, s.arrow.pos.y, s.arrow.size.x, s.arrow.size.y, s.arrow.rotation, arrow_id);
        for(int i=0;i<=s.gs.attempts;i++)
          drawTexture(s.ball[i].img.pos.x, s.ball[i].img.pos.y, s.ball[i].img.size.x, s.ball[i].img.size.y, s.ball[i].img.rotation, Rock_id[i]);
        drawTexture(s.pause.pos.x, s.pause.pos.y, s.pause.size.x, s.pause.size.y, s.pause.rotation, pause_id);
        break;
        
        
      case kState_set_force:
        for(int i=s.gs.attempts; i<3; i++){
          drawTexture(s.attempt[i].pos.x, s.attempt[i].pos.y, s.attempt[i].size.x, s.attempt[i].size.y, s.attempt[i].rotation, attempt_id[i]);
        }
        drawTexture(s.gradiant.pos.x, s.gradiant.pos.y, s.gradiant.size.x, s.gradiant.size.y, s.gradiant.rotation, gradiant_id);
        if(s.gs.score>0) drawTexture(s.smallgoal[0].pos.x, s.smallgoal[0].pos.y, s.smallgoal[0].size.x, s.smallgoal[0].size.y, s.smallgoal[0].rotation, smallGoal_id[0]);
        if(s.gs.score>1) drawTexture(s.smallgoal[1].pos.x, s.smallgoal[1].pos.y, s.smallgoal[1].size.x, s.smallgoal[1].size.y, s.smallgoal[1].rotation, smallGoal_id[1]);
        if(s.gs.score>2) drawTexture(s.smallgoal[2].pos.x, s.smallgoal[2].pos.y, s.smallgoal[2].size.x, s.smallgoal[2].size.y, s.smallgoal[2].rotation, smallGoal_id[2]);
        drawTexture(s.intensity.pos.x, s.intensity.pos.y, s.intensity.size.x, s.intensity.size.y, s.intensity.rotation, intensity_id);
        drawTexture(s.grey.pos.x, s.grey.pos.y, s.grey.size.x, s.grey.size.y, s.grey.rotation, grey_id);
        drawTexture(s.goal.pos.x, s.goal.pos.y, s.goal.size.x, s.goal.size.y, s.goal.rotation, goal_id);
        drawArrow(s.arrow.pos.x, s.arrow.pos.y, s.arrow.size.x, s.arrow.size.y, s.arrow.rotation, arrow_id);
        for(int i=0;i<=s.gs.attempts;i++)
          drawTexture(s.ball[i].img.pos.x, s.ball[i].img.pos.y, s.ball[i].img.size.x, s.ball[i].img.size.y, s.ball[i].img.rotation, Rock_id[i]);
        drawTexture(s.pause.pos.x, s.pause.pos.y, s.pause.size.x, s.pause.size.y, s.pause.rotation, pause_id);
        break;
        
      case kState_game:
        
        for(int i=s.gs.attempts; i<3; i++){
          drawTexture(s.attempt[i].pos.x, s.attempt[i].pos.y, s.attempt[i].size.x, s.attempt[i].size.y, s.attempt[i].rotation, attempt_id[i]);
        }
        drawTexture(s.gradiant.pos.x, s.gradiant.pos.y, s.gradiant.size.x, s.gradiant.size.y, s.gradiant.rotation, gradiant_id);
        if(s.gs.score>0) drawTexture(s.smallgoal[0].pos.x, s.smallgoal[0].pos.y, s.smallgoal[0].size.x, s.smallgoal[0].size.y, s.smallgoal[0].rotation, smallGoal_id[0]);
        if(s.gs.score>1) drawTexture(s.smallgoal[1].pos.x, s.smallgoal[1].pos.y, s.smallgoal[1].size.x, s.smallgoal[1].size.y, s.smallgoal[1].rotation, smallGoal_id[1]);
        if(s.gs.score>2) drawTexture(s.smallgoal[2].pos.x, s.smallgoal[2].pos.y, s.smallgoal[2].size.x, s.smallgoal[2].size.y, s.smallgoal[2].rotation, smallGoal_id[2]);
        drawTexture(s.intensity.pos.x, s.intensity.pos.y, s.intensity.size.x, s.intensity.size.y, s.intensity.rotation, intensity_id);
        drawTexture(s.grey.pos.x, s.grey.pos.y, s.grey.size.x, s.grey.size.y, s.grey.rotation, grey_id);
        drawTexture(s.goal.pos.x, s.goal.pos.y, s.goal.size.x, s.goal.size.y, s.goal.rotation, goal_id);
        for(int i=0;i<=s.gs.attempts;i++)
          drawTexture(s.ball[i].img.pos.x, s.ball[i].img.pos.y, s.ball[i].img.size.x, s.ball[i].img.size.y, s.ball[i].img.rotation, Rock_id[i]);
        drawTexture(s.pause.pos.x, s.pause.pos.y, s.pause.size.x, s.pause.size.y, s.pause.rotation, pause_id);
        break;
        
        
      case kState_pause:
        for(int i=s.gs.attempts; i<3; i++){
          drawTexture(s.attempt[i].pos.x, s.attempt[i].pos.y, s.attempt[i].size.x, s.attempt[i].size.y, s.attempt[i].rotation, attempt_id[i]);
        }
        drawTexture(s.gradiant.pos.x, s.gradiant.pos.y, s.gradiant.size.x, s.gradiant.size.y, s.gradiant.rotation, gradiant_id);
        if(s.gs.score>0) drawTexture(s.smallgoal[0].pos.x, s.smallgoal[0].pos.y, s.smallgoal[0].size.x, s.smallgoal[0].size.y, s.smallgoal[0].rotation, smallGoal_id[0]);
        if(s.gs.score>1) drawTexture(s.smallgoal[1].pos.x, s.smallgoal[1].pos.y, s.smallgoal[1].size.x, s.smallgoal[1].size.y, s.smallgoal[1].rotation, smallGoal_id[1]);
        if(s.gs.score>2) drawTexture(s.smallgoal[2].pos.x, s.smallgoal[2].pos.y, s.smallgoal[2].size.x, s.smallgoal[2].size.y, s.smallgoal[2].rotation, smallGoal_id[2]);
        drawTexture(s.intensity.pos.x, s.intensity.pos.y, s.intensity.size.x, s.intensity.size.y, s.intensity.rotation, intensity_id);
        drawTexture(s.grey.pos.x, s.grey.pos.y, s.grey.size.x, s.grey.size.y, s.grey.rotation, grey_id);
        drawTexture(s.goal.pos.x, s.goal.pos.y, s.goal.size.x, s.goal.size.y, s.goal.rotation, goal_id);
        for(int i=0;i<=s.gs.attempts;i++)
          drawTexture(s.ball[i].img.pos.x, s.ball[i].img.pos.y, s.ball[i].img.size.x, s.ball[i].img.size.y, s.ball[i].img.rotation, Rock_id[i]);
        drawTexture(s.pause.pos.x, s.pause.pos.y, s.pause.size.x, s.pause.size.y, s.pause.rotation, pause_id);
        drawTexture(s.play.pos.x, s.play.pos.y, s.play.size.x, s.play.size.y, s.play.rotation, play_id);
        break;
        
        
      case kState_score:
        drawTexture(s.goal.pos.x, s.goal.pos.y, s.goal.size.x, s.goal.size.y, s.goal.rotation, goal_id);
        drawTexture(s.grey.pos.x, s.grey.pos.y, s.grey.size.x, s.grey.size.y, s.grey.rotation, grey_id);
        if(s.gs.score>0) drawTexture(s.smallgoal[0].pos.x, s.smallgoal[0].pos.y, s.smallgoal[0].size.x, s.smallgoal[0].size.y, s.smallgoal[0].rotation, smallGoal_id[0]);
        if(s.gs.score>1) drawTexture(s.smallgoal[1].pos.x, s.smallgoal[1].pos.y, s.smallgoal[1].size.x, s.smallgoal[1].size.y, s.smallgoal[1].rotation, smallGoal_id[1]);
        if(s.gs.score>2) drawTexture(s.smallgoal[2].pos.x, s.smallgoal[2].pos.y, s.smallgoal[2].size.x, s.smallgoal[2].size.y, s.smallgoal[2].rotation, smallGoal_id[2]);
        drawTexture(s.score.pos.x, s.score.pos.y, s.score.size.x, s.score.size.y, s.score.rotation, score_id);
        drawTexture(s.replay.pos.x, s.replay.pos.y, s.replay.size.x, s.replay.size.y, s.replay.rotation, replay_id);
        break;
      default:
        break;
    }
  }
  
  
  void Game::loadTexture(unsigned int* texID, const char* path){
    int width = 0;
    int height = 0;
    bool alpha = true;
    unsigned char* file = nullptr;
    
    file = support_LoadImageFile(path, &width, &height, &alpha);
    
    glGenTextures(1, texID);
    glBindTexture(GL_TEXTURE_2D, *texID);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, file);
    glBindTexture(GL_TEXTURE_2D, 0);
  }
  
  void Game::compileTexture(TextureBuffer* buffer) {
    glGenBuffers(1,&buffer->vbo);
    glGenBuffers(1,&buffer->ebo);
    
    //Vertex
    glBindBuffer(GL_ARRAY_BUFFER, buffer->vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(v_), v_, GL_STATIC_DRAW);
    //Index
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(index_), index_, GL_STATIC_DRAW);
    //Position
    glVertexAttribPointer(attribute_position_, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), 0);
    glEnableVertexAttribArray(attribute_position_);
    //UV
    glVertexAttribPointer(uv_position_, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (void*)(sizeof(float) * 3));
    glEnableVertexAttribArray(uv_position_);
  }
  
  void Game::drawTexture(float x, float y, float w, float h, float r, unsigned int texID) const{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texID);
    glUniform1i(glGetUniformLocation(program_handle_, "u_texture"), 0);
    
    glm::mat4 mvp(1.0f);
    glm::mat4 translation = glm::translate(glm::mat4(1.0f), glm::vec3( x , y-h , 0.0f));
    glm::mat4 scale = glm::scale(glm::mat4(1.0f), glm::vec3(w, h, 1.0f ));
    glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), r, glm::vec3(0.0f, 0.0f, 1.0f));
    
    mvp =  translation * rotation * scale;
    glm::mat4 matrix_ortho = {
      2.0f / width_, 0.0f, 0.0f, 0.0f,
      0.0f, 2.0f / height_, 0.0f, 0.0f,
      0.0f, 0.0f, -1.f, 0.0f,
      -1.0f, -1.0f, -0.0f, 1.0f
    };
    mvp = matrix_ortho * mvp;
    glUniformMatrix4fv(uniform_mvp_matrix_, 1, GL_FALSE, glm::value_ptr(mvp));
    
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
  }
  
  void Game::drawArrow(float x, float y, float w, float h, float r, unsigned int texID) const{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texID);
    glUniform1i(glGetUniformLocation(program_handle_, "u_texture"), 0);
    
    glm::mat4 mvp(1.0f);
    glm::mat4 translation = glm::translate(glm::mat4(1.0f), glm::vec3( x , y-h , 0.0f));
    glm::mat4 translation2 = glm::translate(glm::mat4(1.0f), glm::vec3( s.arrow.size.x , 0.0f , 0.0f));
    glm::mat4 scale = glm::scale(glm::mat4(1.0f), glm::vec3(w, h, 1.0f ));
    glm::mat4 rotation = glm::rotate(glm::mat4(1.0f), r, glm::vec3(0.0f, 0.0f, 1.0f));
    
    mvp =  translation * rotation * translation2 * scale;
    glm::mat4 matrix_ortho = {
      2.0f / width_, 0.0f, 0.0f, 0.0f,
      0.0f, 2.0f / height_, 0.0f, 0.0f,
      0.0f, 0.0f, -1.f, 0.0f,
      -1.0f, -1.0f, -0.0f, 1.0f
    };
    mvp = matrix_ortho * mvp;
    glUniformMatrix4fv(uniform_mvp_matrix_, 1, GL_FALSE, glm::value_ptr(mvp));
    
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
  }
  
  bool Game::loadShaders() {
    // shaders
    const char *vert_shader_str = support_LoadVertexShader("vertex_shader.vsh");
    const char *frag_shader_str = support_LoadFragmentShader("fragment_shader.fsh");
    
    program_handle_ = loadProgram(vert_shader_str, frag_shader_str);
    if (program_handle_ == 0)
      return false;
    
    // Get the attribute locations
    attribute_position_ = glGetAttribLocation(program_handle_, "a_position");
    // get the texture location
    uv_position_ = glGetAttribLocation(program_handle_, "a_texture");
    // Get uniform locations.
    uniform_mvp_matrix_ = glGetUniformLocation(program_handle_, "u_mvp");
    
    delete vert_shader_str;
    delete frag_shader_str;
    
    return true;
  }
  
  int Game::loadProgram(const char* str_vert_source,
                        const char* str_frag_source) {
    int id_vert_shader;
    int id_frag_shader;
    int id_program;
    int link[1];
    
    id_vert_shader = loadShader(str_vert_source, GL_VERTEX_SHADER);
    if (id_vert_shader == 0) {
      utils_LogDebug("Petancurling", "Load vertex shader failed.");
      return 0;
    }
    
    id_frag_shader = loadShader(str_frag_source, GL_FRAGMENT_SHADER);
    if (id_frag_shader == 0) {
      utils_LogDebug("Petancurling", "Load fragment shader failed.");
      return 0;
    }
    
    id_program = glCreateProgram();
    
    // attach shaders to program
    glAttachShader(id_program, id_vert_shader);
    glAttachShader(id_program, id_frag_shader);
    
    // link program
    glLinkProgram(id_program);
    
    // get the link status
    glGetProgramiv(id_program, GL_LINK_STATUS, link);
    if (link[0] <= 0) {
      GLint log_length;
      glGetProgramiv(id_program, GL_INFO_LOG_LENGTH, &log_length);
      if (log_length > 0) {
        GLchar *log = new GLchar[log_length];
        glGetProgramInfoLog(id_program, log_length, &log_length, log);
        utils_LogDebug("Petancurling", "Failed load program: %s", log);
        delete[] log;
      }
      utils_LogDebug("Petancurling", "Load program failed.");
      return 0;
    }
    // delete the shaders, since we have already loaded
    glDeleteShader(id_vert_shader);
    glDeleteShader(id_frag_shader);
    return id_program;
  }
  
  int Game::loadShader(const char* str_source, int id_type) {
    int compiled[1];
    int id_shader = glCreateShader(id_type);
    glShaderSource(id_shader, 1, &str_source, 0);
    glCompileShader(id_shader);
    glGetShaderiv(id_shader, GL_COMPILE_STATUS, compiled);
    if (compiled[0] == 0) {
      GLint log_length;
      glGetShaderiv(id_shader, GL_INFO_LOG_LENGTH, &log_length);
      if (log_length > 0) {
        GLchar *log = new GLchar[log_length];
        glGetShaderInfoLog(id_shader, log_length, &log_length, log);
        utils_LogDebug("Petancurling", "Failed loadShader: %s", log);
        delete[] log;
      }
      return 0;
    }
    return id_shader;
  }
}  // namespace petancurling
