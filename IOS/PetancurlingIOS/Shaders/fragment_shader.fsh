varying lowp vec2 text_coords;
uniform sampler2D u_texture;

void main()
{
  if(texture2D(u_texture, text_coords).a < 0.1) discard;
  else gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0) * texture2D(u_texture, text_coords);
}
