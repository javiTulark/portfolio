//
//  vertext_shader.vsh
//  PetancurlingIOS
//
//  Created by Marcos Martí Nacher on 22/10/16.
//  Copyright © 2016 ESAT. All rights reserved.
//

layout (location = 0) in vec3 position;
layout (location = 2) in vec2 texCoord;

varying TexCoord;

void main()
{
    gl_Position = vec4(position, 1.0f);
    TexCoord = texCoord;
}
