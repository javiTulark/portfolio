//
//  support.h
//  PetancurlingIOS
//
//  Created by Marcos Martí Nacher on 22/10/16.
//  Copyright © 2016 ESAT. All rights reserved.
//

#ifndef __SUPPORT_H__
#define __SUPPORT_H__ 1

#include <stdbool.h>

#ifndef __ANDROID__
#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#endif


#ifdef __cplusplus
extern "C" {
#endif
	
	
// Functions load shaders
const char* support_LoadVertexShader(const char* shader_file);
const char* support_LoadFragmentShader(const char* shader_file);

// Function for load images
unsigned char* support_LoadImageFile(const char* file, int* image_width,
									 int* image_height, bool* has_alpha);

// Function for load audio
void* support_LoadOpenALAudioData(const char* file, ALsizei *out_data_size,
								  ALenum *out_data_format, ALsizei* out_sample_rate);

// Function help save game data
const char* support_PathToSaveGameData(const char* file);

// Functions manage music
int support_LoadMusic(const char* file);
bool support_PlayMusic(unsigned int index);
bool support_PauseMusic(unsigned int index);
bool support_IsPlayingMusic(unsigned int index);
void support_ReleaseMusic(unsigned int index);
  
// Functions manage sounds
bool support_InitSounds(unsigned int max_num_sounds);
int support_LoadSound(const char* file);
bool support_PlaySound(unsigned int index);
void support_ReleaseSounds();
  
#ifdef __cplusplus
}
#endif


#endif  // __SUPPORT_H__
