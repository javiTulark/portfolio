//
//  support.swift
//  PetancurlingIOS
//
//  Created by Marcos Martí Nacher on 22/10/16.
//  Copyright © 2016 ESAT. All rights reserved.
//

import AVFoundation

class Support: NSObject {

  static var audioPlayer = [AVAudioPlayer]()
    static var soundPlayer = [AVAudioPlayer]()
    var url: NSURL? = nil, url2: NSURL? = nil, url3: NSURL? = nil, url4: NSURL? = nil
    static var maxSoundsNum: Int32 = 10
    static var Sound: Int32 = 0
    static var Sound_control: Int32 = 0
    static var i: Int32 = 0
    static var control = false;
	static func PathForResource(resource: String) -> String? {
		let lastComponent = (resource as NSString).lastPathComponent as NSString
		let fileName = lastComponent.stringByDeletingPathExtension
		let fileType = lastComponent.pathExtension
		
		let path = NSBundle.mainBundle().pathForResource(fileName, ofType: fileType)
		if path == nil {
			return nil
		}
		else {
			return path!
		}
	}
	
	// Function help save game data
	static func PathToSaveGameData(file: String) -> String? {
        let documents = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        let savePath = (documents as NSString).stringByAppendingPathComponent(file)
        
        return savePath
        
    }
	
	// Functions manage music
	static func LoadMusic(file: String) -> Int32 {
   
    do {
    let bundle = NSBundle.mainBundle()
        
        if (PathForResource(file + ".mp3") == nil) {
            NSLog("WARNING - The following music file doens't exist: \(String(file))")
            return -1
        }
        if (PathForResource(file) == "") {
            NSLog("WARNING - The following music file is emply: \(String(file))")
            return -1
        }

    let path = bundle.URLForResource(file, withExtension: "mp3")! as NSURL
    
    audioPlayer.append(try AVAudioPlayer.init(contentsOfURL:path))
    audioPlayer.last?.prepareToPlay()
    audioPlayer.last?.numberOfLoops = -1
  }
  catch {
  NSLog("ERROR - Can't init AVAuidoPlayer music file: \(error)")
  return -1
    }
    return 1
	}

	static func PlayMusic(index: UInt) -> Bool {
    if((audioPlayer.last?.play()) != nil){ return true}
    else {return false}
	}

	static func PauseMusic(index: UInt) -> Bool {
    if((audioPlayer.last?.pause()) != nil){ return true}
    else {return false}
	}

	static func IsPlayingMusic(index: UInt) -> Bool {
    if((audioPlayer.last?.playing) != nil){ return true}
    else {return false}
	}
	
	static func ReleaseMusic(index: UInt) {
        audioPlayer.last?.stop();
	}
	
	// Functions manage sounds
	static func InitSounds(max_num_sounds: Int32) -> Bool {
       
        if (control == false){
        Sound = max_num_sounds;
        if max_num_sounds > maxSoundsNum {
            return false
        }else{
            return true}
            
            
        }
        NSLog(" InitSound() ya inicializada")
        return false;
    }
	
	static func LoadSound(file: String) -> Int32 {
        do {
            Sound_control = Sound_control + 1
            
            if (Sound == 0){
                NSLog("InitSound() not called")
                return -1};
            
            if (Sound < Sound_control){
                 NSLog("ERROR MAX SOUNDS")
                return -1};
            let bundle = NSBundle.mainBundle()
            
            if (PathForResource(file + ".mp3") == nil) {
                NSLog("WARNING - The following music file doens't exist: \(String(file))")
                return -1
            }
            if (PathForResource(String(file)) == "") {
                NSLog("WARNING - The following music file doens't exist: \(String(file))")
                return -1
            }

            
            let path = bundle.URLForResource(file, withExtension: "mp3")! as NSURL
            
            soundPlayer.append(try AVAudioPlayer.init(contentsOfURL:path))
            soundPlayer.last?.prepareToPlay()
            soundPlayer.last?.numberOfLoops = 0
        }
        catch {
            NSLog("ERROR - Can't init AVAuidoPlayer music file: \(error)")
            return -1
        }
       
        return (Int32(soundPlayer.count)-1);
        
        
		
	}
	
	static func PlaySound(index: UInt) -> Bool {
        if (Int(index) > ((soundPlayer.count)-1)){return false}
        
        soundPlayer[Int(index)].play()
        return true
        
		
	}
	
	static func ReleaseSounds() {
        
        
        var i = 1
        while i <= Int(Sound_control) {
           soundPlayer[Int(i)].stop();
            i = i + 1
        }
        
        
	}
	
	
		
}

