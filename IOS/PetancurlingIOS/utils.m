//
//  utils.m
//  PetancurlingIOS
//
//  Created by Marcos Martí Nacher on 22/10/16.
//  Copyright © 2016 ESAT. All rights reserved.
//

#include "utils.h"

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>


double utils_GetTimeSeconds() {
	double current_time = CACurrentMediaTime();
	return current_time;
}


void utils_LogInfo(const char *tag, const char* format, ...) {
	va_list ap;
	
	va_start(ap, format);
	NSString *form =[NSString stringWithUTF8String:format];
	NSLogv(form, ap);
	va_end (ap);
}

void utils_LogDebug(const char *tag, const char* format, ...) {
	va_list ap;
	
	va_start(ap, format);
	NSString *form =[NSString stringWithUTF8String:format];
	NSLogv(form, ap);
	va_end (ap);
}

void utils_LogWarn(const char *tag, const char* format, ...) {
	va_list ap;
	
	va_start(ap, format);
	NSString *form =[NSString stringWithUTF8String:format];
	NSLogv(form, ap);
	va_end (ap);
}

void utils_LogError(const char *tag, const char* format, ...) {
	va_list ap;
	
	va_start(ap, format);
	NSString *form =[NSString stringWithUTF8String:format];
	NSLogv(form, ap);
	va_end (ap);
}


