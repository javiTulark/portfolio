//
//  GameViewController.swift
//  PetancurlingIOS
//
//  Created by Marcos Martí Nacher on 22/10/16.
//  Copyright © 2016 ESAT. All rights reserved.
//

import AVFoundation
import GLKit
import OpenGLES


class GameViewController: GLKViewController {
	
	var gameWrapper: GameWrapper? = nil
	
    var context: EAGLContext? = nil
    
    var paused_: Bool = false;
    
    // FROM C++
    var estadoJuego: Int32 = 0;
  
    deinit {
		NSLog("deinit");
        self.tearDownGL()
        
        if EAGLContext.currentContext() === self.context {
            EAGLContext.setCurrentContext(nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.context = EAGLContext(API: .OpenGLES2)
        
        if !(self.context != nil) {
            print("Failed to create ES context")
        }
        
        let view = self.view as! GLKView
        view.context = self.context!
        view.drawableDepthFormat = .Format24
		
		// Initialize Audio Session
		initAudioSession()
		
		// Create GameWrapper
		self.gameWrapper = GameWrapper()
		
		// Setup Open GL
        self.setupGL()
        
        
    }
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		let width = self.view.bounds.size.width
		let height = self.view.bounds.size.height
		let scale = UIScreen.mainScreen().scale
		let realWidth = Float(width*scale)
		let realHeight = Float(height*scale)
		self.gameWrapper!.initializeWithWidth(realWidth, andHeight: realHeight)
	}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        if self.isViewLoaded() && (self.view.window != nil) {
            self.view = nil
            
            self.tearDownGL()
            
            if EAGLContext.currentContext() === self.context {
                EAGLContext.setCurrentContext(nil)
            }
            self.context = nil
        }
		
		// Dispose of any resources that can be recreated.
    }
	
	override func prefersStatusBarHidden() -> Bool {
		return true
	}
	
    func setupGL() {
        EAGLContext.setCurrentContext(self.context)
        
        self.gameWrapper!.setupGL()
    }
    
    func tearDownGL() {
        EAGLContext.setCurrentContext(self.context)
        
        self.gameWrapper!.tearDownGL()
    }
    
    // MARK: - GLKView and GLKViewController delegate methods
    
    func update() {
        self.gameWrapper!.update()
        estadoJuego  = self.gameWrapper!.getGameState();
    }
    
    func touchDownScreen(){
        self.gameWrapper!.touchDownScreen()
    }
  
    func touchPause(){
        self.gameWrapper!.touchPause()
    }
  
    func touchUpScreen(){
        self.gameWrapper!.touchUpScreen()
    }
    
    func pause(){
        self.gameWrapper!.pause()
    }
    
    func resume(){
        self.gameWrapper!.resume()
    }
  
    func loadSounds(){
        
    }
  
    func playSong(numSound: Int){
    }
    
    override func glkView(view: GLKView, drawInRect rect: CGRect) {
		// Render with ES2
		self.gameWrapper!.render()
    }
	
	// MARK: - AVAudioSession and AVAudioSessionDelegate delegate methods
	
	func initAudioSession() {
		do {
			let audioSession = AVAudioSession.sharedInstance()
			try audioSession.setCategory(AVAudioSessionCategoryAmbient)
		}
		catch {
			NSLog("WARNING - AVAudioSession error setting session category: \(error)")
		}
		
		
		do {
			let audioSession = AVAudioSession.sharedInstance()
			try audioSession.setActive(true)
		}
		catch {
			NSLog("WARNING - AVAudioSession error activating: \(error)")
		}
		
		NSNotificationCenter.defaultCenter().addObserver(
			self,
			selector: #selector(interruption),
			name: AVAudioSessionInterruptionNotification,
			object: nil)
	}
	
	func interruption(notification: NSNotification) {
		let interruptionType = notification.userInfo![AVAudioSessionInterruptionTypeKey] as! AVAudioSessionInterruptionType
		
		if interruptionType == AVAudioSessionInterruptionType.Began {
			//session inactive, players have been paused; update any internal state
			beginInterruption()
		}
		else if interruptionType == AVAudioSessionInterruptionType.Ended {
			// activate session, start playing, update internal state
			endInterruption()
		}
	}
	
	func beginInterruption() {
		NSLog("AVAudioSession begin interruption")
	}
	
	func endInterruption() {
		do {
			let audioSession = AVAudioSession.sharedInstance()
			try audioSession.setActive(true)
		}
		catch {
			NSLog("WARNING - AVAudioSession error activating: \(error)")
		}
		
		NSLog("AVAudioSession end interruption")
	}
    
    override func touchesBegan(touches: Set<UITouch>,   withEvent event: UIEvent?) {
        for touch in touches {
            let origTouchPos:CGPoint = touch.locationInView(touch.view)
            NSLog("TouchBegan (%u): %f %f", touch.hash, origTouchPos.x,  origTouchPos.y);
            switch(estadoJuego){
            case 2:
              if(origTouchPos.x>270 && origTouchPos.x<290 && origTouchPos.y>8 && origTouchPos.y<30){
                touchPause();
              }
              else{
                touchDownScreen();
              }
              break;
            case 4:
              if(origTouchPos.x>270 && origTouchPos.x<290 && origTouchPos.y>8 && origTouchPos.y<30){
                touchPause();
              }
              else{
                touchDownScreen();
              }
              break;
            case 6:
              if(origTouchPos.x>270 && origTouchPos.x<290 && origTouchPos.y>8 && origTouchPos.y<30){
                touchPause();
              }
                break;
            default:
              touchDownScreen();
                break;
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>,   withEvent event: UIEvent?) {
        for touch in touches {
            let origTouchPos:CGPoint = touch.locationInView(touch.view)
            NSLog("TouchBegan (%u): %f %f", touch.hash, origTouchPos.x,  origTouchPos.y);
        }
    }
    
    
    override func touchesEnded(touches: Set<UITouch>,   withEvent event: UIEvent?) {
        for touch in touches {
            let origTouchPos:CGPoint = touch.locationInView(touch.view)
            NSLog("TouchEnd (%u): %f %f", touch.hash, origTouchPos.x,  origTouchPos.y);
            touchUpScreen();
        }
    }
    
    
    
    
}
