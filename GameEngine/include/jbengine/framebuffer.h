#ifndef __JB_FRAMEBUFFER__
#define __JB_FRAMEBUFFER__ 1

#include "types.h"

namespace JB {
	class Framebuffer {
	public:
		Framebuffer();
		~Framebuffer();
		///Inits the framebuffer with any given with and height
		///Usually the same as your screen resolution
		void Init(const unsigned int width, const unsigned int height);
		///Framebuffer id
		unsigned int getID();
		///Color texture id
		unsigned int getColorID();
		///Depth texture id
		unsigned int getDepthID();

	private:
		struct FrameData;
		FrameData* framedata_;
	};
}
#endif // !__JB_FRAMEBUFFER__