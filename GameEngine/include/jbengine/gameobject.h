#ifndef __JB_GAMEOBJECT__
#define __JB_GAMEOBJECT__ 1

#include "types.h"
#include "material.h"
#include "camera.h"
#include "geometry.h"
#include "texture.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace JB {
	class GameObject {
	public:
		GameObject();
		~GameObject();

		/// First of all, set and load material & geometry
		void setGeometry(Geometry* geo);
		void setMaterial(Material* mat);
		///Optional
		void setTexture(Texture* tex);
		/// Enum Shape
		void LoadGeometry(JB::Shape shape);
		void LoadMaterial(const char* vertexshader, const char* fragmentshader);
		/// Call once after sets and loads
		void compile();
		/// Call at update
		void execute(Camera cam);

		/// Up to 10 light sources, call it once for each light with different id
		/// intensity range: 0-1
		void setLightSource(Vec3 dir,
			float intensity,
			unsigned int id,
			unsigned int depthID,
			glm::mat4 lightspace);

		/// Getters
		Vec3 getPosition();
		glm::mat4 transform_;
		///Setters
		void setTextureID(unsigned int id);
		void setTextureShadows(unsigned int id, unsigned int id2);
	private:
		Geometry* geo_;
		Material* mat_;
		Texture* tex_;
		Vec3 position_;
		Vec3 rotation_;
		Vec3 scale_;
		bool loadgeo_;
		bool loadmat_;
		bool usingtex_;
		unsigned int program_;
	};
}
#endif // __JB_GAMEOBJECT__