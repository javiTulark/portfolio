#ifndef __JB_LUA_HANDLER__
#define __JB_LUA_HANDLER__ 1

#include "lua.hpp"
#include "gamestate.h"
#include "shaders.h"

namespace JB {
	class LuaHandler {
	public:

		GameState* init();
		void execute_file(char* file_lua);

		lua_State *L;
	};
}
#endif // !__JB_LUA_HANDLER__


