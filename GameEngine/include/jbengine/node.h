#ifndef __JB_NODE__
#define __JB_NODE__ 1

#include <vector>
#include <memory>
#include "scoped_ptr.h"
#include "types.h"
#include "gameobject.h"
#include "geometry.h"
#include "material.h"
#include "light.h"
#include "Engine.h"
#include "components.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace JB {

	class Component;
	class Node{
	public:
		Node();
		~Node();
		Node* createRoot();
		Node* addChild();
		void removeChild(Node **n);

		bool setComponent(Components component);
		void removeComponent(Components component);

		///Used for gameobjects
		void LoadGeometry(JB:: Shape shape);
		void LoadMaterial(const char* vertexshader, const char* fragmentshader);
		void compileGameObject();
		void execute(Camera cam);

		///Used for Lights
		void ApplyLight(Node* light);
		void executeLight();
		Vec3 getDirection();
		glm::mat4 getViewMatrix();
		glm::mat4 getlightSpaceMatrix();
		void setDirection(Vec3 dir);
		Camera getCam();
		unsigned int getDepthID();

		///Used for sineTransform
		void setAxisMovement(bool axisX, bool axisY, bool axisZ, float amplitude, float speed );

		///Setters
		void setPosition(Vec3 v);
		void setRotation(Vec3 v);
		void setScale(Vec3 v);
		void setTexture(Texture* tex);
		void setTextureID(unsigned int id);
		void setTextureShadows(unsigned int id, unsigned int id2);
		void setRenderSize(jb_float x, jb_float y);

		///Getters
		Vec3 getPosition();
		Vec3 getScale();
		int getID();
		float getIntensity();

		int id_;
		Node* father_;
		bool is_light_;

	private:
		std::vector<std::unique_ptr<Node>> children_;
		GameObject game_object_;
		Geometry geo_;
		Material mat_;
		Light light_; //Used if i'm a light
		float light_intensity_;
		int light_id_;

		Vec3 position_;
		Vec3 rotation_;
		glm::vec3 scale_;
		glm::mat4 transform_parent_;
		glm::mat4 transform_;


		///AxisTransform
		void updateAxis();
		bool axisX_, axisY_, axisZ_;
		float amplitude_;
		float speed_;

		///Components functions
		void SetGameObject();
		void SetLight();
		void SetOther();

	};
}
#endif // !__JB_NODE__

