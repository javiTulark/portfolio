#ifndef __JB_CHRONO__
#define __JB_CHRONO__ 1

#include "types.h"

namespace JB {
	class Chrono {
	public:
		Chrono();
		~Chrono();

	 void Begin();
	 void End();
	 ///Prints the value
	 void Show(const char* str);

	private:
		double init_;
		double current_;
		double end_;
	};
}
#endif // !__JB_CHRONO__