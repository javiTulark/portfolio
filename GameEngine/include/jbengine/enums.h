#ifndef __JB_ENUMS__
#define __JB_ENUMS__ 1

namespace JB {

	enum Shape {
		kShape_None = 0,
		kShape_Triangle,
		kShape_Quad,
		kShape_Cube,
		kShape_Sphere,
		kShape_Donut,
		kShape_RenderQuad
	};

	enum GeometryAttrib {
		kGeometryAttrib_Position = 0,
		kGeometryAttrib_Normal,
		kGeometryAttrib_UV
	};

	enum Components {
		kComponent_GameObject = 0,
		kComponent_Light,
		kComponent_sineTransform //eje, amplitud, velocidad
	};

	enum CommandType {
		kInvalid,
		kDraw,
		KLight
	};

	enum Types {
		T_NULL = 0,
		T_INT = 1,
		T_INT2 = 2,
		T_INT3 = 3,
		T_UINT = T_INT,
		T_UINT2 = T_INT2,
		T_UINT3 = T_INT3,
		T_FLOAT = T_INT,
		T_FLOAT2 = T_INT2,
		T_FLOAT3 = T_INT3
	};
}

#endif // __JB_ENUMS__