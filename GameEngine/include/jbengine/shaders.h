#ifndef __JB_SHADERS__
#define __JB_SHADERS__ 1

namespace JB {

	//-------------------------------------------------------------
	//Default shader for a material without texture
	//-------------------------------------------------------------
	static const char* vertex = "#version 330 core\n"
		"layout (location = 0) in vec3 position;\n"
		"layout (location = 1) in vec3 normal;\n"
		"layout (location = 2) in vec2 uv;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"out vec3 normal_f;\n"
		"void main() {\n"
		"  normal_f = mat3(model) * normalize(normal);\n"
		"  gl_Position = projection * view * model * vec4(position, 1.0);\n"
		"}\n\0";

	static const char* fragment = "#version 330 core\n"
		"out vec4 color;\n"
		"in vec3 normal_f;\n"
		"void main() {\n"
		"  vec3 UL = vec3(1.0,1.0,1.0);\n"
		"  vec3 result = (normalize(normal_f) + UL)/2;\n"
		"  color = vec4(result, 1.0f);\n"
		"}\n\0";

	//-------------------------------------------------------------
	//Complex shader that recieves color and shadow textures, and up to 10 lights
	//-------------------------------------------------------------
	static const char* vertexTex = "#version 330 core \n"
		"layout(location = 0) in vec3 position; \n"
		"layout(location = 1) in vec3 normal; \n"
		"layout(location = 2) in vec2 uv; \n"

		"uniform mat4 model; \n"
		"uniform mat4 view; \n"
		"uniform mat4 projection;\n"
		"uniform vec4 lightpos[10];\n"			
		"uniform vec3 campos;\n"					
		"uniform mat4 shadowView;\n"	// in projection*view de la camara de la luz		
		"uniform mat4 shadowView2;\n"	

		"out vec4 LightPos[10]; \n"
		"out vec2 TexCoord; \n"			
		"out vec3 fNormal; \n"
		"out vec3 fPosition; \n"
		"out vec3 CameraPos;\n"
		"out vec4 lightSpace;\n"
		"out vec4 lightSpace2;\n"			 //	 out

		"void main() {\n"
		"  CameraPos = campos;\n"
		"  fNormal = mat3(model) * normalize(normal);\n"
		"  fPosition = position;\n"
		"  LightPos = lightpos;\n"
		"  TexCoord = vec2(uv.x, uv.y);\n"

		"  vec3 fragpos = vec3(model * vec4(position, 1.0));\n"
		"  lightSpace = vec4(shadowView * vec4(fragpos, 1.0));"
		"  lightSpace2 = vec4(shadowView2 * vec4(fragpos, 1.0));"
		"  gl_Position = (projection * view * model) * vec4(position, 1.0f);\n"
		"}\n\0";

	static const char* fragmentTex = "#version 330 core\n"
		"in vec2 TexCoord; \n"
		"in vec4 LightPos[10]; \n"
		"in vec3 fNormal;\n"
		"in vec3 fPosition;\n"
		"in vec3 CameraPos;\n"
		"in vec4 lightSpace;\n"
		"in vec4 lightSpace2;\n"		 //in

		"out vec4 color; \n"
		"uniform sampler2D texture_1; \n"
		"uniform sampler2D texture_2; \n"		
		"uniform sampler2D texture_3; \n"				 //
		"vec4 texColor;\n"
		"vec3 lightDir;\n"

		"float CompareDepthFragment(vec4 v, sampler2D tex) {\n"
		"  vec3 projCoords = v.xyz / v.w;\n"
		"  projCoords = projCoords * 0.5 + 0.5;\n"
		"  float closestDepth = texture(tex, projCoords.xy).r;\n"
		"  float currentDepth = projCoords.z;\n"
		"  float shadow = (currentDepth- 0.005) > closestDepth ? 1.0 : 0.0;\n"

		"  return shadow;\n"
		"}\n"

		"void main() {\n"
		"  vec3 white = vec3(1.0, 1.0, 1.0);\n"
		"  vec2 coord = vec2(1.0-TexCoord.x,TexCoord.y);\n"
		"  texColor = texture(texture_1, coord);\n"
		"  vec3 result = vec3(0.0,0.0,0.0);\n"

		"  for(int i = 0; i < 10; i++){\n"
		// Ambient
		"    float ambientStrength = 0.1f;\n"
		"    vec3 ambient = ambientStrength * white * LightPos[i].a;\n"

		// Diffuse
		"    vec3 norm = normalize(fNormal);\n"
		"    lightDir = normalize(LightPos[i].xyz - fPosition);\n"
		"    float diff = max(dot(norm, lightDir), 0.0);\n"
		"    vec3 diffuse = diff * white * LightPos[i].a;\n"

		// Specular
		"    float specularStrength = 0.5f;\n"
		"    vec3 viewDir = normalize(-CameraPos - fPosition);\n"
		"    vec3 reflectDir = reflect(-lightDir, norm);\n"
		"    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);\n"
		"    vec3 specular = specularStrength * spec * white * LightPos[i].a;\n"

		"   result += (ambient + diffuse + specular) * white *0.5;\n"
		"  }\n"

		// Shadow
		"  float shadow = CompareDepthFragment(lightSpace, texture_2); \n"
		"  float shadow2 = CompareDepthFragment(lightSpace2, texture_3); \n"		//

		// Draw
		"  if(texColor.a < 0.05){ discard;}\n"
		"  else if(LightPos[0].x == 0 && LightPos[0].y == 0 && LightPos[0].z == 0){\n"
		"    color = texColor;\n"
		"  }\n"
		"  else{\n"
		"    color =  (((1.0-shadow) + (1.0-shadow2))*0.5) * texColor * vec4(result, 1.0) + 0.15;\n"
		"  }\n"
		"}\n\0";


	//-------------------------------------------------------------
	//Basic shader that simply draws any given texture
	//-------------------------------------------------------------
	static const char* BasicVertex = "#version 330 core\n"
		"layout (location = 0) in vec3 position;\n"
		"layout (location = 1) in vec3 normal;\n"
		"layout (location = 2) in vec2 uv;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"out vec2 TexCoord;\n"

		"void main() {\n"
		"  TexCoord = vec2(uv.x, uv.y);\n"
		"  gl_Position = projection * view * model * vec4(position, 1.0);\n"
		"}\n\0";

	static const char* BasicFragment = "#version 330 core\n"
		"in vec2 TexCoord;\n"
		"out vec4 color;\n"

		"uniform sampler2D texture_1;\n"
		"vec4 texColor;\n"

		"void main() {\n"
		"  texColor = texture(texture_1, TexCoord);\n"
		"  color = texColor; \n"
		"}\n\0";

	//-------------------------------------------------------------
	//Special shader used for a cubemap
	//-------------------------------------------------------------
	static const char* vertexCubeMap = "#version 330 core\n"
		"layout(location = 0) in vec3 position; \n"
		"out vec3 TexCoords; \n"
		"uniform mat4 projection; \n"
		"uniform mat4 view; \n"

		"void main() {\n"
		"  vec4 pos = projection * view * vec4(position, 1.0); \n"
		"  gl_Position = pos.xyww; \n"
		"  TexCoords = position; \n"
		"}\n\0";

	static const char* fragmentCubeMap = "#version 330 core\n"
		"in vec3 TexCoords;\n"
		"out vec4 color;\n"
		"uniform samplerCube skybox;\n"

		"void main() {\n"
		"  color = texture(skybox, TexCoords);\n"
		"}\n\0";

	//-------------------------------------------------------------
	//Simple vertex for a post process
	//-------------------------------------------------------------
	static const char* vertexPostProcess = "#version 330 core\n"
		"layout (location = 0) in vec3 position;\n"
		"layout (location = 1) in vec3 normal;\n"
		"layout (location = 2) in vec2 uv;\n"
		"uniform mat4 model;\n"
		"uniform mat4 view;\n"
		"uniform mat4 projection;\n"
		"out vec2 TexCoord;\n"

		"void main() {\n"
		"  TexCoord = vec2(uv.x, uv.y);\n"
		"  gl_Position = projection * view * model * vec4(position, 1.0);\n"
		"}\n\0";

	//-------------------------------------------------------------
	//fragment used for the first validation postprocess
	//-------------------------------------------------------------
	static const char* fragmentPostProcessDesaturate = "#version 330 core\n"
		"in vec2 TexCoord; \n"
		"in vec3 fPosition;\n"
		"uniform sampler2D texture_1; \n"
		"uniform sampler2D texture_2; \n"

		"out vec4 color; \n"

		"void main() {\n"
		"  vec4 c = texture(texture_1, TexCoord); \n"
		//"  vec4 d = texture(texture_2, TexCoord); \n"
		"  float a = (fPosition.z*0.5) + 0.5;\n"
		"  color = c*(1.0-a) + c.g*a;\n"
		"}\n\0";
	
	//-------------------------------------------------------------
	// fragment used for a blur effect postprocess
	//-------------------------------------------------------------
	static const char* fragmentPostProcessBlur = "#version 330 core\n"
		"in vec2 TexCoord; \n"
		"out vec4 color; \n"
		"uniform sampler2D texture_1; \n"
																									 
		"float horizontal = 1.0/700;\n"
		"float vertical = 1.0/700;\n"
		"void main() {\n"
		"  vec4 sum = vec4(0.0);\n"
		"  for (int x = -1; x <= 1; x++){\n"
		"  	 for (int y = -1; y <= 1; y++){\n"
		"      sum += texture(texture_1,vec2(TexCoord.x + x * horizontal, TexCoord.y + y * vertical)) *0.11;\n"
		"     }\n"
		"  }\n"
		"  color = sum;\n"
		"}\n\0";

}
#endif // !__JB_SHADERS__