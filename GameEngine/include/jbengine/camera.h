#ifndef __JB_CAMERA__
#define __JB_CAMERA__ 1

#include "texture.h"
#include "types.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace JB {
	class Camera {
	public:
		Camera();
		~Camera();

		///Mandatory calls
		void setPosition(Vec3 position);
		void setTarget(Vec3 target);
		void setWindowSize(jb_float width, jb_float length);

		///Optional. Default values are 5.0f and 50.0f
		void setNearFar(jb_float nearPlane, jb_float farPlane);

		///Only AFTER the 3 mandatory have been called
		void update(int ptogram,
			glm::mat4 model,
			unsigned int texture_id,
			unsigned int texture_id2,
			unsigned int texture_id3,
			JB::Vec4* ldir);

		glm::mat4 getViewMatrix();
		jb_float getFoV();
		jb_float getAspectRatio();
		///out projection_ * view_
		glm::mat4 getlightSpaceMatrix();

	private:
		jb_float field_of_view_;
		jb_float near_plane_;
		jb_float far_plane_;
		jb_float aspect_ratio_;

		glm::vec3 cameraPosition_;
		glm::vec3 cameraDirection_;
		glm::vec3 cameraRight_;
		glm::vec3 cameraUp_;
		glm::vec3 up_vector_;
		glm::vec3 target_;
		glm::mat4 lightSpaceMatrix_;

		glm::mat4 model_;
		glm::mat4 view_;
		glm::mat4 projection_;

		//model * view * projection
		glm::mat4 mvp_;
	};
}
#endif // __JB_CAMERA__


