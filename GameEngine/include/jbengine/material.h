#ifndef __JB_MATERIAL__
#define __JB_MATERIAL__ 1

#include "camera.h"
#include "types.h"
#include "scoped_array.h"
#include "scoped_ptr.h"
#include "texture.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace JB {
	class Material {

	public:
		Material();
		~Material();
		glm::mat4 transform_;

		/**
		* @brief Loads vertex and fragment shaders into our ptr_.
		* @param fragmentshader: Pointer to the desired fragment shader.
		* @param vertexshader: Pointer to the desired vertex shader.
		*/
		void load(const char* fragmentshader, const char* vertexshader);

		/// @brief Compiles vertex, frangment & program, deletes pointer if compile error.
		void compile();
		unsigned int getProgram();

		/// @brief Return true if material is loaded and compiled.
		bool ready();
		///Use program with any given camera
		void use(Camera cam);

	private:
		struct Data;
		scoped_ptr<Data> ptr_;

		friend class GameObject;
		bool usingtex_;
		int texID_;
		int texID2_;
		int texID3_;
		Vec3 getPosition();
		Vec4 lightDir_[10];
		glm::mat4 lightSpace_;

		Texture* tex_;
	};
}

#endif //!__JB_MATERIAL__