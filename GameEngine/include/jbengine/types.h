#ifndef __JB_TYPES__
#define __JB_TYPES__ 1

#include <stdint.h>
//#include "node.h"

namespace JB {
#ifdef _WIN32
	typedef int8_t jb_int8;
	typedef int16_t jb_int16;
	typedef int32_t jb_int32;
	typedef int64_t jb_int64;

	typedef uint8_t jb_uint8;
	typedef uint16_t jb_uint16;
	typedef uint32_t jb_uint32;
	typedef uint32_t jb_uint64;

	typedef char jb_char;
	typedef float jb_float;
	typedef double jb_double;
	typedef bool jb_bool;

#endif // _WIN32

	struct Vec2 {
		jb_float x, y;
	};

	struct Vec3 {
		jb_float x, y, z;
	};

	struct Vec4 {
		jb_float x, y, z, w;
	};

	struct Mat3 {
		Vec3 c1;
		Vec3 c2;
		Vec3 c3;
	};
	
}
#endif // !__JB_TYPES__