#ifndef __JB_GAMESTATE__
#define __JB_GAMESTATE__ 1

#include "node.h"
#include "command.h"
#include "bx\thread.h"
#include "jsoncons/json.hpp"

namespace JB {
	class GameState {
	public:
		GameState();

		Camera cam;
	
		Node root_;
		std::vector<Node*> nodes_;
		JB::Texture Tex_Sun[10], Tex_Earth[10], Tex_Moon[10];
		std::vector<Texture*> texture_;
		
		
		std::vector<JB::Command> comand_list;
		JB::Command comand_aux;

		bx::Semaphore s1, s2;

		jsoncons::json root;
		std::vector<jsoncons::json> data;
		jsoncons::json jsonstart;
		jsoncons::json jsonend;

		float startUpdate;
		float endUpdate;
		float startDraw;
		float endDraw;

		void StartChrono(const char *name, int frame,  float time);
		void EndChrono(const char *name, int frame,  float time);
		

	};
}
#endif //__JB_GAMESTATE__
