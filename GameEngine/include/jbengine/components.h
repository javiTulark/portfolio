#ifndef __JB_COMPONENTS__
#define __JB_COMPONENTS__ 1

#include "types.h"

namespace JB {
	class Node;
	class Component {
	public:

		JB::jb_uint32 type_;
		JB::jb_bool enable_;
		Node* node_;

		Component();
		~Component();

		void SetGameObject(Node *n);
		void SetLight();
		void SetOther();

	};
}
#endif //__JB_COMPONENTS__