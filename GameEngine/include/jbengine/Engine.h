#ifndef __JB_ENGINE__
#define __JB_ENGINE__ 1

#include "window.h"
#include "chrono.h"
#include <glm/glm.hpp>

namespace JB {
	class Engine {
	private:
		static Engine *instance_;
		Engine();
		~Engine();
	public:
		void Init();
		JB::Window* window_;
		JB::Chrono* chrono_;

		unsigned int fbo;
		unsigned int shadowbuffer[10];
		unsigned int depthID[10];
		glm::mat4 lightTransform[10];

		int light_id_;
		static Engine* Instance() {
			if (!instance_) {
				instance_ = new Engine;
			}
			return instance_;
		}
	};
}
#endif // __JB_ENGINE__