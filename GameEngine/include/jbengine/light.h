#ifndef __JB_LIGHT__
#define __JB_LIGHT__ 1

#include "camera.h"
#include "types.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace JB {
	class Light {
	public:
		Light();
		~Light();

		void init(unsigned int id);
		///get framebuffer id
		unsigned int getFBO();
		///get color texture id
		unsigned int getColor();
		///get depth texture id
		unsigned int getDepth();
		///get view_
		glm::mat4 getViewMatrix();
		///get projection_ * view_
		glm::mat4 getlightSpaceMatrix();
		///get vec4{pos.x, pos.y, pos.x, light_intensity}
		JB::Vec4 getInfo();

		Vec3 position_;
		Vec3 target_;
		glm::vec3 direction_;
		Camera getCam();

	private:
		jb_float intensity_;
		glm::mat4 lightSpaceMatrix_;

		struct Data;
		Data* ptr_;
	};
}
#endif // !__JB_LIGHT__
