#ifndef __JB_GEOMETRY__
#define __JB_GEOMETRY__ 1

#include "types.h"
#include "scoped_array.h"
#include "scoped_ptr.h"
#include "enums.h"

namespace JB {
	class Geometry  {
	public:

		struct GeoData {
			struct Vertex {
				Vec3 position;
				Vec3 normal;
				Vec2 uv;
			};
			scoped_array<jb_uint32> index;
			scoped_array<Vertex> vertex;
		};

		/// @brief Initializes to false or nullptr the contents of Data.
		Geometry();

		/// @brief Empty.		
		~Geometry();

		/**
		* @brief Loads geometry depending on its shape.
		* @param Triangle, quead or cube from our shape enum
		*/
		void load(Shape shape);

		/// @brief Generates buffers, enables and binds them.
		void compile();

		///@brief Return trus if the geometry is loaded and compiled.
		bool ready();

		///@brief Binds and draws elements.
		void draw();

		void setPosition(Vec3 pos);
		void setRenderSize(jb_float x, jb_float y);
		Vec3 Normalize(Vec3 A, Vec3 B);

	private:
		jb_uint32 stride_;
		Vec3 vec3Add(Vec3 A, Vec3 B);
		struct Data;
		scoped_ptr<Data> ptr_;
	};
}

#endif // __JB_GEOMETRY__
