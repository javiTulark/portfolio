#ifndef __JB_CUBEMAP__
#define __JB_CUBEMAP__ 1

#include "types.h"
#include "camera.h"
#include <vector>
namespace JB {
	class Cubemap {
	public:
		Cubemap();
		~Cubemap();
		///vector length 6
		void Load(std::vector<const char*> faces);
		///Execute from any given camera
		void execute(JB::Camera cam);

	private:
		struct CubeData;
		CubeData* ptr_;
	};
}
#endif // !__JB_CUBEMAP__
