#ifndef __JB_TEXTURE__
#define __JB_TEXTURE__ 1
#include "types.h"
#include "scoped_ptr.h"

namespace JB {
	class Texture {
	public:

		Texture();
		~Texture();
		///str: route to the image from the .sln file
		void load(const char* str);
		///Call compile once the route has been set
		void compile();
		///get texture id
		jb_uint32 getTexID();

	private:
		struct Data;
		scoped_ptr<Data> ptr_;
	};
}
#endif // __JB_TEXTURE__