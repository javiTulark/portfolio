#ifndef __JB_COMMAND__
#define __JB_COMMAND__ 1

#include "enums.h"
#include "node.h"
#include "camera.h"
namespace JB {
	class Command {
	public:
		Command();
		~Command();

		JB::CommandType comand_type;
		Node *node;
		JB::Camera *camera;

		///Executes any command through the default camera
		void execute();
		///Executes any command through any given camera. Used for lights to cast shadows
		void executeShadow(JB::Camera lightcam);
		///Creates a command type from the enum list
		void Create(JB::CommandType ct, Node *n, JB::Camera *cam);
	};
}
#endif //__JB_COMMAND__