#ifndef __JB_WINDOW__
#define __JB_WINDOW__ 1

#include "types.h"
#include "texture.h"

namespace JB {
	class Window
	{
	public:
		Window();
		~Window();
		int Init();
	
		///returns true if window is closed
		bool IsClosed();
		///init framebuffer with the given framebuffer id
		void InitRender(const int id);
		///end the previous framebuffer
		void EndRender();
		///swap buffers for next frame
		void Swap();
		///Clears default framebuffer
		void Clear();
		///Inits default framebuffer
		void initFrameBuffer();

		///BackgroundColor of the default scene
		void ClearColor(jb_float r, jb_float g, jb_float b, jb_float a);
		///Background color of the framebuffer
		void ClearRenderColor(jb_float r, jb_float g, jb_float b, jb_float a);

	private:
		struct WindowData;
		jb_float R_, G_, B_, A_;
		jb_float R2_, G2_, B2_, A2_;
		WindowData *WindowData_;
		bool FB_used_;
	};

}
#endif // !__JB_WINDOW__
