#include "Engine.h"
#include "node.h"
#include "window.h"
#include "geometry.h"
#include "material.h"
#include "gameobject.h"
#include "shaders.h"
#include "Windows.h"
#include "camera.h"
#include "texture.h"
#include "chrono.h"
#include "cubemap.h"

#include "glew/include/GL/glew.h"
#include "GLFW/include/GLFW/glfw3.h"
#include "bx\thread.h"

#include "lua.hpp"
#include "luahandler.h"
#include <iostream>
#include <vector>
#include "math.h"
#include <thread> 
#include "command.h"
#include "jsoncons/json.hpp"


typedef struct GameState_T {
	//JB::Camera cam;
	JB::Node* Light[2];
	JB::Texture *Tex, *Tex2, *Tex3;
	JB::Node* Floor;
	/*
	JB::Node* Earth;
	JB::Node* Sun;
	JB::Node* Moon;
	*/
	int ren_cont;
	int gp_cont;
	bx::Semaphore s1, s2;

};
GameState_T gs, gs2;

void Prepare(JB::Engine* engine, JB::GameState *gsl) {

	gs.gp_cont = 0;
	gs.ren_cont = 0;

	JB::Vec3 scale;
	scale = { 20.0f,20.0f,20.0f };
	/*

	//////////////////////  Figures  //////////////////
	gs.Root = gs.Root->createRoot();

	gs.Sun = gs.Root->addChild();
	gs.Sun->setComponent(JB::kComponent_GameObject);
	gs.Sun->setComponent(JB::kComponent_sineTransform);
	gs.Sun->setAxisMovement(true, false, false, 5, 2);
	gs.Sun->setScale(scale);
	gs.Tex2 = new(JB::Texture);
	gs.Tex2->load("../../data/images/sun.jpg");	 ///

	gs.Sun->LoadGeometry(JB::kShape_Sphere);
	gs.Sun->LoadMaterial(JB::vertexTex, JB::fragmentTex);
	gs.Sun->setTexture(gs.Tex2); ///
	gs.Sun->compileGameObject();

	//-
	gs.Earth = gs.Sun->addChild();
	gs.Earth->setComponent(JB::kComponent_GameObject);
	gs.Earth->setComponent(JB::kComponent_sineTransform);
	gs.Earth->setAxisMovement(false, true, false, 5, 2);
	gs.Tex = new(JB::Texture);
	gs.Tex->load("../../data/images/earth.png");	///

	gs.Earth->LoadGeometry(JB::kShape_Sphere);
	gs.Earth->LoadMaterial(JB::vertexTex, JB::fragmentTex);
	gs.Earth->setTexture(gs.Tex);///
	gs.Earth->compileGameObject();
	//-


	gs.Moon = gs.Earth->addChild();
	gs.Moon->setComponent(JB::kComponent_GameObject);
	gs.Moon->setComponent(JB::kComponent_sineTransform);
	gs.Moon->setAxisMovement(false, false, true, 5, 2);
	gs.Tex3 = new(JB::Texture);
	gs.Tex3->load("../../data/images/moon.jpg");	 ////

	gs.Moon->LoadGeometry(JB::kShape_Sphere);
	gs.Moon->LoadMaterial(JB::vertexTex, JB::fragmentTex);
	gs.Moon->setTexture(gs.Tex3);	////
	gs.Moon->compileGameObject();
	*/

	//Cubo con flores
	gs.Floor = gsl->nodes_.at(0)->addChild();
	gs.Floor->setComponent(JB::kComponent_GameObject);
	gs.Tex = new(JB::Texture);
	gs.Tex->load("../../data/images/ground.png");
	gs.Floor->setTexture(gs.Tex);
	gs.Floor->LoadGeometry(JB::kShape_Cube);
	gs.Floor->LoadMaterial(JB::vertexTex, JB::fragmentTex);
	gs.Floor->compileGameObject();

	gsl->nodes_.at(1)->setScale(scale);
	gsl->nodes_.at(1)->setRotation(JB::Vec3{ 90.0f,0.0f,0.0f });
	gsl->nodes_.at(1)->setPosition(JB::Vec3{ 0.0f,0.0f,3.0f });

	//gsl->nodes_.at(2)->setScale(scale);
	//gsl->nodes_.at(2)->setRotation(JB::Vec3{ 90.0f,0.0f,0.0f });
	//gsl->nodes_.at(2)->setPosition(JB::Vec3{ 0.0f,0.0f,-30.0f });

	/*
	*/
	gs.Tex2 = new(JB::Texture);
	gs.Tex2->load("../../data/images/io.jpg");	 ///
	gsl->nodes_.at(1)->setTexture(gs.Tex2);///
	gsl->nodes_.at(1)->compileGameObject();
	//////////////////////  Light  //////////////////
	/*
	*/
	for (int i = 0; i < 2; i++) {
		gs.Light[i] = gsl->nodes_.at(0)->addChild();
		gs.Light[i]->setPosition(JB::Vec3{ (i * 40) - 20.0f,20.0f,0.0f });
		gs.Light[i]->setComponent(JB::kComponent_Light);

		gsl->nodes_.at(1)->ApplyLight(gs.Light[i]);
		gsl->nodes_.at(2)->ApplyLight(gs.Light[i]);
		gsl->nodes_.at(3)->ApplyLight(gs.Light[i]);

	}

	engine->window_->initFrameBuffer();


	/////////////////////  CAMERA  //////////////////
	JB::Vec3 camera_position = { 0.0f, 5.0f, 20.0f };
	gsl->cam.setPosition(camera_position);
	gsl->cam.setTarget(JB::Vec3{ 0.0f,0.0f,0.0f });
	gsl->cam.setWindowSize(1440.0f, 900.0f);

	for (int i = 1; i < gsl->nodes_.size(); i++) {
		gsl->nodes_.at(i)->setTextureShadows(engine->depthID[0], engine->depthID[1]);
	}
}

void normalize(JB::Vec3 & v) {
	float len = sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
	if (len == 0.0f)
		len = 0.0001f;
	v.x /= len;
	v.y /= len;
	v.z /= len;
}


void Update(JB::GameState *gsl) {

	gsl->s1.wait();



	gsl->startUpdate = glfwGetTime();


	//Rotation
	/*
	*/
	JB::jb_float rotation_speed = JB::jb_float(glfwGetTime()) * 50.0f;
	JB::Vec3 v = { 0.0f, rotation_speed*0.5f, 0.0f };
	JB::Vec3 v2 = { 0.0f, rotation_speed*2.0f, 0.0f };
	JB::Vec3 lpos = { 5 * cos((float)glfwGetTime()),0.0f, 5 * sin((float)glfwGetTime()) };
	//JB::Vec3 lpos2 = { 1 * cos((float)glfwGetTime()),0.0f, 1 * sin((float)glfwGetTime()) };
	//JB::Vec3 lpos3 = { 0.0f,1 * cos((float)glfwGetTime()), 1 * sin((float)glfwGetTime()) };

	//gsl.Light->setPosition(lpos);


	//movimientos de los planetas/luces
	//OJO CUIDADO al ponerlo a mano se nos puede salir del vector
	//gsl->nodes_.at(1)->setPosition(lpos);//es la luz
	gsl->nodes_.at(2)->setPosition(lpos);
	gsl->nodes_.at(2)->setRotation(JB::Vec3{ 0.0f,(float)glfwGetTime() * 50, 0.0f });
	//gsl->nodes_.at(3)->setPosition(lpos2);
	//gsl->nodes_.at(4)->setPosition(lpos3);

	gs.Floor->setRotation(JB::Vec3{ 0.0f,(float)glfwGetTime() * 50, 0.0f });

	/*
	for (int i = 0; i < 1000; i++)
	{
	printf("\nUpdate pos %d, frame %d", i, gs.gp_cont);
	}
	*/
	gs.gp_cont++;
	//posicion(0, 0); printf("\nGameplay %d", gs.gp_cont);

	/*
	gs.comand_aux.Create(JB::KLight,gs.Light,&gsl->cam);
	gs.comand_list.push_back(gs.comand_aux);
	gs.comand_aux.Create(JB::kDraw, gsl->nodes_.at(1), &gsl->cam);
	gs.comand_list.push_back(gs.comand_aux);
	gs.comand_aux.Create(JB::kDraw, gsl->nodes_.at(2), &gsl->cam);
	gs.comand_list.push_back(gs.comand_aux);
	gs.comand_aux.Create(JB::kDraw, gsl->nodes_.at(3), &gsl->cam);
	gs.comand_list.push_back(gs.comand_aux);
	*/

	/*
	*/
	gsl->comand_aux.Create(JB::kDraw, gs.Floor, &gsl->cam);
	gsl->comand_list.push_back(gsl->comand_aux);

	//desde el 1 por que en el 0 esta el Root
	for (int i = 1; i < gsl->nodes_.size(); i++) {
		if (!gsl->nodes_.at(i)->is_light_) {
			//gsl->nodes_.at(i)->ApplyLight(gsl->nodes_.at(1));
			gsl->comand_aux.Create(JB::kDraw, gsl->nodes_.at(i), &gsl->cam);
			gsl->comand_list.push_back(gsl->comand_aux);
		}
		else {
			//gsl->comand_aux.Create(JB::KLight, gsl->nodes_.at(i), &gsl->cam);
			//gsl->comand_list.push_back(gsl->comand_aux);
			printf(".");
		}


	}
	gsl->s2.post();
	gsl->endUpdate = glfwGetTime();
}

void DrawShadow(JB::Engine* engine, JB::GameState *gsl, JB::Camera lightcam) {


	for (int i = 0; i < gsl->comand_list.size(); i++) {
		gsl->comand_list.at(i).executeShadow(lightcam);
	}

}

void Draw(JB::Engine* engine, JB::GameState *gsl) {
		
	//gsl->s1.post();
	/*
	*/
	//for (int i = 0; i < 2000; i++)	 printf("\n");
	for (int i = 0; i < 2; i++) {
		gs.Light[i]->executeLight();

	}

	gs.ren_cont++;
	//posicion(0, 1);  printf("\nRender %d", gs.ren_cont);
	// Draw elements
	if (gsl->comand_list.size() == 0) printf("\n caca");
	else {
		for (int i = 0; i < gsl->comand_list.size(); i++) {
			gsl->comand_list.at(i).execute();
		}

		gsl->comand_list.clear();
	}
	/*
	*/

	/*

	gs2.Earth->execute(gs.cam);
	gs.Sun->execute(gs.cam);
	gs2.Moon->execute(gs.cam);
	*/

	
}




int main(int argc, char** argv) {
	JB::Engine* engine = JB::Engine::Instance();
	engine->Init();
	engine->window_->Init();

	JB::GameState* gs_lua;

	JB::LuaHandler lua_handler;
	gs_lua = lua_handler.init();
	lua_handler.execute_file("../../data/demobua.lua");


	Prepare(engine, gs_lua);
	double current_time = 0.0;
	double diff_time = 0.0;

	/*
	*/
	gs_lua->s1.post();
	std::thread(Update, gs_lua).detach();
	
	gs_lua->s1.post();

	
	while (!engine->window_->IsClosed()) {
	


		std::thread(Update, gs_lua).detach();

		gs_lua->s1.post();
		gs_lua->startDraw = glfwGetTime();

		/////////////////////////////
		for (int i = 0; i < 2; i++) {
			engine->window_->InitRender(engine->shadowbuffer[i]);
			DrawShadow(engine, gs_lua, gs.Light[i]->getCam());
		}
		///////////////////////////
		engine->window_->InitRender(engine->fbo);
		Draw(engine, gs_lua);
		engine->window_->EndRender();
		////////////////////////////////

		engine->window_->Clear();

		engine->window_->Swap();

		gs_lua->endDraw = glfwGetTime();
		
		gs_lua->StartChrono("Update", gs.gp_cont, gs_lua->startUpdate);
		gs_lua->EndChrono("Update", gs.gp_cont, gs_lua->endUpdate);
		gs_lua->StartChrono("Draw", gs.ren_cont, gs_lua->startDraw);
		gs_lua->EndChrono("Draw", gs.ren_cont, gs_lua->endDraw);
		gs_lua->root["traceEvents"] = jsoncons::json(gs_lua->data.begin(), gs_lua->data.end());

	}

	gs_lua->root["displayTimeUnit"] = "ns";
	gs_lua->root["meta_cpu_count"] = 8;
	gs_lua->root["meta_user"] = "aras";
	std::ofstream os("../../data/datajson.json");
	os << pretty_print(gs_lua->root);

	return 0;
}