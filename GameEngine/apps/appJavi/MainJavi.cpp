#include "Engine.h"
#include "node.h"
#include "window.h"
#include "geometry.h"
#include "material.h"
#include "gameobject.h"
#include "shaders.h"
#include "Windows.h"
#include "camera.h"
#include "texture.h"
#include "chrono.h"
#include "cubemap.h"

#include "glew/include/GL/glew.h"
#include "GLFW/include/GLFW/glfw3.h"
#include "bx\thread.h"

#include "lua.hpp"
#include "luahandler.h"
#include <iostream>
#include <vector>
#include "math.h"
#include <thread> 
#include "command.h"
#include "jsoncons/json.hpp"

const int totalShadows = 2;

struct GameState_T {
	JB::Node* Light[totalShadows];
	JB::Texture Tex, Tex2;
	int draw_counter;
	int update_counter;
	bx::Semaphore s1, s2;
};
GameState_T gs;

void Prepare(JB::Engine* engine, JB::GameState *gsl) {
	gs.update_counter = 0;
	gs.draw_counter = 0;

	gs.Tex.load("../../data/images/ground.png");
	gs.Tex2.load("../../data/images/image.jpg");

	//Nodes
	gsl->nodes_.at(1)->setScale(JB::Vec3{20.0f,20.0f,20.0f});
	gsl->nodes_.at(1)->setRotation(JB::Vec3{ 90.0f,0.0f,0.0f });
	gsl->nodes_.at(1)->setPosition(JB::Vec3{ 0.0f,0.0f,3.0f });
	gsl->nodes_.at(1)->setTexture(&gs.Tex);
	gsl->nodes_.at(1)->compileGameObject();

	gsl->nodes_.at(3)->setScale(JB::Vec3{ 0.5f,0.5f,0.5f });

	gsl->nodes_.at(5)->setScale(JB::Vec3{ 0.5f,0.5f,0.5f });
	gsl->nodes_.at(5)->setTexture(&gs.Tex2);
	gsl->nodes_.at(5)->compileGameObject();

	gsl->nodes_.at(8)->setTexture(&gs.Tex2);
	gsl->nodes_.at(8)->setScale(JB::Vec3{ 0.5f,0.5f,0.5f });


	for (int i = 0; i < totalShadows; i++)	{
		gs.Light[i] = gsl->nodes_.at(0)->addChild();
		if(i==0)gs.Light[0]->setPosition(JB::Vec3{ 20.0f,30.0f,0.0f });
		if(i==1)gs.Light[1]->setPosition(JB::Vec3{ 10.0f,30.0f,0.0f });
		gs.Light[i]->setComponent(JB::kComponent_Light);

		gsl->nodes_.at(1)->ApplyLight(gs.Light[i]);
		gsl->nodes_.at(2)->ApplyLight(gs.Light[i]);
		gsl->nodes_.at(3)->ApplyLight(gs.Light[i]);
		gsl->nodes_.at(4)->ApplyLight(gs.Light[i]);
		gsl->nodes_.at(5)->ApplyLight(gs.Light[i]);
		gsl->nodes_.at(6)->ApplyLight(gs.Light[i]);
		gsl->nodes_.at(7)->ApplyLight(gs.Light[i]);
		gsl->nodes_.at(8)->ApplyLight(gs.Light[i]);
		gsl->nodes_.at(9)->ApplyLight(gs.Light[i]);
	}

	//Init default framebuffer
	engine->window_->initFrameBuffer();


	JB::Vec3 camera_position = { 0.0f, 5.0f, 25.0f };
	gsl->cam.setPosition(camera_position);
	gsl->cam.setTarget(JB::Vec3{ 0.0f,0.0f,0.0f });
	gsl->cam.setWindowSize(1440.0f, 900.0f);
										 
	for (unsigned int i = 1; i < gsl->nodes_.size(); i++) {
		gsl->nodes_.at(i)->setTextureShadows(engine->depthID[0], engine->depthID[1]);
	}
}

void Update(JB::GameState *gsl) {

	JB::jb_float rotation_speed = JB::jb_float(glfwGetTime()) * 50.0f;

	gsl->nodes_.at(2)->setRotation(JB::Vec3{ 0.0f,rotation_speed, 0.0f });
	gsl->nodes_.at(3)->setRotation(JB::Vec3{ 0.0f,rotation_speed, 0.0f });
	gsl->nodes_.at(4)->setRotation(JB::Vec3{ 0.0f,rotation_speed*0.5f, 0.0f });
	gsl->nodes_.at(5)->setRotation(JB::Vec3{ rotation_speed,rotation_speed, rotation_speed });

	gsl->nodes_.at(6)->setRotation(JB::Vec3{ 0.0f,rotation_speed*0.1f, 0.0f });
	gsl->nodes_.at(7)->setRotation(JB::Vec3{ 0.0f,rotation_speed*0.2f, 0.0f });
	gsl->nodes_.at(8)->setRotation(JB::Vec3{ 0.0f,rotation_speed*0.3f, 0.0f });
	gsl->nodes_.at(4)->setRotation(JB::Vec3{ 0.0f,rotation_speed*0.4f, 0.0f });

	gs.update_counter++;

	gsl->comand_list.clear();
	for (unsigned int i = 1; i < gsl->nodes_.size(); i++)	{
		if (!gsl->nodes_.at(i)->is_light_) {
			gsl->comand_aux.Create(JB::kDraw, gsl->nodes_.at(i), &gsl->cam);
			gsl->comand_list.push_back(gsl->comand_aux);
		}
		else {
			gsl->comand_aux.Create(JB::KLight, gsl->nodes_.at(i), &gsl->cam);
			gsl->comand_list.push_back(gsl->comand_aux);
		}
	}
}

void DrawShadow(JB::Engine* engine, JB::GameState *gsl,JB::Camera lightcam) {
	for (unsigned int i = 0; i < gsl->comand_list.size(); i++)	{
		gsl->comand_list.at(i).executeShadow(lightcam);
	}
}

void Draw(JB::Engine* engine, JB::GameState *gsl) {
	for (int i = 0; i < totalShadows; i++)	{
		gs.Light[i]->executeLight();
	}

	gs.draw_counter++;
	if (gsl->comand_list.size() == 0) printf("\n empty list");
	else	{
		for (unsigned int i = 0; i < gsl->comand_list.size(); i++)		{
			gsl->comand_list.at(i).execute();
		}
	//	gsl->comand_list.clear();
	}
}


int main(int argc, char** argv) {
	JB::Engine* engine = JB::Engine::Instance();
	engine->Init();
	engine->window_->Init();

	//Loading nodes, locations, shapes and textures from lua
	JB::GameState* gs_lua;
	JB::LuaHandler lua_handler;
	gs_lua = lua_handler.init();
	lua_handler.execute_file("../../data/demojavi.lua");

	//Prepare scene
	Prepare(engine, gs_lua);


	while (!engine->window_->IsClosed()) {

		Update(gs_lua);
		//Render the scene once from each light
		for (int i = 0; i < totalShadows; i++){
			engine->window_->InitRender(engine->shadowbuffer[i]);
			DrawShadow(engine, gs_lua, gs.Light[i]->getCam());
			engine->window_->EndRender();
		}
		//Render the scene from the front default camera
		engine->window_->InitRender(engine->fbo);
		Draw(engine, gs_lua);
		engine->window_->EndRender();

		engine->window_->Clear();
		engine->window_->Swap();

	}



	return 0;
}
