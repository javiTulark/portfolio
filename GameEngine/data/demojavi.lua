--Geomtry->"Cube","Sphere","Quad","Triangle","Donut"
--Texture->"Sun","Earth","Moon"
--Light->true, false
--addNode(father,X,Y,Z,Geometry,Light,texture)

local R = getRoot()

--local n1 = addNode(R)
local n1 = addNode(R,0,0,0,"Quad",false,"Moon")
local n2 = addNode(R,0,0,0,"Sphere",false,"Sun")
local n3 = addNode(n2,2,0,0,"Sphere",false,"Earth")
local n4 = addNode(n2,4,0,0,"Sphere",false,"Earth")
local n5 = addNode(n4,2,2,0,"Cube",false,"Earth")
local n6 = addNode(n2,8,0,2,"Sphere",false,"Earth")
local n7 = addNode(n2,-10,2,0,"Quad",false,"Earth")
local n8 = addNode(n2,0,7,4,"Donut",false,"Earth")
local n9 = addNode(n2,0,2,-14,"Sphere",false,"Earth")
--local n11 = addNode(n9,0,1,1,"Sphere",false,"Sun")
--local n12 = addNode(n2,0,1,1,"Sphere",false,"Moon")
--local n13 = addNode(n7,0,1,1,"Sphere",false,"Sun")
--local n14 = addNode(n3,0,1,1,"Sphere",false,"Moon")
--local n15 = addNode(n4,0,1,1,"Sphere",false,"Sun")
--local n16 = addNode(n5,0,1,1,"Sphere",false,"Moon")

