solution "_JB_Solution"

platforms {"x32"}
configurations {"Debug", "Release"}

location "build/JB_Engine"
  project "JB_Engine"
    language "C++"
    kind "staticlib"

    files {"include/jbengine/**.h","src/jbengine/**.cpp"}
    

    includedirs {"include/jbengine"}
    includedirs {"src/deps","src/deps/glew/include"}
    includedirs {"src/deps/bx/include"}


--Include glew headers and sources

    files{
      "src/deps/glew/src/glew.c",
      --"src/deps/glew/include/GL/glew.h"
    }

--Include bx headers and sources

	files{
		"src/deps/bx/src/*.cpp",
		"src/deps/bx/include/**.h",
		"src/deps/bx/include/**.inl"
	}	
--Include common headers and sources for jsoncons

  files{
    "src/deps/jsoncons/**.h"
  }
  
--Include common sources for glm

    files{
      "src/deps/glm/src/**.cpp",

    }
    
--Include common headers for glfw

    files {
      "src/deps/GLFW/src/internal.h",
      "src/deps/GLFW/src/glfw_config.h",
      "src/deps/GLFW/include/GLFW/glfw3.h",
      "src/deps/GLFW/include/GLFW/glfw3native.h",
    }

--Include common sources for glfw

    files{
      "src/deps/GLFW/src/context.c",
      "src/deps/GLFW/src/init.c",
      "src/deps/GLFW/src/input.c",
      "src/deps/GLFW/src/monitor.c",
      "src/deps/GLFW/src/vulkan.c",
      "src/deps/GLFW/src/window.c"
    }

--Include sources and headers depending of the os for glfw

    if os.is("windows") then
      files {
        "src/deps/GLFW/src/win32_platform.h",
        "src/deps/GLFW/src/win32_joystick.h",
        "src/deps/GLFW/src/wgl_context.h",
        "src/deps/GLFW/src/egl_context.h",
        "src/deps/GLFW/src/win32_init.c",
        "src/deps/GLFW/src/win32_joystick.c",
        "src/deps/GLFW/src/win32_monitor.c",
        "src/deps/GLFW/src/win32_time.c",
        "src/deps/GLFW/src/win32_tls.c",
        "src/deps/GLFW/src/win32_window.c",
        "src/deps/GLFW/src/wgl_context.c",
        "src/deps/GLFW/src/egl_context.c"
      }
    end

--Include sources and headers for SOIL

files{
  "src/deps/soil/src/*.c", 
  "src/deps/soil/src/*.h"
}

includedirs{
  "src/deps/soil/src/."
}

links{
  "src/deps/soil/lib/libSOIL"
}

defines{
  "WIN32",
  "_DEBUG",
  "_LIB"
}

--Include sources and headers for LUA

  files{
    "src/deps/lua/src/*.h",
    "src/deps/lua/src/*.c"
  }

  excludes "src/deps/lua/src/luac.c"
  excludes "src/deps/lua/src/lua.c"

  includedirs "src/deps/lua/src"
  includedirs "data"
-----------------------------------------------
defines{
        "GLEW_STATIC"
      }
configuration "Debug"
  flags {"Symbols"}

configuration "Release"
  flags {"OptimizeSize"}

configuration "windows"
  targetdir   "build/executable"
  defines{
    "_GLFW_WIN32"
  }
  links { "opengl32" }
 
------------------------------------------------------

function app(name)
  project(name)
  language "C++"
  --kind "windowedApp"
  kind "consoleApp"

  platforms {"x32"}
  configurations {"Debug", "Release"}

  files("apps/"..name.."/*.cpp")
 
  includedirs {"include/jbengine"}
  includedirs {"src/deps","src/deps/glew/include"}
  includedirs "src/deps/lua/src"
  includedirs {"data","data/images"}
  includedirs {"src/deps/bx/include"}

  configuration "Debug"
    flags {"Symbols"}

  configuration "Release"
    flags {"OptimizeSize"}

  
  configuration "x32"
    targetdir   ("build/executable")

  configuration "windows"
    links {"opengl32","JB_Engine"}
end

app("appBua")
app("appJavi") 
      