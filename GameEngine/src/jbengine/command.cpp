#include "command.h"


namespace JB {

	Command::Command() {

	}
	Command::~Command() {

	}

	void Command::execute() {

		switch (comand_type)
		{
		case JB::kInvalid:
			break;
		case JB::kDraw:
			node->execute(*camera);

			break;
		case JB::KLight:
			node->executeLight();
			break;
		default:
			break;
		}
	}

	void Command::executeShadow(Camera lightcam)
	{
		switch (comand_type)
		{
		case JB::kInvalid:
			break;
		case JB::kDraw:
			node->execute(lightcam);

			break;
		case JB::KLight:
			node->executeLight();
			break;
		default:
			break;
		}
	}

	void Command::Create(CommandType ct, Node * n, Camera *cam)
	{
		comand_type = ct;
		node = n;
		camera = cam;
	}

}