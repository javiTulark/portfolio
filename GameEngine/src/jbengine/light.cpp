#include "light.h"
#include "Engine.h"
#include "framebuffer.h"
#include "glew/include/GL/glew.h"
#include "GLFW/include/GLFW/glfw3.h"
#include <iostream>

struct JB::Light::Data {
	jb_int16 width_, height_;
	GLuint framebuffer;
	GLuint renderbuffer;
	GLuint texture;
	Framebuffer fbo;
	Camera cam;
};

JB::Light::Light() {
	Light::ptr_ = new Light::Data();
}

JB::Light::~Light() {
	delete ptr_;
}

unsigned int JB::Light::getFBO() {
	return ptr_->fbo.getID();
}

unsigned int JB::Light::getColor() {
	return ptr_->fbo.getColorID();
}

unsigned int JB::Light::getDepth() {
	return ptr_->fbo.getDepthID();
}

glm::mat4 JB::Light::getViewMatrix() {
	return ptr_->cam.getViewMatrix();
}

glm::mat4 JB::Light::getlightSpaceMatrix() {
	return lightSpaceMatrix_;
}

void JB::Light::init(unsigned int id) {
	ptr_->cam.setPosition(position_);
	ptr_->cam.setTarget(target_);
	ptr_->cam.setWindowSize(1440.0f, 900.0f);

	glm::vec3 p = { position_.x, position_.y, position_.z };
	glm::vec3 t = { target_.x, target_.y, target_.z };
	direction_ = glm::vec3{ t.x - p.x, t.y - p.y, t.z - p.z };

	//LightSpaceMatrix is the one we send to the shader to cast a shadow
	glm::vec3 cameraDirection_ = glm::normalize(p - t);
	glm::vec3 cameraRight_ = glm::normalize(glm::cross(glm::vec3{ 0.0,1.0,0.0 }, cameraDirection_));
	glm::vec3 cameraUp_ = glm::cross(cameraDirection_, cameraRight_);
	glm::mat4 view_ = glm::lookAt(p, cameraDirection_, cameraUp_);
	glm::mat4 projection_ = glm::perspective(45.0f, 1.0f, 5.0f, 50.0f);
	lightSpaceMatrix_ = projection_*view_;
	Engine::Instance()->lightTransform[id] = lightSpaceMatrix_;


	ptr_->fbo.Init(1440.0f, 900.0f);

	Engine::Instance()->shadowbuffer[id] = ptr_->fbo.getID();
	Engine::Instance()->depthID[id] = ptr_->fbo.getDepthID();

	printf("\nColorBuffer  id: %d", ptr_->fbo.getID());
	printf("\nDepthTexture id: %d", ptr_->fbo.getDepthID());
	printf("\n------------------");
}

JB::Camera JB::Light::getCam() {
	return ptr_->cam;
}

JB::Vec4 JB::Light::getInfo() {
	JB::Vec4 aux = { direction_.x, direction_.y, direction_.z, intensity_ };
	return aux;
}
