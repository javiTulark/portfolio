#include "framebuffer.h"
#include "texture.h"
#include "Engine.h"
#include "glew/include/GL/glew.h"
#include "GLFW/include/GLFW/glfw3.h"
#include <iostream>


struct JB::Framebuffer::FrameData {
	GLuint framebuffer;
	GLuint color;
	GLuint depth;
};

JB::Framebuffer::Framebuffer() {
	Framebuffer::framedata_ = new Framebuffer::FrameData();
}

JB::Framebuffer::~Framebuffer() {
	delete framedata_;
	glDeleteFramebuffers(1, &framedata_->framebuffer);
}

void JB::Framebuffer::Init(const unsigned int width, const unsigned int height) {
	// color attachment texture
	glGenTextures(1, &framedata_->color);
	glBindTexture(GL_TEXTURE_2D, framedata_->color);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// Depth attachment texture
	glGenTextures(1, &framedata_->depth);
	glBindTexture(GL_TEXTURE_2D, framedata_->depth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	//------------------------ FrameBuffer
	glGenFramebuffers(1, &framedata_->framebuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, framedata_->framebuffer);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, framedata_->color, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, framedata_->depth, 0);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer is not complete!" << std::endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

//	printf("\nColorBuffer  id: %d", framedata_->framebuffer);
//	printf("\nColorTexture id: %d", framedata_->color);
//	printf("\nDepthTexture id: %d", framedata_->depth);

}

unsigned int JB::Framebuffer::getID() {
	return framedata_->framebuffer;
}

unsigned int JB::Framebuffer::getColorID() {
	return framedata_->color;
}

unsigned int JB::Framebuffer::getDepthID() {
	return framedata_->depth;
}
