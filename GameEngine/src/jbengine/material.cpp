#include "material.h"
#include "glew/include/GL/glew.h"
#include "GLFW/include/GLFW/glfw3.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

struct JB::Material::Data {
	GLuint vertexshader;
	GLuint fragmentshader;
	GLuint program;

	bool isLoaded;
	GLint isCompiled;

	const GLchar* kVertexShader;
	const GLchar* kFragmentShader;
	

	glm::vec3 localpos;
};

JB::Material::Material() {
	ptr_.alloc();
	ptr_->vertexshader = 0;
	ptr_->fragmentshader = 0;
	ptr_->program = 0;
	ptr_->isLoaded = false;
	ptr_->isCompiled = 0;
	ptr_->kVertexShader = nullptr;
	ptr_->kFragmentShader = nullptr;
	for (int i = 0; i < 10; i++) {
		lightDir_[i] = { 0.0f, 0.0f, 0.0f, 0.0f };
	}
	tex_ = nullptr;
	usingtex_ = false;
	texID_ = -1;
	texID2_ = -1;
}

JB::Material::~Material() {}

void JB::Material::load(const char* vertexshader, const char* fragmentshader) {
	ptr_->kVertexShader = vertexshader;
	ptr_->kFragmentShader = fragmentshader;
	ptr_->isLoaded = true;
}

void JB::Material::compile() {
	GLchar debugLog[512];

	/////////////////////  Vertex ///////////////////// 
	ptr_->vertexshader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(ptr_->vertexshader, 1, &ptr_->kVertexShader, NULL);
	glCompileShader(ptr_->vertexshader);
	glGetShaderiv(ptr_->vertexshader, GL_COMPILE_STATUS, &ptr_->isCompiled);
	if (!ptr_->isCompiled) {
		glGetShaderInfoLog(ptr_->vertexshader, 512, NULL, debugLog);
		printf("mat shader vertex: \n%s \nEndprint\n\n", debugLog);
		glDeleteShader(ptr_->vertexshader);
	}

	/////////////////////  Fragment ///////////////////
	ptr_->fragmentshader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(ptr_->fragmentshader, 1, &ptr_->kFragmentShader, NULL);
	glCompileShader(ptr_->fragmentshader);
	glGetShaderiv(ptr_->fragmentshader, GL_COMPILE_STATUS, &ptr_->isCompiled);
	if (!ptr_->isCompiled) {
		glGetShaderInfoLog(ptr_->fragmentshader, 512, NULL, debugLog);
		printf("mat shader fragment: \n%s \nEndprint\n\n", debugLog);
		glDeleteShader(ptr_->vertexshader);
		glDeleteShader(ptr_->fragmentshader);
	}

	/////////////////////  Program ////////////////////
	ptr_->program = glCreateProgram();
	glAttachShader(ptr_->program, ptr_->vertexshader);
	glAttachShader(ptr_->program, ptr_->fragmentshader);
	glLinkProgram(ptr_->program);

	glGetProgramiv(ptr_->program, GL_LINK_STATUS, &(ptr_->isCompiled));
	if (!ptr_->isCompiled) {
		glGetProgramInfoLog(ptr_->program, 512, NULL, debugLog);
		printf("mat link program: \n%s \nEndprint\n\n", debugLog);
		glDeleteShader(ptr_->vertexshader);
		glDeleteShader(ptr_->fragmentshader);
	}
}

bool JB::Material::ready() {
	if (ptr_->isCompiled == 1 && ptr_->isLoaded == true)
		return true;
	else
		return false;
}

unsigned int JB::Material::getProgram() {
	return ptr_->program;
}

JB::Vec3 JB::Material::getPosition() {
	Vec3 aux = { ptr_->localpos.x, ptr_->localpos.y, ptr_->localpos.z};
	return aux;
}

void JB::Material::use(Camera cam) {
	if (ready()) {
		glUseProgram(ptr_->program);

		glm::mat4 model;
		model = model * transform_;
		ptr_->localpos = glm::vec3(model[3]);

		GLuint idColor;
		GLuint idShadow1;
		GLuint idShadow2;

		if (usingtex_) {
			idColor = tex_->getTexID();
		}
		else
			idColor = -1;
		if (texID_ != -1)idColor = texID_;

		if (texID2_ != -1)idShadow1 = texID2_;
		else idShadow1 = -1;

		if (texID3_ != -1)idShadow2 = texID3_;
		else idShadow2 = -1;

		cam.update(ptr_->program, model, idColor, idShadow1, idShadow2, lightDir_);
	}
	else
		printf("\n mat_ failed to load or compile");
}