#include "gameobject.h"

#include "glew/include/GL/glew.h"
#include "GLFW/include/GLFW/glfw3.h"
#include <stdio.h>


//////////////// MAIN ACTIVITY ///////////////
JB::GameObject::GameObject() :
	loadgeo_(false),
	loadmat_(false),
	usingtex_(false),
	geo_(nullptr),
	mat_(nullptr),
	tex_(nullptr),
	program_(0) {

};

JB::GameObject::~GameObject() {
};

void JB::GameObject::setGeometry(Geometry* geo) {
	geo_ = geo;
}

void JB::GameObject::setMaterial(Material* mat) {
	mat_ = mat;
}

void JB::GameObject::setTexture(Texture* tex) {
	tex_ = tex;
	mat_->tex_ = tex;
}

void JB::GameObject::LoadGeometry(JB::Shape shape) {
	geo_->load(shape);
}

void JB::GameObject::LoadMaterial(const char* vertexshader, const char* fragmentshader) {
	mat_->load(vertexshader, fragmentshader);
}

void JB::GameObject::setTextureID(unsigned int id) {
	mat_->texID_ = id;
}

void JB::GameObject::setTextureShadows(unsigned int id, unsigned int id2) {
	mat_->texID2_ = id;
	mat_->texID3_ = id2;
}

void JB::GameObject::compile() {
	if (tex_ != nullptr) {
		tex_->compile();
		usingtex_ = true;
		mat_->usingtex_ = true;
	}
	mat_->compile();
	geo_->compile();
}

JB::Vec3 JB::GameObject::getPosition() {
	return mat_->getPosition();
}

void JB::GameObject::setLightSource(
	JB::Vec3 dir,
	float intensity,
	unsigned int id,
	unsigned int depthID,
	glm::mat4 lightspace) {

	JB::Vec3 self_pos = mat_->getPosition();
	if (intensity < 0)intensity = 0.0f;
	if (intensity > 1)intensity = 1.0f;
	if (id >= 0 && id < 10) {
		mat_->lightDir_[id] = { dir.x, dir.y, dir.z, intensity };
		mat_->texID2_ = depthID;
		mat_->lightSpace_ = lightspace;
	}
	else
		printf("\nError setting a light source");
}

void JB::GameObject::execute(Camera cam) {
	mat_->use(cam);
	mat_->transform_ = transform_;

	geo_->draw();
}
