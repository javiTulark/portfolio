#include "texture.h"
#include "glew/include/GL/glew.h"
#include "GLFW/include/GLFW/glfw3.h"
#include <glm/glm.hpp>
#include <SOIL.h>


struct JB::Texture::Data {
	GLuint tex;
	jb_int32 width;
	jb_int32 height;
	unsigned char* image;
};

JB::Texture::Texture(){
	ptr_.alloc();
	ptr_->tex = 0;
	ptr_->width = 128;
	ptr_->height = 128;
}

JB::Texture::~Texture(){
	//glDeleteTextures(1, &ptr_->tex);
}

void JB::Texture::load(const char* str) {
	ptr_->image = SOIL_load_image(str, &ptr_->width, &ptr_->height, 0, SOIL_LOAD_RGBA);
}

void JB::Texture::compile(){
	glGenTextures(1, &ptr_->tex);
	glBindTexture(GL_TEXTURE_2D, ptr_->tex);

	//Tex parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//Tex filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//Generate Mitmaps
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ptr_->width, ptr_->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, ptr_->image);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(ptr_->image);
	glBindTexture(GL_TEXTURE_2D, 0);
}

JB::jb_uint32 JB::Texture::getTexID() {
	return ptr_->tex;
}

