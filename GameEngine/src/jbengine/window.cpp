#include "window.h"
#include "gameobject.h"
#include "material.h"
#include "geometry.h"
#include "node.h"
#include "enums.h"
#include "shaders.h"
#include "camera.h"								
#include "framebuffer.h"


#include "glew/include/GL/glew.h"
#include "GLFW/include/GLFW/glfw3.h"
#include <iostream>
#include <fstream>
#include "../deps/lua/src/lua.hpp"
#include <SOIL.h>


	JB::jb_float triangles[] = {
		-0.0f, 0.5f, 0.0,
		0.5f, -0.5f, 0.0,
		-0.5f, -0.5f, 0.0
	};
	int time_ = 0;


	struct JB::Window::WindowData {
		GLFWwindow* window_;
		jb_int16 width_, height_;
		Node* N;
		Camera Cam;
		Framebuffer fbo;
	};


	JB::Window::Window() : 
	R_(0.1f), G_(0.4f),	B_(0.1f),	A_(1.0f),
	R2_(0.1f), G2_(0.1f),	B2_(0.1f), A2_(1.0f),
	FB_used_(false)
	{
		Window::WindowData_ = new Window::WindowData();
	}


	JB::Window::~Window()	{
		delete WindowData_;
		glfwTerminate();
	}

	bool JB::Window::IsClosed() {
		if (glfwWindowShouldClose(WindowData_->window_)) 
			return true;
		else 
			return false;
	}

	void JB::Window::ClearColor(jb_float r, jb_float g, jb_float b, jb_float a) {
		R_ = r;
		G_ = g;
		B_ = b;
		A_ = a;
	}

	void JB::Window::ClearRenderColor(jb_float r, jb_float g, jb_float b, jb_float a) {
		R2_ = r;
		G2_ = g;
		B2_ = b;
		A2_ = a;
	}
			
	void JB::Window::Swap() {
		glfwPollEvents();
		glfwSwapBuffers(WindowData_->window_);
	}

  int JB::Window::Init() {
		if (!glfwInit()) 
			return 1;

		lua_State *L = luaL_newstate();
		luaL_openlibs(L);
		if (luaL_dofile(L, "config.lua")) {
			std::ofstream configfile("config.lua");
			if (configfile.is_open()) {
				configfile << "width=1440" << std::endl;
				configfile << "height=900" << std::endl;
			//	WindowData_->width_ = 1440;
			//	WindowData_->height_ = 900;
				configfile.close();
			}
		}
		else {
			lua_getglobal(L, "width");
			WindowData_->width_ = lua_tonumber(L, -1);
			lua_pop(L, 1);
			lua_getglobal(L, "height");
			WindowData_->height_ = lua_tonumber(L, -1);
			lua_pop(L, 1);
		}
		lua_close(L);

		WindowData_->window_ = glfwCreateWindow(
				WindowData_->width_, WindowData_->height_, "JB 3.34", NULL, NULL);
		
		//------------------------ Cool icon
		unsigned char* image;
		jb_int32 width;
		jb_int32 height;
		image = SOIL_load_image("../../data/images/icon.png", &width, &height, 0, SOIL_LOAD_RGBA);
		GLFWimage img;
		img.height = height;
		img.width = width;
		img.pixels = image;

		glfwSetWindowIcon(WindowData_->window_, 1, &img);
		
		//------------------------------
		if (!WindowData_->window_) {
			glfwTerminate();
			return -1;
		}
		glfwMakeContextCurrent(WindowData_->window_);
		glewInit();
		glViewport(0, 0, WindowData_->width_, WindowData_->height_);
		glDepthFunc(GL_LESS);
		glEnable(GL_DEPTH_TEST);
		return 0;
	}

	void JB::Window::InitRender(const int id) {
		glBindFramebuffer(GL_FRAMEBUFFER, id);
		glClearColor(R2_, G2_, B2_, A2_);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void JB::Window::EndRender() {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void JB::Window::Clear() {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClearColor(R_, G_, B_, A_);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		if(FB_used_)WindowData_->N->execute(WindowData_->Cam);
	}

	void JB::Window::initFrameBuffer() {
		WindowData_->fbo.Init(WindowData_->width_, WindowData_->height_);
		Engine::Instance()->fbo = WindowData_->fbo.getID();

		//------------------------ Default Quad for FrameBuffer
		WindowData_->N = WindowData_->N->createRoot();
		WindowData_->N->setComponent(JB::kComponent_GameObject);
		WindowData_->N->setRenderSize(WindowData_->width_, WindowData_->height_);
		WindowData_->N->LoadGeometry(JB::kShape_RenderQuad);

		//Color
		WindowData_->N->setTextureID(WindowData_->fbo.getColorID());
		//Depth
		//WindowData_->N->setTextureID(Engine::Instance()->depthID[0]);
		//WindowData_->N->setTextureID(Engine::Instance()->depthID[1]);
		//SetMaterial
		WindowData_->N->LoadMaterial(JB::BasicVertex, JB::BasicFragment);

		WindowData_->N->compileGameObject();
		WindowData_->Cam.setPosition(JB::Vec3{ 0.0f, 0.0f, 20.0f });
		WindowData_->Cam.setTarget(JB::Vec3{ 0.0f,0.0f,0.0f });
		WindowData_->Cam.setWindowSize(1440.0f, 900.0f);

		FB_used_ = true;
		//-------------------------
	}