#include "gamestate.h"

namespace JB {

	GameState::GameState() {
		


		for (int i = 0; i < 10; i++)
		{
			Tex_Sun[i].load("../../data/images/sun.jpg");
			Tex_Earth[i].load("../../data/images/earth.png");
			Tex_Moon[i].load("../../data/images/moon.jpg");
		}
		
		
		startUpdate = 0;
		endUpdate = 0;
		startDraw = 0;
		endDraw = 0;
		root_.createRoot();

	}

	void GameState::StartChrono(const char *name, int frame, float time) {
		char num[256];
		itoa(frame, num, 10);
		jsonstart["name"] = num;
		jsonstart["ph"] = "B";
		jsonstart["pid"] = 1;
		jsonstart["tid"] = name;
		jsonstart["ts"] = time;
		data.push_back(jsonstart);
	}

	void GameState::EndChrono(const char *name, int frame, float time) {
		char num[256];
		itoa(frame, num, 10);
		jsonend["name"] = num;
		jsonend["ph"] = "E";
		jsonend["pid"] = 1;
		jsonend["tid"] = name;
		jsonend["ts"] = time;
		data.push_back(jsonend);
	}

	
}