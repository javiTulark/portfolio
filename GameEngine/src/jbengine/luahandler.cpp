#include "luahandler.h"
#include <iostream>

namespace JB {

	//class Gamestate;
	GameState gs_;
	Texture *tx_;
	int sun_cont = 0;
	int earth_cont = 0;
	int moon_cont = 0;
	

	int getRoot(lua_State *L)
	{
		lua_pushinteger(L,gs_.root_.id_);
		
		
		//para rescatar el GameState de Lua usaremos
					//LuaHandler *lh = GetInstance(L);
		
		return 1;
	}

	 /*
	int LuaHandler::lua_getRoot(lua_State *L)
	{

		//Esto debe de ir en GetInstance, ya que lo vamos a invocar cada
		//vez que queramos sacar el GameState
		lua_getfield(L, LUA_REGISTRYINDEX, "instance");
		void *ptr = lua_touserdata(L, -1);
		lua_pop(L, 1);
	
		//return reinterpret_cast<LuaHandler*>(ptr)->getRoot(L);
		return 0;
	}

	 */
	int addNode(lua_State *L)
	{
		/*
		
		*/
		//saco de la pila de lua los datos
		int id_father = lua_tonumber(L, 1);
		float  posX = lua_tonumber(L, 2);
		float posY = lua_tonumber(L, 3);
		float posZ = lua_tonumber(L, 4);
		const char* geometry = lua_tostring(L, 5);
		if (geometry == nullptr) geometry = { "0" };
		bool is_light = lua_toboolean(L, 6);
		const char* Texture = lua_tostring(L, 7);
		if (Texture == nullptr) Texture = { "Sun" };
		
		//creo el nodo con los datos pasados
		JB::Node* n = gs_.nodes_[id_father]->addChild();
		Vec3 scale = { 1.0f,1.0f,1.0f };
		n->setScale(scale);
		Vec3 pos = { posX,posY,posZ };
		n->setPosition(pos);
		
		if (!is_light) {
		n->setComponent(JB::kComponent_GameObject);
		/*
		*/
			if (strcmp(geometry, "Triangle") == 0) n->LoadGeometry(kShape_Triangle);
			else if (strcmp(geometry, "Quad") == 0) n->LoadGeometry(kShape_Quad);
			else if (strcmp(geometry, "Cube") == 0) n->LoadGeometry(kShape_Cube);
			else if (strcmp(geometry, "Sphere") == 0) n->LoadGeometry(kShape_Sphere);
			else if (strcmp(geometry, "Donut") == 0)n->LoadGeometry(kShape_Donut);
			//else 	n->LoadGeometry(kShape_Cube);

			n->LoadMaterial(JB::vertexTex, JB::fragmentTex);

			if (strcmp(Texture, "Sun") == 0) {
				n->setTexture(&gs_.Tex_Sun[sun_cont]);
				sun_cont++;
			}
			else if (strcmp(Texture, "Earth") == 0) { 
				n->setTexture(&gs_.Tex_Earth[earth_cont]);
				earth_cont++;
			}
			else if (strcmp(Texture, "Moon") == 0) {
				//n->setTexture(gs_.texture_.front());
				n->setTexture(&gs_.Tex_Moon[moon_cont]);
				moon_cont++;
			}
			//else n->setTexture(&gs_.Tex_Sun);


			n->compileGameObject();
		}
		else {
			n->setComponent(JB::kComponent_Light);
			n->is_light_ = true;
		}


		gs_.nodes_.push_back(n);
		n->id_ = gs_.nodes_.size() - 1;
		lua_pushinteger(L, n->id_);


		return 1;
	}
	GameState* LuaHandler::init()
	{
	
		L = luaL_newstate();
		luaL_openlibs(L);
		gs_.nodes_.push_back(&gs_.root_);
			
		lua_register(L, "getRoot", getRoot);
		lua_register(L, "addNode", addNode);
		 //Meter el GameState a traves de Reg
	 
		return &gs_;
	
	}

	void LuaHandler::execute_file(char* file_lua)
	{
		//luaL_dofile(L, "../../../data/file.lua");
		
		if (luaL_dofile(L, file_lua)) {
			std::cout << "ERROR IN LUA " << lua_tostring(L, -1) << std::endl;
			lua_pop(L, 1);
		}
	}

	
}
