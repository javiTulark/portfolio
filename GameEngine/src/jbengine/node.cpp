#include "node.h"
#include "components.h"
#include <iostream>
#include "GLFW/include/GLFW/glfw3.h"

namespace JB {

	Node::Node() :
		id_(0),
		position_(JB::Vec3{ 0.0f,0.0f,0.0f }),
		rotation_(JB::Vec3{ 0.0f,0.0f,0.0f }),
		scale_(glm::vec3{ 1.0f,1.0f,1.0f }),
		light_intensity_(1.0f),
		axisX_(false),
		axisY_(false),																																
		axisZ_(false),
		is_light_(false){

	}

	Node::~Node() {
	}

	Node* Node::addChild() {
		//Should attach father
		std::unique_ptr<Node> n(new Node());
		n->father_ = this;
		Node* p = n.get();
		children_.push_back(std::move(n));
		return p;
	}

	Node* Node::createRoot() {
		Node* n = new Node();
		n->father_ = nullptr;
		return n;
	}

	void Node::removeChild(Node **n) {
		for (jb_uint32 i = 0; i < children_.size(); i++) {
			if (children_[i].get() == *n) {
				//NO hace falta recorrer todos los hijos
				children_.erase(children_.begin() + i);
				printf("Delete: %u\n", *n);
				*n = nullptr;
			}
		}
	}

	bool Node::setComponent(Components component) {
	
		switch (component) {
			case kComponent_GameObject:
				SetGameObject();
				
				return true;
				break;
			case kComponent_Light:
				SetLight();
				return true;
				break;
			case kComponent_sineTransform:
				SetOther();
				break;
			default:
				return false;
				break;
		}
	}

	void Node::removeComponent(Components component) {

	}

	Vec3 Node::getPosition() {
		glm::vec3 v = glm::vec3(transform_[3]);
		return JB::Vec3{ v.x, v.y, v.z };
	}

	Vec3 Node::getScale() {
		glm::vec3 v = glm::vec3(transform_[0][0], transform_[1][1], transform_[2][2]);
		return JB::Vec3{ v.x, v.y, v.z };
	}

	int Node::getID() {
		return light_id_;
	}

	float Node::getIntensity() {
		return light_intensity_;
	}

	void JB::Node::LoadGeometry(JB::Shape shape) {
			game_object_.LoadGeometry(shape);
	}

	void JB::Node::LoadMaterial(const char* vertexshader, const char* fragmentshader) {
			game_object_.LoadMaterial(vertexshader, fragmentshader);
	}

	void JB::Node::ApplyLight(Node* light) {
		game_object_.setLightSource(
			light->getDirection(),
			light->getIntensity(),
			light->getID(),
			light->getDepthID(),
			light->getlightSpaceMatrix());
	}

	JB::Vec3 JB::Node::getDirection() {
		return Vec3{ light_.direction_.x, light_.direction_.y, light_.direction_.z };
	}

	glm::mat4 Node::getViewMatrix() {
		return light_.getViewMatrix();
	}

	glm::mat4 Node::getlightSpaceMatrix() {
		return light_.getlightSpaceMatrix();
	}

	void JB::Node::setDirection(Vec3 dir) {
		light_.direction_ = glm::vec3{ dir.x, dir.y, dir.z };
	}

	JB::Camera JB::Node::getCam() {
		return light_.getCam();
	}

	unsigned int Node::getDepthID() {
		return light_.getDepth();
	}

	void JB::Node::compileGameObject() {
		game_object_.compile();
	}

	void JB::Node::setTexture(Texture* tex) {
		game_object_.setTexture(tex);
	}

	void JB::Node::setRenderSize(jb_float x, jb_float y) {
		geo_.setRenderSize(x, y);
	}

	void JB::Node::setTextureID(unsigned int id) {
		game_object_.setTextureID(id);
	}	
	
	void JB::Node::setTextureShadows(unsigned int id, unsigned int id2) {
		game_object_.setTextureShadows(id, id2);
	}

	void Node::setPosition(Vec3 v) {
		position_.x = v.x; position_.y = v.y; position_.z = v.z;
	}

	void Node::setRotation(Vec3 v) {
		rotation_.x = v.x;
		rotation_.y = v.y;
		rotation_.z = v.z;
	}

	void Node::setScale(Vec3 v) {
		scale_.x = v.x; scale_.y = v.y; scale_.z = v.z;
	}

	void Node::setAxisMovement(bool axisX, bool axisY, bool axisZ, float amplitude, float speed) {
		axisX_ = axisX;
		axisY_ = axisY;
		axisZ_ = axisZ;
		amplitude_ = amplitude;
		speed_ = speed;
	}

	void Node::updateAxis() {
		if (axisX_)position_.x = amplitude_*sin(speed_*(float)glfwGetTime());
		if (axisY_)position_.y = amplitude_*sin(speed_*(float)glfwGetTime());
		if (axisZ_)position_.z = amplitude_*sin(speed_*(float)glfwGetTime());
	}

	void Node::SetGameObject()	{
		game_object_.setGeometry(&geo_);
		game_object_.setMaterial(&mat_);
	}

	void Node::SetLight()	{
		light_id_ = Engine::Instance()->light_id_;
		light_.position_ = position_;
		light_.target_ = JB::Vec3{ 0.0f,0.0f,0.0f };
		printf("\nLight        id: %d", light_id_);
		light_.init(light_id_);
		Engine::Instance()->light_id_++;
	}

	void Node::SetOther()	{
	}

	void Node::executeLight() {
		/// If this node has a parent, get its transform matrix and update its own transform
		if (father_)transform_parent_ = father_->transform_;
		glm::mat4 transform;
		transform *= transform_parent_;

		updateAxis();
		// Rotation, traslation and scale
		transform = glm::rotate(transform, glm::radians(rotation_.x), glm::vec3(1.0f, 0.0f, 0.0f));
		transform = glm::rotate(transform, glm::radians(rotation_.y), glm::vec3(0.0f, 1.0f, 0.0f));
		transform = glm::rotate(transform, glm::radians(rotation_.z), glm::vec3(0.0f, 0.0f, 1.0f));
		transform = glm::translate(transform, glm::vec3(position_.x, position_.y, position_.z));
		transform = glm::scale(transform, scale_);

		//transform_ will be the one to give to my child
		transform_ = transform;
	}

	void Node::execute(Camera cam) {
		/// If this node has a parent, get its transform matrix and update its own transform
		if (father_)transform_parent_ = father_->transform_;
		glm::mat4 transform;
		transform *= transform_parent_;

		updateAxis();
		// Rotation, traslation and scale
		transform = glm::rotate(transform, glm::radians(rotation_.x), glm::vec3(1.0f, 0.0f, 0.0f));
		transform = glm::rotate(transform, glm::radians(rotation_.y), glm::vec3(0.0f, 1.0f, 0.0f));
		transform = glm::rotate(transform, glm::radians(rotation_.z), glm::vec3(0.0f, 0.0f, 1.0f));
		transform = glm::translate(transform, glm::vec3(position_.x, position_.y, position_.z));
		transform = glm::scale(transform, scale_);

		//transform_ will be the one to give to my child
		transform_ = transform;
		game_object_.transform_ = transform;
		game_object_.execute(cam);
	}
}

