#include "camera.h"
#include "engine.h"
#include "glew/include/GL/glew.h"
#include "GLFW/include/GLFW/glfw3.h"
#include <iostream>

JB::Camera::Camera() {

	cameraPosition_.x = 0.0f;
	cameraPosition_.y = 0.0f;
	cameraPosition_.z = 0.0f;
	up_vector_.x = 0.0f;
	up_vector_.y = 1.0f;
	up_vector_.z = 0.0f;
	field_of_view_ = 45.0f;
	near_plane_ = 5.0f;
	far_plane_ = 50.0f;
	target_.x = 0.0f;
	target_.y = 0.0f;
	target_.z = 0.0f;
}

JB::Camera::~Camera() {
}

void JB::Camera::setPosition(Vec3 position) {
	glm::vec3 pos;
	pos.x = position.x;
	pos.y = position.y;
	pos.z = position.z;
	cameraPosition_ = pos;
}

void JB::Camera::setTarget(Vec3 target) {
	target_.x = target.x;
	target_.y = target.y;
	target_.z = target.z;
}

void JB::Camera::setNearFar(jb_float nearPlane, jb_float farPlane) {
	near_plane_ = nearPlane;
	far_plane_ = farPlane;
}

void JB::Camera::setWindowSize(jb_float width, jb_float length) {
	aspect_ratio_ = width / length;
}

void JB::Camera::update(int program,
	glm::mat4 model,
	unsigned int texture_id,
	unsigned int texture_id2,
	unsigned int texture_id3,
	JB::Vec4* ldir) {

	cameraDirection_ = glm::normalize(cameraPosition_ - target_);
	cameraRight_ = glm::normalize(glm::cross(up_vector_, cameraDirection_));
	cameraUp_ = glm::cross(cameraDirection_, cameraRight_);
	view_ = glm::lookAt(cameraPosition_, cameraDirection_, cameraUp_);
	projection_ = glm::perspective(field_of_view_, aspect_ratio_, near_plane_, far_plane_);
	lightSpaceMatrix_ = projection_*view_;

	//Color
	if (texture_id != -1) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture_id);
		int location = glGetUniformLocation(program, "texture_1");
		glUniform1i(location, 0);
	}

	//Shadow1
	if (texture_id2 != -1) {
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture_id2);
		int location = glGetUniformLocation(program, "texture_2");
		glUniform1i(location, 1);
	}

	//Shadow2
	if (texture_id3 != -1) {
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, texture_id3);
		int location = glGetUniformLocation(program, "texture_3");
		glUniform1i(location, 2);
	}

	glm::mat4 lightspace = Engine::Instance()->lightTransform[0];
	glm::mat4 lightspace2 = Engine::Instance()->lightTransform[1];

	GLint modelLoc = glGetUniformLocation(program, "model");
	GLint viewLoc = glGetUniformLocation(program, "view");
	GLint projLoc = glGetUniformLocation(program, "projection");
	GLint lightLoc = glGetUniformLocation(program, "lightpos");
	GLint CamLoc = glGetUniformLocation(program, "campos");
	GLint shadowDir = glGetUniformLocation(program, "shadowView");
	GLint shadowDir2 = glGetUniformLocation(program, "shadowView2");

	glm::vec4 lightpos[10];
	for (int i = 0; i < 10; i++) {
		lightpos[i] = { ldir[i].x, ldir[i].y, ldir[i].z, ldir[i].w };
	}

	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view_));
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection_));
	glUniform4fv(lightLoc, 10, (const GLfloat*)lightpos);
	glUniform3fv(CamLoc, 1, glm::value_ptr(cameraPosition_));
	glUniformMatrix4fv(shadowDir, 1, GL_FALSE, glm::value_ptr(lightspace));
	glUniformMatrix4fv(shadowDir2, 1, GL_FALSE, glm::value_ptr(lightspace2));
}

glm::mat4 JB::Camera::getViewMatrix() {
	return (view_);
}

JB::jb_float JB::Camera::getFoV() {
	return field_of_view_;
}

JB::jb_float JB::Camera::getAspectRatio() {
	return aspect_ratio_;
}

glm::mat4 JB::Camera::getlightSpaceMatrix() {
	return lightSpaceMatrix_;
}
