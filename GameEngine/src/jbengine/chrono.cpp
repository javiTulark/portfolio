#include "chrono.h"
#include "glew/include/GL/glew.h"
#include "GLFW/include/GLFW/glfw3.h"
#include <iostream>

JB::Chrono::Chrono(){
}

JB::Chrono::~Chrono() {
}

void JB::Chrono::Begin() {
	init_ = glfwGetTime();
}

void JB::Chrono::End() {
	end_ = glfwGetTime() - init_;
}

void JB::Chrono::Show(const char* str) {
	std::cout << str << " " << end_ << " Seconds " << std::endl;
	//printf("\n%2.4lf", end_*0.001f);
}
