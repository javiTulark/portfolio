#include "geometry.h"
#include <stdlib.h>
#include "glew/include/GL/glew.h"
#include "GLFW/include/GLFW/glfw3.h"
#include <iostream>

struct JB::Geometry::Data {
	scoped_ptr<JB::Geometry::GeoData> geo;
	GLuint VBO, VAO, EBO;
	jb_float scale_;
	bool isLoaded;
	bool isCompiled;
	jb_float screen_size_x;
	jb_float screen_size_y;
};

JB::Geometry::Geometry() {
	ptr_.alloc();
	ptr_->geo.alloc();
	ptr_->VBO = 0;
	ptr_->VAO = 0;
	ptr_->EBO = 0;
	ptr_->scale_ = 1.0f;
	ptr_->isCompiled = false;
	stride_ = 2 * sizeof(Vec3) + sizeof(Vec2);
	ptr_->screen_size_x = 1440;
	ptr_->screen_size_y = 900;
}
JB::Geometry::~Geometry() {}

JB::Vec3 JB::Geometry::vec3Add(Vec3 A, Vec3 B) {
	Vec3 aux = { A.x + B.x, A.y + B.y, A.z + B.z };
	return aux;
}

void JB::Geometry::setPosition(Vec3 pos) {
	for (unsigned int i = 0; i < (ptr_->geo->vertex.size()); i++) {
		ptr_->geo->vertex[i].position = vec3Add(ptr_->geo->vertex[i].position, pos);
	}
}

JB::Vec3 JB::Geometry::Normalize(Vec3 A, Vec3 B) {
	Vec3 aux;
	jb_float module;
	aux.x = A.x - B.x;
	aux.y = A.y - B.y;
	aux.z = A.z - B.z;
	module = sqrt((aux.x * aux.x) + (aux.y * aux.y) + (aux.z * aux.z));
	aux.x /= module;
	aux.y /= module;
	aux.z /= module;
	return aux;
}

void JB::Geometry::load(Shape shape) {
	switch (shape) {
		case kShape_None:
			ptr_->isLoaded = false;
			break;
		case kShape_Triangle: {
				float size = ptr_->scale_;
				ptr_->geo->vertex.alloc(3);
				ptr_->geo->index.alloc(3);
				//Left
				ptr_->geo->vertex[0].position = { -1.0f, -1.0f, 0.0f };
				ptr_->geo->vertex[0].normal = { 0.0f, 0.f, 1.f };
				ptr_->geo->vertex[0].uv = { 0.0f, 0.0f };
				//Right
				ptr_->geo->vertex[1].position = { 1.0f, -1.0f, 0.0f };
				ptr_->geo->vertex[1].normal = { 0.0f, 0.f, 1.f };
				ptr_->geo->vertex[1].uv = { 1.0f, 0.0f };
				//Top
				ptr_->geo->vertex[2].position = { 0.0f, 1.0f, 0.0f };
				ptr_->geo->vertex[2].normal = { 0.0f, 0.f, 1.f };
				ptr_->geo->vertex[2].uv = { 0.5f, 1.0f };
				//index
				for (jb_uint8 i = 0; i < 3; i++) {
					ptr_->geo->index[i] = i;
				}

				ptr_->isLoaded = true;
			} break;

		case kShape_Quad: {
				float size = ptr_->scale_;
				ptr_->geo->vertex.alloc(4);
				ptr_->geo->index.alloc(6);

				//Left top
				ptr_->geo->vertex[0].position = { -size, size, 0.0f };
				ptr_->geo->vertex[0].normal = { 0.f, 0.f, 1.f };
				ptr_->geo->vertex[0].uv = { 0.0f, 1.0f };
				//bot left
				ptr_->geo->vertex[1].position = { -size, -size, 0.0f };
				ptr_->geo->vertex[1].normal = { 0.f, 0.f, 1.f };
				ptr_->geo->vertex[1].uv = { 0.0f, 0.0f };
				//bot right
				ptr_->geo->vertex[2].position = { size, -size, 0.0f };
				ptr_->geo->vertex[2].normal = { 0.f, 0.f, 1.f };
				ptr_->geo->vertex[2].uv = { 1.0f, 0.0f };
				//Top rigth
				ptr_->geo->vertex[3].position = { size, size, 0.0f };
				ptr_->geo->vertex[3].normal = { 0.f, 0.f, 1.f };
				ptr_->geo->vertex[3].uv = { 1.0f, 1.0f };

				//index
				ptr_->geo->index[0] = 0;
				ptr_->geo->index[1] = 1;
				ptr_->geo->index[2] = 3;
				ptr_->geo->index[3] = 1;
				ptr_->geo->index[4] = 2;
				ptr_->geo->index[5] = 3;

				ptr_->isLoaded = true;
			} break;

		case kShape_Cube: {
				float size = ptr_->scale_;
				ptr_->geo->vertex.alloc(24);
				ptr_->geo->index.alloc(36);
				//Up
				ptr_->geo->vertex[0].position = { -size, size, -size };
				ptr_->geo->vertex[1].position = { size, size, -size };
				ptr_->geo->vertex[2].position = { size, size, size };
				ptr_->geo->vertex[3].position = { -size, size, size };
				//Right
				ptr_->geo->vertex[4].position = { size, size, -size };
				ptr_->geo->vertex[5].position = { size, -size, -size };
				ptr_->geo->vertex[6].position = { size, -size, size };
				ptr_->geo->vertex[7].position = { size, size, size };
				//Front
				ptr_->geo->vertex[8].position = { size, size, size };
				ptr_->geo->vertex[9].position = { size, -size, size };
				ptr_->geo->vertex[10].position = { -size, -size, size };
				ptr_->geo->vertex[11].position = { -size, size, size };
				//Left
				ptr_->geo->vertex[12].position = { -size, size, -size };
				ptr_->geo->vertex[13].position = { -size, -size, -size };
				ptr_->geo->vertex[14].position = { -size, -size, size };
				ptr_->geo->vertex[15].position = { -size, size, size };
				//Bottom
				ptr_->geo->vertex[16].position = { -size, -size, -size };
				ptr_->geo->vertex[17].position = { -size, -size, size };
				ptr_->geo->vertex[18].position = { size, -size, size };
				ptr_->geo->vertex[19].position = { size, -size, -size };
				//Back
				ptr_->geo->vertex[20].position = { -size, size, -size };
				ptr_->geo->vertex[21].position = { size, size, -size };
				ptr_->geo->vertex[22].position = { size, -size, -size };
				ptr_->geo->vertex[23].position = { -size, -size, -size };

				//Normales
				//Up
				ptr_->geo->vertex[0].normal = { 0, 1, 0 };
				ptr_->geo->vertex[1].normal = { 0, 1, 0 };
				ptr_->geo->vertex[2].normal = { 0, 1, 0 };
				ptr_->geo->vertex[3].normal = { 0, 1, 0 };
				//Right
				ptr_->geo->vertex[4].normal = { 1, 0, 0 };
				ptr_->geo->vertex[5].normal = { 1, 0, 0 };
				ptr_->geo->vertex[6].normal = { 1, 0, 0 };
				ptr_->geo->vertex[7].normal = { 1, 0, 0 };
				//Front
				ptr_->geo->vertex[8].normal = { 0, 0, 1 };
				ptr_->geo->vertex[9].normal = { 0, 0, 1 };
				ptr_->geo->vertex[10].normal = { 0, 0, 1 };
				ptr_->geo->vertex[11].normal = { 0, 0, 1 };
				//Left
				ptr_->geo->vertex[12].normal = { -1, 0, 0 };
				ptr_->geo->vertex[13].normal = { -1, 0, 0 };
				ptr_->geo->vertex[14].normal = { -1, 0, 0 };
				ptr_->geo->vertex[15].normal = { -1, 0, 0 };
				//Bottom
				ptr_->geo->vertex[16].normal = { 0, -1, 0 };
				ptr_->geo->vertex[17].normal = { 0, -1, 0 };
				ptr_->geo->vertex[18].normal = { 0, -1, 0 };
				ptr_->geo->vertex[19].normal = { 0, -1, 0 };
				//Back
				ptr_->geo->vertex[20].normal = { 0, 0, -1 };
				ptr_->geo->vertex[21].normal = { 0, 0, -1 };
				ptr_->geo->vertex[22].normal = { 0, 0, -1 };
				ptr_->geo->vertex[23].normal = { 0, 0, -1 };

				//uv
				// Up
				ptr_->geo->vertex[0].uv = { 0, 1 };
				ptr_->geo->vertex[1].uv = { 1, 1 };
				ptr_->geo->vertex[2].uv = { 1, 0 };
				ptr_->geo->vertex[3].uv = { 0, 0 };
				//Right
				ptr_->geo->vertex[4].uv = { 1, 1 };
				ptr_->geo->vertex[5].uv = { 1, 0 };
				ptr_->geo->vertex[6].uv = { 0, 0 };
				ptr_->geo->vertex[7].uv = { 0, 1 };
				//Front
				ptr_->geo->vertex[8].uv = { 1, 1 };
				ptr_->geo->vertex[9].uv = { 1, 0 };
				ptr_->geo->vertex[10].uv = { 0, 0 };
				ptr_->geo->vertex[11].uv = { 0, 1 };
				//Left
				ptr_->geo->vertex[12].uv = { 0, 1 };
				ptr_->geo->vertex[13].uv = { 0, 0 };
				ptr_->geo->vertex[14].uv = { 1, 0 };
				ptr_->geo->vertex[15].uv = { 1, 1 };
				//Bottom
				ptr_->geo->vertex[16].uv = { 0, 0 };
				ptr_->geo->vertex[17].uv = { 0, 1 };
				ptr_->geo->vertex[18].uv = { 1, 1 };
				ptr_->geo->vertex[19].uv = { 1, 0 };
				//Back
				ptr_->geo->vertex[20].uv = { 1, 1 };
				ptr_->geo->vertex[21].uv = { 0, 1 };
				ptr_->geo->vertex[22].uv = { 0, 0 };
				ptr_->geo->vertex[23].uv = { 1, 0 };

				//index
				//Up
				ptr_->geo->index[0] = 0;
				ptr_->geo->index[1] = 3;
				ptr_->geo->index[2] = 2;
				ptr_->geo->index[3] = 2;
				ptr_->geo->index[4] = 1;
				ptr_->geo->index[5] = 0;
				//Right
				ptr_->geo->index[6] = 4;
				ptr_->geo->index[7] = 7;
				ptr_->geo->index[8] = 6;
				ptr_->geo->index[9] = 6;
				ptr_->geo->index[10] = 5;
				ptr_->geo->index[11] = 4;
				//Front
				ptr_->geo->index[12] = 8;
				ptr_->geo->index[13] = 11;
				ptr_->geo->index[14] = 9;
				ptr_->geo->index[15] = 11;
				ptr_->geo->index[16] = 10;
				ptr_->geo->index[17] = 9;
				//Left
				ptr_->geo->index[18] = 12;
				ptr_->geo->index[19] = 13;
				ptr_->geo->index[20] = 14;
				ptr_->geo->index[21] = 15;
				ptr_->geo->index[22] = 12;
				ptr_->geo->index[23] = 14;
				//Back
				ptr_->geo->index[24] = 16;
				ptr_->geo->index[25] = 19;
				ptr_->geo->index[26] = 18;
				ptr_->geo->index[27] = 18;
				ptr_->geo->index[28] = 17;
				ptr_->geo->index[29] = 16;
				//Bottom
				ptr_->geo->index[30] = 20;
				ptr_->geo->index[31] = 21;
				ptr_->geo->index[32] = 22;
				ptr_->geo->index[33] = 22;
				ptr_->geo->index[34] = 23;
				ptr_->geo->index[35] = 20;

				ptr_->isLoaded = true;
			}	break;

		case kShape_Sphere: {
				const int paralels = 25;
				const int meridians = 25;
				const int paradouble = paralels * 2;
				const float pi = 3.1415926535f;
				float pi2 = pi / 2.0f;
				float angle = pi / (paralels - 1);

				float V[paradouble];
				for (int i = 0; i < paralels; i++) {
					V[i * 2] = cos(pi2 + (i * angle));
					V[i * 2 + 1] = sin(pi2 + (i * angle));
				}

				float alpha = (2.0f * pi) / (paralels - 1);
				int number_of_elements = (paralels - 1) * meridians * 6;
				ptr_->geo->vertex.alloc(paralels * meridians);
				ptr_->geo->index.alloc(number_of_elements);

				int aux;
				for (int i = 0; i < meridians; i++) {
					//	std::cout << alpha * (float)i / 0.01745329252f << std::endl;
					for (int j = 0; j < paralels; j++) {
						aux = (i * paralels) + j;
						// Vertex
						ptr_->geo->vertex[aux].position = {
							V[2 * j] * cos(alpha * i),
							V[(2 * j) + 1],
							V[2 * j] * sin(alpha * i)
						};
						//Normal
						Vec3 zero = { 0.0f, 0.0f, 0.0f };
						ptr_->geo->vertex[aux].normal = Normalize(zero, ptr_->geo->vertex[aux].position);
						//UV
						float value1 = 0.5f + (atan2(ptr_->geo->vertex[aux].normal.x, ptr_->geo->vertex[aux].normal.z) / (2.0f * pi));
						float value2 = 0.5f - (asin(ptr_->geo->vertex[aux].normal.y) / pi);

						ptr_->geo->vertex[aux].uv = { value1,	-value2 };
						//std::cout << value1 << std::endl;
					}
				}

				unsigned int cara = 0;
				int totalpoints = paralels * meridians;

				for (int i = 0; i < (meridians); i++) {
					for (int j = 0; j < (paralels - 1); j++) {
						int n = i * paralels + j;
						ptr_->geo->index[cara + 0] = n;
						ptr_->geo->index[cara + 1] = (paralels + n) % totalpoints;
						ptr_->geo->index[cara + 2] = (paralels + n + 1) % totalpoints;
						ptr_->geo->index[cara + 3] = n;
						ptr_->geo->index[cara + 4] = (paralels + n + 1) % totalpoints;
						ptr_->geo->index[cara + 5] = (n + 1) % totalpoints;
						cara += 6;
					}
				}
				ptr_->isLoaded = true;
			} break;

		case kShape_Donut: {
				const int paralels = 30;
				int meridians = 30;
				const int paradouble = paralels * 2;
				float pi = 3.141592f;
				float pi2 = pi / 2.0f;
				float angle = pi / (paralels - 1);

				float V[paradouble];
				for (int i = 0; i < paralels; i++) {
					V[i * 2] = 10 + cos(pi2 + i * angle * 2);
					V[i * 2 + 1] = sin(pi2 + i * angle * 2);
				}

				float alpha = 2.0f * angle;
				int number_of_elements = (paralels - 1) * meridians * 6;
				int aux;

				ptr_->geo->vertex.alloc(paralels * meridians);
				ptr_->geo->index.alloc(number_of_elements);

				for (int i = 0; i < meridians; i++) {
					for (int j = 0; j < paralels; j++) {
						aux = i * paralels + j;
						ptr_->geo->vertex[aux].position = {
							V[2 * j] * cos(alpha * i),
							V[(2 * j) + 1],
							V[2 * j] * sin(alpha * i)
						};

						ptr_->geo->vertex[aux].normal = {
							-1 * V[(2 * j) + 1],
							V[2 * j] * cos(alpha * i),
							V[2 * j] * sin(alpha * i)
						};
					}
				}

				unsigned int cara = 0;
				int totalpoints = paralels * meridians;

				for (int i = 0; i < meridians; i++) {
					for (int j = 0; j < (paralels - 1); j++) {
						int n = i * paralels + j;
						ptr_->geo->index[cara] = n;
						ptr_->geo->index[cara + 1] = (paralels + n) % totalpoints;
						ptr_->geo->index[cara + 2] = (paralels + n + 1) % totalpoints;
						ptr_->geo->index[cara + 3] = n;
						ptr_->geo->index[cara + 4] = (paralels + n + 1) % totalpoints;
						ptr_->geo->index[cara + 5] = (n + 1) % totalpoints;
						cara += 6;
					}
				}
				ptr_->isLoaded = true;
			} break;

		case kShape_RenderQuad: {
			float size = ptr_->scale_;
			ptr_->geo->vertex.alloc(4);
			ptr_->geo->index.alloc(6);

			//Left top
			ptr_->geo->vertex[0].position = { -17.7f, 11, 0.0f };
			ptr_->geo->vertex[0].normal = { 0.f, 0.f, 1.f };
			ptr_->geo->vertex[0].uv = { 0.0f, 1.0f };
			//bot left
			ptr_->geo->vertex[1].position = { -17.7f, -11, 0.0f };
			ptr_->geo->vertex[1].normal = { 0.f, 0.f, 1.f };
			ptr_->geo->vertex[1].uv = { 0.0f, 0.0f };
			//bot right
			ptr_->geo->vertex[2].position = { 17.7f, -11, 0.0f };
			ptr_->geo->vertex[2].normal = { 0.f, 0.f, 1.f };
			ptr_->geo->vertex[2].uv = { 1.0f, 0.0f };
			//Top rigth
			ptr_->geo->vertex[3].position = { 17.7f, 11, 0.0f };
			ptr_->geo->vertex[3].normal = { 0.f, 0.f, 1.f };
			ptr_->geo->vertex[3].uv = { 1.0f, 1.0f };
			//index
			ptr_->geo->index[0] = 0;
			ptr_->geo->index[1] = 1;
			ptr_->geo->index[2] = 3;
			ptr_->geo->index[3] = 1;
			ptr_->geo->index[4] = 2;
			ptr_->geo->index[5] = 3;

			ptr_->isLoaded = true;
		} break;


	}
}

void JB::Geometry::setRenderSize(jb_float x, jb_float y) {
	ptr_->screen_size_x = x;
	ptr_->screen_size_y = y;
}

void JB::Geometry::compile() {

	glGenVertexArrays(1, &ptr_->VAO);
	glGenBuffers(1, &ptr_->EBO);
	glGenBuffers(1, &ptr_->VBO);
	glBindVertexArray(ptr_->VAO);

	/////////////////////  Vertex /////////////////////
	glBindBuffer(GL_ARRAY_BUFFER, ptr_->VBO);
	glBufferData(GL_ARRAY_BUFFER,
	             ptr_->geo->vertex.sizeInBytes(),
	             ptr_->geo->vertex.get(),
	             GL_STATIC_DRAW);

	/////////////////////  Index /////////////////////
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ptr_->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
	             ptr_->geo->index.sizeInBytes(),
	             ptr_->geo->index.get(),
	             GL_STATIC_DRAW);

	///////////////////// Position ////////////////////
	glVertexAttribPointer(kGeometryAttrib_Position,
	                      T_FLOAT3,
	                      GL_FLOAT,
	                      GL_FALSE,
	                      stride_,
	                      (GLvoid*)0);
	glEnableVertexAttribArray(kGeometryAttrib_Position);

	/////////////////////  Normal /////////////////////
	glVertexAttribPointer(kGeometryAttrib_Normal,
	                      T_FLOAT3,
	                      GL_FLOAT,
	                      GL_FALSE,
	                      stride_,
	                      (GLvoid*)sizeof(Vec3));
	glEnableVertexAttribArray(kGeometryAttrib_Normal);

	/////////////////////  UV /////////////////////
	glVertexAttribPointer(kGeometryAttrib_UV,
	                      T_FLOAT2,
	                      GL_FLOAT,
	                      GL_FALSE,
	                      stride_,
	                      (GLvoid*)(2 * sizeof(Vec3)));
	glEnableVertexAttribArray(kGeometryAttrib_UV);

	glBindVertexArray(0);

	ptr_->isCompiled = true;
}

bool JB::Geometry::ready() {
	if (ptr_->isCompiled == true && ptr_->isLoaded == true)
		return true;
	else
		return false;
}

void JB::Geometry::draw() {
	if (ready()) {

		glBindVertexArray(ptr_->VAO);
		glDrawElements(GL_TRIANGLES, ptr_->geo->index.size(), GL_UNSIGNED_INT, 0);
		//glDrawElements(GL_LINE_STRIP, ptr_->geo->index.size(), GL_UNSIGNED_INT, 0); //Draws lines

		/*
		glLineWidth(2.5);
		glColor3f(1.0, 1.0, 1.0);
		glBegin(GL_LINES);
		glVertex3f(0.0, 0.0, 0.0); //initPoint
		glVertex3f(15, 0, 0);  //EndPoint
		glEnd();
		*/

		glBindVertexArray(0);

	}
	else
		printf("\n geo_ failed to load or compile");
}